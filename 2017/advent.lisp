(ql:quickload :alexandria)
(ql:quickload :puz)
(ql:quickload :cl-ppcre)
(ql:quickload :queues)
(ql:quickload :cl-slice)
(ql:quickload :bordeaux-threads)

(require :queues.simple-cqueue)


;; puzzle 1

(defvar *puzzle-1-input-list* (coerce "818275977931166178424892653779931342156567268946849597948944469863818248114327524824136924486891794739281668741616818614613222585132742386168687517939432911753846817997473555693821316918473474459788714917665794336753628836231159578734813485687247273288926216976992516314415836985611354682821892793983922755395577592859959966574329787693934242233159947846757279523939217844194346599494858459582798326799512571365294673978955928416955127211624234143497546729348687844317864243859238665326784414349618985832259224761857371389133635711819476969854584123589566163491796442167815899539788237118339218699137497532932492226948892362554937381497389469981346971998271644362944839883953967698665427314592438958181697639594631142991156327257413186621923369632466918836951277519421695264986942261781256412377711245825379412978876134267384793694756732246799739464721215446477972737883445615664755923441441781128933369585655925615257548499628878242122434979197969569971961379367756499884537433839217835728263798431874654317137955175565253555735968376115749641527957935691487965161211853476747758982854811367422656321836839326818976668191525884763294465366151349347633968321457954152621175837754723675485348339261288195865348545793575843874731785852718281311481217515834822185477982342271937155479432673815629144664144538221768992733498856934255518875381672342521819499939835919827166318715849161715775427981485233467222586764392783699273452228728667175488552924399518855743923659815483988899924199449721321589476864161778841352853573584489497263216627369841455165476954483715112127465311353411346132671561568444626828453687183385215975319858714144975174516356117245993696521941589168394574287785233685284294357548156487538175462176268162852746996633977948755296869616778577327951858348313582783675149343562362974553976147259225311183729415381527435926224781181987111454447371894645359797229493458443522549386769845742557644349554641538488252581267341635761715674381775778868374988451463624332123361576518411234438681171864923916896987836734129295354684962897616358722633724198278552339794629939574841672355699222747886785616814449297817352118452284785694551841431869545321438468118" 'list))

(defun rotate-right (l)
  (append (last l) (butlast l 1)))

(defun char-digit (c)
  (- (char-code c) 48))

					; part 1

(defun d1p1 ()
  (apply #'+
	 (map 'list
	      (alexandria:compose #'char-digit #'first)
	      (remove-if-not
			    (lambda (pair) (equal (first pair) (second pair)))
			    (map 'list (lambda (x y) (list x y))
				 (rotate-right *puzzle-1-input-list*)
				 *puzzle-1-input-list*)))))


					; part 2

(defun rotate-right-n (l n)
  (append
   (nthcdr (- (length l) n) l)
   (butlast l n)))

(defun d1p2 ()
  (apply #'+
	 (map 'list
	      (alexandria:compose #'char-digit #'first)
	      (remove-if-not
	       (lambda (pair) (equal (first pair) (second pair)))
	       (map 'list
		    (lambda (x y) (list x y))
		    (rotate-right-n *puzzle-1-input-list* (/ (length *puzzle-1-input-list*) 2))
		    *puzzle-1-input-list*)))))


;; puzzle 2

(defvar *puzzle-2-input*)

(setf *puzzle-2-input* "3751	3769	2769	2039	2794	240	3579	1228	4291	220	324	3960	211	1346	237	1586
550	589	538	110	167	567	99	203	524	288	500	111	118	185	505	74
2127	1904	199	221	1201	250	1119	377	1633	1801	2011	1794	394	238	206	680
435	1703	1385	1461	213	1211	192	1553	1580	197	571	195	326	1491	869	1282
109	104	3033	120	652	2752	1822	2518	1289	1053	1397	951	3015	3016	125	1782
2025	1920	1891	99	1057	1909	2237	106	97	920	603	1841	2150	1980	1970	88
1870	170	167	176	306	1909	1825	1709	168	1400	359	817	1678	1718	1594	1552
98	81	216	677	572	295	38	574	403	74	91	534	662	588	511	51
453	1153	666	695	63	69	68	58	524	1088	75	1117	1192	1232	1046	443
3893	441	1825	3730	3660	115	4503	4105	3495	4092	48	3852	132	156	150	4229
867	44	571	40	884	922	418	328	901	845	42	860	932	53	432	569
905	717	162	4536	4219	179	990	374	4409	4821	393	4181	4054	4958	186	193
2610	2936	218	2552	3281	761	204	3433	3699	2727	3065	3624	193	926	1866	236
2602	216	495	3733	183	4688	2893	4042	3066	3810	189	4392	3900	4321	2814	159
166	136	80	185	135	78	177	123	82	150	121	145	115	63	68	24
214	221	265	766	959	1038	226	1188	1122	117	458	1105	1285	1017	274	281")


					; part 1

(defun max-and-min (row)
  (list (apply 'max row) (apply 'min row)))

(defun d2p1 ()
  (apply '+ (map 'list
		 (lambda (row)
		   (apply '- (max-and-min (map 'list #'parse-integer (cl-ppcre:split "\\s+" row)))))
		 (cl-ppcre:split "\\n" *puzzle-2-input*))))

					; part 2

(defun only-two-evenly-divisible-numbers (l)
  (let* ((first-num (first l))
	 (candidates (rest l))
	 (winning-candidate (find-if
			     (lambda (i) (= 0 (apply 'mod (sort (list i first-num) #'>))))
			     candidates)))
    (cond
      (winning-candidate (list first-num winning-candidate))
      (candidates (only-two-evenly-divisible-numbers candidates)))))

(defun d2p2 ()
  (apply '+ (map 'list
		 (lambda (row)
		   (apply '/
			  (sort (only-two-evenly-divisible-numbers (map 'list #'parse-integer (cl-ppcre:split "\\s+" row))) #'>)))
		 (cl-ppcre:split "\\n" *puzzle-2-input*))))


;; puzzle 3

					; part 1

(defun directions-for-index (n)
  (do ((directions '((:right :up) (:left :down)) (reverse directions))
       (magnitude 1 (1+ magnitude))
       (cur nil (append
		 cur
		 (apply 'append
			(map 'list
			     (lambda (item)
				     (make-sequence 'list magnitude :initial-element item))
			     (first directions)))))) ; accumulator that goes '(:right :up :left :left :down :down :right :right :right ...
      ((>= (length cur) (1- n)) (subseq cur 0 (1- n)))))

(defun apply-direction (posn direction)
  (let ((x (first posn))
	(y (second posn)))
    (case direction
      (:right (list (1+ x) y))
      (:left (list (1- x) y))
      (:up (list x (1+ y)))
      (:down (list x (1- y))))))

(defun manhattan-distance (posn)
  (apply #'+ (map 'list #'abs posn)))

(manhattan-distance (reduce #'apply-direction (directions-for-index 368078) :initial-value '(0 0)))

					; part 2

(defvar *day-3-spiral-grid*)
(setf *day-3-spiral-grid* (make-hash-table :test 'equal))

(defun spiral-grid-get (posn)
  (if (equal '(0 0) posn)
      1
      (or (gethash posn *day-3-spiral-grid*) 0)))

(defun spiral-grid-set (posn value)
  (setf (gethash posn *day-3-spiral-grid*) value))


(defun neighbors (posn)
  (let ((x (first posn))
	(y (second posn)))
    (list (list (1- x) (1- y))
	  (list     x  (1- y))
	  (list (1+ x) (1- y))
	  (list (1- x)     y)
	  (list (1+ x)     y)
	  (list (1- x) (1+ y))
	  (list     x  (1+ y))
	  (list (1+ x) (1+ y)))))

(defun value-for-posn (posn)
  (apply #'+ (map 'list #'spiral-grid-get (neighbors posn))))

(let* ((input-value 368078)
       (really-big-index 100000)
       (coordinates-in-order
	(reverse
	 (reduce (lambda (coordinates direction)
		   (cons (apply-direction (first coordinates) direction) coordinates))
		 (directions-for-index really-big-index)
		 :initial-value '((0 0))))))
  (dolist (coordinate coordinates-in-order)
    (let ((new-value (value-for-posn coordinate)))
      (if (> new-value input-value)
	  (return new-value)
	  (spiral-grid-set coordinate new-value)))))

;; day 4
(defvar *day-4-input*)
(setf *day-4-input* "kvvfl kvvfl olud wjqsqa olud frc
slhm rdfm yxb rsobyt rdfm
pib wzfr xyoakcu zoapeze rtdxt rikc jyeps wdyo hawr xyoakcu hawr
ismtq qwoi kzt ktgzoc gnxblp dzfayil ftfx asscba ionxi dzfayil qwoi
dzuhys kfekxe nvdhdtj hzusdy xzhehgc dhtvdnj oxwlvef
gxg qahl aaipx tkmckn hcsuhy jsudcmy kcefhpn kiasaj tkmckn
roan kqnztj edc zpjwb
yzc roc qrygby rsvts nyijgwr xnpqz
jqgj hhgtw tmychia whkm vvxoq tfbzpe ska ldjmvmo
nyeeg omn geyen ngyee rcjt rjuxh
qpq udci tnp fdfk kffd eyzvmg ufppf wfuodj toamfn tkze jzsb
rrcgxyp rbufd tfjmok vpyhej hcnz ftkojm
jnmomfc jnmomfc bkluz izn ovvm flsch bkluz
odisl hzwv hiasrhi hez ihihsra qpbmi ltwjj iknkwxf nbdtq gbo
gjtszl gjtszl fruo fruo
rdapv gaik cqboix sxnizhh uxmpali jdd usqnz advrp dze
flooz flooz qad tcrq yze bnoijff qpqu vup hyagwll
lnazok dze foi tqwjsk hpx qcql euzpj mwfrk
ilb fmviby ivybmf gtx xtg
rpauuu timere gyg wcolt ireetm safi
croe szwmq bbhd lciird vhcci pdax
hnc ykswt qqqmei goe bri wmyai hnc qpgqc pberqf bzs
hsnrb wdvh iezzrq iezzrq rdbmpta iezzrq kemnptg alkjnp wymmz
ngw don ddvyds nlhkoa aaf gptumum ugtpmmu
vmccke qbpag kvf kvf tgrfghb kvf bhpd sglgx
obomgk bkcgo yso ttft vbw ckl wjgk
fli qvw zhin dfpgfjb udsin nihz ovr tiewo
tgmzmph hauzieo jmg tdbtl lvfr qpaayq qapaqy ausioeu jun piygx
jkp guqrnx asdqmxf vmfvtqb tloqgyo ioix gajowri tmek ilc puhipb
uycn zxqm znft ayal znacus kvcyd ekv qqfpnh
fqghur xtbtdd ztjrylr bpuikb ziyk
rvakn uqbl ozitpdh uqbl dsej xehj
laxp haz jyd xnkrb ijldth woy xapl iqgg alpx gnupa ukptmmh
dyiy dyiy ihb qcyxr
wbwkd hdwu zvgkn hdwu wjc sakwhn zxujdo npllzp uyr uyr
fxczpmn cininu akcxs ggslxr riyxe ojisxe
ppbch sampq dnct afikor dnct edsqy pnzyzmc afikor
jnvygtn hijqjxl vsd jnvygtn nqcqv zns odq gkboxrv kolnq wrvd
mroq mroq flsbu flsbu
fyshor xvpaunj qmktlo xoce wkiyfu ukcl srndc ugwylwm ozcwdw mtqcste kpokr
cfh cxjvx cfh cfh uewshh
bpspbap bpspbap fquj mxmn bwls iirhvuk dmpkyt exrn mxmn
tvyvzk ezszod ntxr xtnr och
knfxhy kbnyl knfxhy xhkssx lxru uprh nkxpbx oodolxr tpvyf
nblmysu iwoffs upgof tyagwf aan vovji ajk ywzq oyfi sfulz
aushzkm lcaeki mkuzsah ynxvte rsntd refk pcm
mgguob gobmug dzenpty gmogbu
yvq eepof rgnree nerger fpb stfrln ernger
hrgkbl mzwvswk rsrsbk ieru holco pajvvn ztgsr qkyp fyeg owpcmoj
fowda gmsqdca yugj mcrroxv mqcbojd fjnqfji qdfsc jqs
qnc rvjfz vvxk sjd xrma ucdjvq sbw zydyt dfzww
ocajazv cozaajv tqunkla udwf ecnnmbz lsakqg bki njnda zsdu ccfqw rxpc
qqm qdfya qxyx qmq qfday uqnfttt
rnbirb iapor qet iapor hxkhz dfvzig pedl ybyb
mkgamxg xkniv meb hbzmxjn dhbj zhbxjmn hdjb
ilteux pyutyfx mau lrr bacak
sjjonmn dbbbgs crxyuu jztstgd ezb uiabyaa
tra fle ufzlvf nnaw kec hiwnnlj tei wld iyt syk hjdczb
qmd jtlud dgh dbanock fzp dsjgqru wwvo jwvxwgv xlemfij jcacd
rpkx oxesil snazcgx fly miiyc ikmtmp oefyyn egbw
ypfpeu wldnyd acchppb yqwcaw wldnyd turbz megci nbgxq xkc ypfpeu
iqqv iqqv neui iqqv
ypsxm icqyup zyetrwq nbisrv
viommi toszx dpueq eyy cunjou ffcjc jaeez djefra pxvkj liudlig yye
fhnacbg jghchh ghjhhc iue hwqmo
vbjw lpn cizba ltnsfpz tzoweml irewlc uzckhpd mszal obd
yeos utxkft hflxkfe fxczge qpgigkc ksgr vuumql vhlvv
xzmkv xzmkv krecdi klpem jsbu nwcmik emfzxf cjmpgnj
vtkjo pmiv zou gxo qdiyxsf hwyinjk jhkgf rjq
dyuoc ywiyvch irfgl ywiyvch fxb fxb
tuz onhr syu rqya abkaf bcfx mbknex juwoor zmksl
oheg spjorx ksdy vwtq fxz phvtazk tcze lrxg
hew lbup botaj ltr jpd
dxgc tzinkej gnz hxvvub adsqmc dxgc asgpp rqbdcra goy pmamdua bhiacva
xqv ygb kihxqz vyv pjcny vmyvsdv cgsi nfyx
tqga ssshrw ndq qlbvwh huyd pxbgj qbxk dkkbf jxy chsobw pph
hxl iwph iwph xnr otifm ljhre
zlgvpd kapxpoc dve rklk ogh hgnp rbrmc zzkz hhmcx aklmo
sar gfor nkf hek nkf aql shc aql
dtcrw kfjzcjx qyhi bldson whwdayo mqtgt xhqzp ttqmg
omspdml isze jdl nvwo qrkm wztfg ssfgyh dryj jhp unsmty
jxt cszylng ifht ixtuna azoi xutqlv jtx tjx
usgm azuayp fgkby ezpyq jqwl ezofj
tnhvil nrvg moyrpqs sldx qymoff megflxh pyhqwms xmdw
zomy zcquwnv lzx bvcna yods mjp dgsez
blklyf xokd gpit tiysj yrwfhm tofx
dtig vhdp omuj vhpd
fogwxim qvdwig emdiv jvhl euwbzkg xvxb hwmqo ujdmlp epmykj
sjxll sjxll pedvgb sjxll
drvay gtzhgtx yrt okz nqf
haxfazn pvkovwb pgu tgshw mxcjf pbe nwoymzc mxcjf pbe hydwy jradcr
prjsloa ahylvj okbsj qbdcdjt pmfo pagyoeg vkmhjzt khzmjvt opfm xfrji gyjqyel
lzypt jdbtrad ogr jdbtrad heink
rcoucuq gdxewa rcoucuq whlw zhhm rcoucuq azaqohe mzyli rdvaf
yuag ebcf yuag nsotg qqzuxr jfmao vyucw wmoye
qwvk xemm hgqrr wyxkpp tojndm xlvzypw jus bgnu bgnu nklfwhs
daqi knenmku ccm xkiuy vkexsbc kvvdagx umopitw yaocnx yoakqql mllmsp
mrxgl gywit mfopia ncnsvw vdxek axuiot rsejua nei prndudz mnu
egqn gaa qgen urs mix zbn rhn
ewharq aihy udkdaob kgrdd kgrdd kugbjtj fcef llqb pduxaq wcexmm
dwtiw nelq hppad algxgf gcc upou akm efnb mxmhrud
yxqaa ups okbhgt iet qns tqn rnjqxgp
npmhdm cgds ldexvr typi jyivoqk zkgq vfyxu xgfo
dkwnmr umm dkwnmr okpjw wqx jpztebl eqsib dkwnmr
dxbild wpbup evscivq dxbild dxbild geqp ojfbpl jshvqej
cxdntxs csfocjd pyy tuhws teb boyloz xfw scxh pxhonky
lteucke xrgwy hszgzu hnyrcvb
pfgsgwg dxzh fworek qbstod
usemcrf psczxu gcjtr brls
hjol efxczux bqdn gvrnpey yyoqse gbam ndzyj lbwb bhzn unsezg
bapw xifz blupk qqdk bofvqpp wnbuwyt rnwocu lzwgtt zucag pov
xkre lqvd juf lqvd xio xyg xyg
tzdao ztheib aymcf aorg iyawrch hetcxa iyawrch czdymc ccv
ucgl azlppu jvxqlj pest
dvwlw fuuy mnhmm okrp ualnqlm uyuznba fzyejk yaq crl ctprp
odfq knox mkbcku pxucmuf lpjpol phl
ixongh hfs ruorbd auy qyssl kykwcix aytsm rlj aytsm duq segpqhk
izufsk wedpzh podjkor eamo vqvev ifnz podjkor xrnuqe
twyfps bmdbgtu qye qkwjms
wlav htym vhsnu cocphsj mdsuq vhsnu jflgmrp
opajag itwjhfu purnnvk opajag
hpkopqp vnj aialpt lzrkzfs nwucez nwuezc
mcx hzcjxq zbxr dsx tpknx fva
rlvgm xrejsvn ghawxb efyos xty wdzdgh olahbtn rga efyos vhtm nsr
cni mbab qtgeiow ulttn rckc kmiaju jvbq emyvpew cdlxldn ulttn brhkprx
eykpffp rapik qki fhjgdyu tome ehjuy bibjk htxd vexvag
wrk dpxt gwkuiov gbkif ike gbkif pcd wpj toywyf qzsa aol
yqwzh uujn ujun ujnu
srs ralwxrz yxvvmgp sjhbhk waasid cqtxoxf whcladv jkmaq khjbsh dlavcwh
mdvsjh xaj etvxlsy fxgiy rgjesel rlegesj ptriz ebdyhkp kugxm dxv egljser
lhehwrs mqevb ygmv gri izop qgb ivm
loqqam alojlwg hgen hbyw qlwpun loqqam worgnwk kope
phozre todsknr todsknr ibj mvllsar
wuripy ruwlfbh wukbkey qhq iishw tvtvci xawvxc vxacwx hsiwi ogq
xryq vxwupqa zhqex aquxpwv bnvxrba dtbxki
yvvwh zvsm vqskhp vqskhp ggqqlw bpn wbuv
kqz tdy goqwge ygn jgd
szjjhdk zkpoo nxexz ebicc
wzuemcj oyd qupulju iaakzmt vzkvz
nppahov umm wpzev wxkgfxd owgekp bhhb bbhh dgviiw kdfgxwx wryb
bnc rhes lmbuhhy kwbefga bnc rtxnvz bnc
ani mggxf mcoixh zdd nai hbhzl mes bdpqr
mjn uinoty jjegvze bjgqg yhqsxbt coj obylb hddude xqi rhfbhha alood
cbjzj drmihy tfkrhsd nuhav hihzx bvblqpl tdd szmp gjgfv box
uumhdxd cmwgyf vepr rwqdkj exwk
hwvr ydvw bqefu kghes gvbhp awms iqsqes khgse
mrey jqfw fwvzhps komj dayvs fbui zmtd cofn mrey
dsjds fdpx irjj usndok qcctsvf fgk wvg txwxcl dxs llp zyilwtq
xmkelgk fdukc cye legkxkm wwly
enlny eynln cccku brkz dpof mwfoxcd yftmnqh wpebvyc
ggdn jnysl dsacffw ukj hdae cmzxku
uqhm gcachmn kxndfrl htmfis jfnajz fiqiypr kekho kekho ndcw ckrndub dejfna
keazuq ertql rauwl keazuq obmh rauwl ksrotm
jppp poigqhv repfsje grjk xwkyuh pkx ayzcj hoxzv
yhjw pcuyad icie icie icie hwcsuy wcd yihjh jnrxs
gaug ivvx ceb xujonak hbtfkeb ttciml cctoz
dggyyi dggyyi gqlyumf yasu fwdfa cbb nncn verhq
rhgcw gpcyct kiuhbg kiuhbg gpcyct jlmleo nhumm
wulxxu jyjek hclcp ogob viex wiqcupq
tthu nxgzpid kcnj mss ukapgkp nnc bxjocv qwxs oejwsif aywqtu brahkb
dtde bgvb smu vbbg zhlu
lyo nwjjmep ldbok wgxhto wwuh qfgjknk wnsl
lleyr onha hkwulbm jfg
bybjwd uoxvbh mvj iqfpnxs bybjwd zqtszp wvc lbazjr zkzenja cev
rbuyyr divtslq yuqmyt ajyveb smxsjb nlk tzqhq ims fewg wpjhr gqh
kpewfd beq klilis klisli eeezut
euqh hueq ldoo crqurv lvrwh tmaewp oodl
bqi lzrf jyhvxfh bqi jyhvxfh nbztd lwpdn cuzi
srjylou phavzjd wost uxkaq byh sluryoj
ihrdk bcegkpq nygrs qbcq wyjg dvzme pgzhjl vibg kvv
ijsx iedemek ktlz gtga tbal lbki gtga
vmiaxn kefig kefig vngxz
vrdmfvi qts vlvhq vlvhq dihmq
cfz dyrz zlw qnt vok fwvahg skshbqf hbwozdc ntana jdb uflp
rimbj bxemw sfps krtk umta vnk ewmbx nrlje ymrtqrz mxewb kjxunbt
egnuti ozat eltl ngueti
qtcwoxq rmaf qtcwoxq qtcwoxq
zws gcoa pydruw qsrk lrkybdf ugr wkrxoj nyvf vitwn
tmr hhd dojid zwrj bhsim righ keqlep flzunou
lwoquvy acjowxk tqudk oenvioh nyavyl
rgh dfhgyke iff cpxhuz hui koe iff hui dmukrei
bjiumig lcbmbgh vleipx sfawua rnf
gftfh qwb tfdroe xbno qhgofm vqfoe mux
ljdrr gyfggai iun nju xrucbis mhrcrh fukr obvuqc whlalfe xrucbis nju
nxjmjr egqwg arllu xqaahri lzc ivt uhsti
sqiepba rcmts kvesv nvp
tiksw tiksw rjni gbhvzm ctbq zuqfyvz
ibsnm kfka aoqigwo sqouih rxz
jmymq lxio adtmk umyu sxvzquq bporqnb heol fow
mepa eckq rqviawv dkqoei ifmngpp jiava rtklseu
yuycd jiufjci yuycd uowg yuycd udq izkicbr csxobh
nwu tfsjavb rruoxbn oepcov elxf rruoxbn rruoxbn azglwth jcjm ksqiqpv
dthfwip zqnwa zqnwa zqnwa
gso wruece ufl crgnlxv vllsm dpyfm wpa ctxko
wvpze seodz lpq lpq pmtp wsxs ffppx
yfxquj phvjn rtwieq rtwieq kgxztyu vbjvkc prqqd lyzmdo ojbrt ojbrt qiqjz
esaezr rpggiy jey kbzrhu uthus osr xxaiijd qfxlf auhzbx gkigoqw
yfhcj uvgck cds gjhhrg cmempgj yfhcj cjb
yxi voxvtuw unwg jqqm
igvjr ljz rus sru gbjtjt qfeg ztu zjl
leof ocxns hbkoysh hbkoysh leof
hab lyxmf yhh qeks fwhfxki xmbcak okqjii nfgzyg bhtfgdj lpmjn
mgognh tad herere lvwnzx ixwqs zphmuuc etdjz kczsf
mtej rlolsnn zbl uykek dpkan gmz etxtgj
mihuieo emjgbp jgks mihuieo iexrfw mjdnr bvp mcuzea xkbusvi
jvqpj bwt jvqpj bwt gxr
qpnd fpt tpor bibbpcg hmvguez wqc afl ckviua gpi
dntmcg jglm sxtnu sxtnu sxtnu
fzkbptw cbfwo ozvwov wbv gcdd izqo ovwzov lolewo xikqpw
nkxyxzd kpn datf fki werq mwidqx oiibor zizcjph
xvgyxym zor ijoy lvwsf fjuara idvvq rreit mqyyy ctio tzwqqhj rnpee
maqkfpk maqkfpk xukg sfdmnlg xjopvr xjopvr irf
liujcd vnlkouy dxkwc gto vhjvtw
swhqhj cas aupsd swhqhj cas bvbooii jquck dtdm
igh iqicicf ghi pcxt srcrjx gmf gyscphv
drplj drplj wopgpnk wytag wopgpnk
zexe ilcqoh qiefb txkuv lirfzv
ovvpn ovvpn uqeurqx uwzn hgmucj ovvpn sjxulms
rox silka irhsvym kutus otasof tdneav pcagds
mkja omu tyshbfq onp trxs lxa tftbv bnpl djhnc zdqfs muo
tjj rmmqas cbbkxs qio pikk ykyew gxlxt nhsyl ykyew
frcprg njrz oaxcmhc qben pedm ecvtga nzxwpb ior gaklot dpem
zyt kncau spoe qlchg sqys wkpbng yflju qlchg vkve bzadbpa
qtq pkaicl qtq mfkfqvr dnleiq brrjxsx uoyxh pkaicl yvmlug
firwy imtlp ywl qfa dqrbazz ztzb pcsbwhn zesmlag
ivey ivey mtvc mtvc
lhize acwf moa cdeoazd voktshy qmvqq jvmuvk ljfmq tsanygc
xreiqkc aawrovl pofcsg xreiqkc xreiqkc
cjbzvn ozds iniqu sdoz gqmki bablvll krs vjzcbn
izsod htkeqz entxn qtns prpcwu omfnmoy
kwfb tctzda aztctd tadtcz gyt wunbcub ydiwdin xxk
epnl ijcp giq ltfk zjcabve zfksmz epnl giq xxxbsom
ulyukpa mdjsbn dydko uhkdt qms aaaj hustlwu
zlsbu ohx jcwovf egf zlvpqgx qhejm wrywdmw
uhxqrzr mmu kjxcalj unuohiq rri yzngnb ikvlxry mfiym qbksdx
khqciz som yklmm jceb khqciz jspy jceb
ncwggv njvi nqox krtsn lnm
bgtqme xaxcoq qbtgme obqual vorfk baoqul lgrb
jli tsbb nlxjc pkwzmz dlxrj hmho gzguko ilj iyaasm
wlmw grkumg dynwtyo emxhhqr huluk slpqu uhqcmd absmr ufirmwr
pbs pcammxv dplfr tzvmav nccyy blvyq ffhnz bccutq
hgge ghge vxmvz hqxgjdg zab guo gheg
ylj bucoyoq udndc wpgyrbx ueh udndc gxdsdh hdoz wwgqlg
cjdeh gttyqe kdkm ltzd lfeozse quvjq mnwhokm kdv oojxm nxt
mfkzus knqxt saxkqww njx zumsfk sbmcyad cpt agvbuv
tukn vyco yobvsn bzgnn klrnzy kea thzk pxpwq ryfff nxzm
ylbm lxlz lybm lzxl
wgtxoij zad slgsi cvnxfg iomswwl vmx
hkm yinhnkj kmh kwkw kayknck chur styjif yknakck
rtfwhkq rtfwhkq zsf zsf
sldq zlntr ueegiw kajivqc ozcbm ceft snvugom pdyc elppeed nnqrp prwwf
lhk xjonc muc tudag tsafx mmivb dvrjbp qgrew
hnzer fbgqp aazta aazta lxaz lmgv aazta
victgxu victgxu mlpd ummrnbx cazjgnw isxcyp efy zfa cyusj
gyojxo onzq gyojxo uxufp awi ilhl wefwfxr gcjlt tmliynw uxufp pdcnxah
wjwachn xkuhfbp oky oky ybaeqkr rbuix yreoaw wepmye brvon aasb
kiidorw vxtxiqx wtqvbrv efdth isel qbom vcssyc vxtxiqx wtqvbrv riafzsw mqzsj
eurpjd vkhdamt tmfx czeoot hiz ykz lmixzq tfur jhzr
ipuftpj qbll sqkkdw fwncmiv bri oeeh lehd ioh wag
suima nanngc imrmc krq atxdo woy atxdo akev qlr aezco qlr
cfc efwbzck ozkmcxv moczkvx ccf
bnekky iakrk sask uwgnjp iyi rynev bdnas ldh kass
sicmw vvjbvv cap nsumc xgvrlm wsoo uoqdu psykckm
ugg mtr wnzhmmh tjxc ehwnji lwhu mdsckk yvmk enubrqo
grb oxmxz ohu ytetedv ssx apzlppg fdkamm sxofc jdt ynmu wyejok
umoep rbyqm eqfk twqnog cptbbi dragna ngqs ffb cexxnc rbyqm
utizi ormkel wvwur bdx ecelqbv xiccama aag glfvmj
znb rsuqoa uxo svc
obs lbifa cffi catpd
qkxwian ajlzjz wewduzp bbyv qmt fsr qgiu epinp ghmf
hatg bfgmb aght ghat
kuq inp dun cknbun wmwsu drlmmg kyxc bdl
bddybth swdbf jhi fva qpobio bjwm wjaztp jywi
mgckz vhveu zkemhp zdf xtiqqew mlx wazgd
umbjq pya lvvxf jeavij rhrxvew bwjqgpr piz
xaycpwo vjcuc qksc yuixhni sfbfb dydyaq gdfvb tggg xidphvf bpjdrl goskxym
agxfoip gguif wvo agxfoip ntkbaw fbyggy ooft zxih
nzvsu ffwq uxvfbl qrql olhmhom qhdltg ymwz krtndtx olhmhom nfsv krtndtx
qdp jqk ustz xjripzv mnk grnodk pjwdsj uug zqxjqj
mufrcox zunisfs ocvcge acamm xua vor bsde kxr vor kxr orccxx
ncycbp anvcxay bmm wndmeaw oso knmk mmb wamenwd kmkv ppdd
motdcn xzagzwu vuzt utffrn yuqxzrh uvzt ujttq
tauoqy coiy ybesz tauoqy wpmr trquyne ahxbj jzhems dsdy
aczq ypw pgmzz srfn quatjgf
cih ypapk bfxvr euvhkk gugru auhqui
vyf pssgfvy dnhvbfl xpacme dnhvbfl mzdv iynq hcqu
lbzvbu hhxiq hdfyiiz iyzihfd xhqih uzdqyxr
iapbdll vdr cprmrkk vdr dfjqse mlry flpqk vdr
grrfkq xcpxd grrfkq dxc bjpr prvwh swoc swoc
bopo chvwuhf qhd ieesl xey ieesl fnjcbe
kic fyq hsucnu agwyl pzzmd hqksh psw
mxf uau iti lcoz lpg zbu ocre wqlocmh mxf nidqj lcoz
bypmix ptzxgmf xmtzgpf hrvzzq
lbfw zwusma lbfw tuyyy
lrf uej unswvh obgsb npbl zajr kenea uej qnyjcu wzufim qpzkgya
qcrxj llyu kligt hlm ehwtbx dda lgsvhdt xewfcv uikn
nfzjx izqdbq mfbxs imiuc yqxb xlmvix izqdbq eflqfq wku omgtuu izqdbq
lasdwg hiy btzt eefd eyoep icn nnmhg otml rek luixac nyzgn
vekteds utsuxdx utsuxdx vekteds
feyov qrij zbebwg ijrq seplram wttkwm zewbgb kzuhuh
dmkgtv wohgqo ddtqmv zatahx mym hqowog tkmvdg
vhha wjrmuyx kqh vyyrj xzchbi ejsdq orlxg vyyrj dlrc
yetngqn zdtuqox hkarjei fqpsgh eaqwbg zsssog ghb gddqqzr hbg
obldb zsrhz zxp uxphnev mwnbc pfjft fms xwslk vjm fxy
nfij dbfykv ttq gyjgac igxuyqi gtiioqx ilhdex dbfykv uyp bdiwya gqf
pffzruz vogfosh dcs wje
pohhf fhpoh oon yyz
xxuam afwm qxl lnt syyr bwxhhf sozauq shlhfmz kwnn milav ochq
wefcqrt gejw cwerqtf fttf gjew
jfsvnmr osca epwtle pgfif sxom
exlfzmq nakp rgdnx rrcvth vhrrct aajjdrt ryyg dsozd jdqlqj pakn iruv
rmcvo txszcs xxhyxz hbsozk wshkocf rmcvo rcbnt
kitz yjgney yvkymef nauj hmllsgl kyhm kqr pzsu rcf pzsu qpte
cdinpx bfur mkj naz ihkheyr nohhoe
ylris xeqcgup wap bbfih tgfoj
ina gnlnm zyeqhij cudfuf ipufae bvkdzni aat teqsg cudfuf bjokrbl teqsg
aedx edax dnfwq qndwf
rdngdy jde wvgkhto bdvngf mdup eskuvg ezli opibo mppoc mdup zrasc
qcnc iaw grjfsxe gnf gnf
zbjm snznt zelswrk gkhlnx dqxqn qqxnd dmro
zisecvx ztezof uzbq otnrtj qsjzkwm ewvcp rlir bfghlq tgapdr qxmr
ipnqj opjf vabyoe wkwnd
wyf mfqxnrf apm snarf jqu aaghx pwecbv lvghayg
acncv jmmbwlg oiphlm ifuo cvt
pvmb egansnd zmh gcuzzci rrxpslv ubith
uoleptg xbouzn xbmg cfh cpn wpqi xbouzn xtxis sxzpns
rilybri kurbpq vfmjpck tjyogho hfyxad svfofx lfbbhxj khaerfs iqr
seaebgz wlmtkre qguv qguv wlmtkre
sgo edkxya zdqgwtt gxu nibuu rairqoq mzxli dci qsv
tsol mdhzqr rmaqnru ggvcq arbwkn hlkcnj ljkcuof
mmliphp ocup puoc eijjv
gmajqpb ijki ijki kvz
pmqss unhlpcj dlkll nuhlcjp expe tlurzmv nsy vlumtzr tgseozl
gkvaoni hsba hsba viuedv phyoclp fdq phyoclp febld nqfs
rxvdtw abn pntv qrqfzz slsvv abn lrxix mnu npot
ghlfjp woy xwkbmv bkahpkj jve cncvk jvdype fwgvoju yrkwjp gwfvln mvkv
kmluh mie bby fwer chsinb ojglqr nqk mie
yzmiu igkgca ybnsqja jpfejtp yjddy xsosxfi ingx qwuhb emrkwpx idqjmmm
btrllw mphm dkvo ewdl dchcul yah btrllw kmqi mtvgk wtb
hxsgard yuikc lykt tdee adprp gpougod klnzk mzsmlb
hdn znblw ifoblur bwzln dbv
smofpbs vjuyiro llk lfzesga tybu tybu
gffnpug xaup iqiyz fjkpnkz drrk fwyxw lwzfskz gslwpmv vjxylva tbkyo nib
evydmb nhwuiiu fkerq nkgbuyy uclrs ydjgglh xhotwbm riirgzt
bsub eavbt uvd dpzwyt rhn khrbptt xszckc djnfxju axofhat powmso nvdffrv
xtuykl fjz mbikc xpnx hmey fjz fjz
rkls nwdcsyx rkls rkls
tygml untequ ybdfumz nqffbq uipc sove hfnqj
ytecew vven koqn royynd qsn ksl qsn sdw
hknlw qwho whoq oqwh
lzmmtqu qvhyeo cnofuj utpwkjz gnirz yhhu aodbnd
zsr axw kwtzcv tydzo kwtzcv lkxsm
rbjtqe nihifd gvdxd bpxzy rxteky vgcgllv vbbua anygiup rqo
dpd wblfwp wblfwp wblfwp ygahc tqjbaq
gsw gsw pacgj xmrcz zmxhmch xmrcz
pdq rhe xqmq lgpkhg fyffrot ovnqh wle
tbjavke ypzzrj jizx gdxoh icjsat otfh fmygumv
snch nxlgjgp jeyn sxoqfj jtage jtage iuice
rtb coefuj grwg grwg rtb krhqnma vfhgbr
vhegtl btorwxg szcev kbvkx itsk nlzpbed
hiukrf ilzkm yllhh xsgwkdp zyy kjbv
rfcg tdorci zcj wzftlv rfcg rfcg
lgbc lzizat vsno pau nvv vsno bbr lzizat qhtb gwp
sfwnio tcugjk bsfsz ykyfwg ibkap fsrvy mygk kzunawx zyhyh
mpavlh qps bylh lttjkz rqabgk vewb bwev tlzkjt gzrbxga ktmso prpkj
gpf ims ynh ffrs vpa iemp gofh cgbauje
secys qks mcnfhwh drog kqs pajy zoltkw lfihnb myb ioxptu
ytq nrta ouk ajqblf yuwwcd zdy blyoxbw dakk nvgi bzrhzaa
nkoych sufiia xkdvw crtldee zycl qblab egqhr qblab
nllno muxaf vds qjnitmw zkpj wskyhft kmqct xamuzpw qcai cdjtbt kaxv
qzdytpe osr fuw osr qzdytpe whperd rydwdcl knoa
zkdznhd peh duoygr zamrgl irnvj otpe pltpq jdkecg
byzgw rece iigdug ehif tpgje
ccnn foqdran gbctca tefdjxh ntcr rjciii xip xlss crl wvvhzqm twyohf
dqyii milqqc qjgkojp qjgkojp ryde
tdkyj tbrcud tsba vqtmb cjwxnf
hqhmq wemvrce nagig pwnw nagig epg nagig vlsi
tqgvw luoplw hccti npjm rytdruq cylrsun rytdruq vjsbjl rytdruq ppti
itgt tuwc itgt rvp itgt tigns eipl ksmru
pdw wdhtkn nbdbpn wff zhuuipg rvemv qxr
qgkwdq cjilayh ymeks mrpuzai dwgs stfstgz ucvqhb yout oiq
vpxik ypfr qytimvu qms oxbmw ppyfx
fwwidn gdhd pyuexk snsz iwndfw
lfcb sllxjna lfcb hpzahfg mmvgaa svny jhuzd
unyg gicmzd fwc spkciy toyq wjupckd vzzx iuqgka ytqycb pxsufj
goj tnrcml eyizngj txa xrkiw zvu igduz
wek xrrlkna clyof rrlnxak
cjm rmyuku vjom gtf
buk cfae awstd dywgqp hxo wcxvf laihqw xdqfes wdbh qceh uzlwj
sudguo dxwplto rlebdh bkamu dxwplto
crwkyxm yuz kjtdhom crwkyxm
trhc sduorxr aizfryh rsudxor gbyc
pczkyl bptp qnn nxmpwsx udrg hhlb rubtrmx twzodlp xygnht
jmqct cden yfajtkz fevcw sxonbxz sxonbxz qkzkm hhngr fbv
sdsnm mwvicr wypfi cty ndbowr woiz mrauwzd qlno mwvicr
vteyo fng lvr lxytn txpj milg
wjx ahtmgo cgwcaj kaxae fhlvlqf
ezj eetqhzu upwda iiefwlk vyvby
imalvy yeghqe jwcu mvrod cwju
bxnmsa yhfu npsdar tsbri hfuy sirbt oofxmy
fkndt elbjtn vepqtxt elvpf fpelv bzkgag qttexpv prblwb
rmq iqs yvprnyy iezqrzm wlqsrr
yviovq lekxghj oey qwhzj lxknxw qiyovv ksnt jptz
tyrg cifxt hugqf tyrg ffuiv jmax qyw fozfosq ffuiv
nmg rsl jpzazd qbtlf yxqtsj czwmdfd bamge lbjdof uqy jssc
cbx boozjip pwgvzlq rjz kxy kxy hszacok fvsq jhnir cnsba gafz
sbcuxb wfur nnnfqjj fdwg huhe sbcuxb
icwk qelbxs uevp qped zsnhh wpuok wddxsln ftnzupr ruxol cgxjb jbhh
izcp htykj xxmndoq amnspe htykj
vverol oixwlny vqd tvfzu henc gnyrwr
ytxio etytsx choynep zqapo hfjit
lkvgr oyzfa taiqr jok djatvy ckif tmdw oyzfa zroy
jlgpyp kkqysg oqjki hjohoug hbhta muilz zft
sumfyu wftcu bwwdcy lezimwa qwvxv zwh mqyv bmfot aii torcol rnt
tpdj xrw ccsbnh fhptv fwkxjfm dmqaokd bjci
zxi vmf vmf dpyg
sfzxysw lcms bkojtv bkojtv
opywo qll ipkitr mtwp tudrr svhyp huz bxsdpn xomfy
gkod luo qrosbp orbd rpsjzyd rlh gdok tze
nusiuq nusiuq zeys ahufexc
veno jntg avtmtdn qojxru zegdcql odfcetz pgehau
uqun vigjm ykac ozlelj danmji bibugox
rpuozh ajwru rbvuevv uhzsq
iawoe tyb aewio ymf byt inijv ctu fcys micsgzl pbby alt
gktyxp ris mqpfm bkqsfl nrg idbbcxg jhcf
qibt invvv qibt luitx rnm eby hrfbmwl wnap sgkzvb qlwc hrfbmwl
jwkv qecsjbw lycgldd wjvk tjcp dycldgl pzrvr zrlcf kji
nzsrmiq nmhse ilivrk kqv
besmyzi imkgpt iekbjax abxeijk uvzs wwv
jdocl uki ltswp tjkljc ymce iuepze qygqxzs tei lkry
hhyfy gvzd mqksxlq czn afe mesnag eep frwgekg mqksxlq phpy
ehg connnza ekt ddgokw
mpbsoms uzhzl xevww ztt uzhzl
lftybr firc awsud dsxdkk ltf ipjv dtx lcymth
vkcpb gxtxq yioeq fexj xxgqt
srvca fslnnvf nfmkpvt egw wemumq jie vznf dzsjw cukf kcvyir
yxjkl lyjkx jyxlk kgc xtz
tpoe xzov csp leleoqo noyre tdhf cyib sjgtdx raehdw nmcxp
qvt uhznqe bpvos vtq ddlebtd tqv
xlw utsxs gpia rvlvnts elkxr dddihy tnrslvv ibf wlx bxg
cwqnnrt rkkqyf dye yde fzl pthanj
boc rqjenpp xjqte jteqx pvoofc pidqe ruoucy gvnro ognrv
qhalb gnazwc fhl iuti
clnbjfo nnfs nnfs heymvr oarew oarew nxu
lwtrotg hiaxwj ymzbly nvhzjhj zlsaheg nvhzjhj ymzbly
rrvi tsjp tsjp tsjp killji
rpx hiclj cmwq ibhj nfd
pvwymn iebkd xmpw vuhhkap ksw zigzy mzzyyxy rmuh iwwhea cglfq
rlwelgy sffml jin qsdzro xlsty mgqzuu etxjuo emzd jgnoyq tkjuy vfvb
tkctdj hhkuc viskmy obw
zvjkuj akeky ikj jqd hfhzbwe bkc
btev nrdo hcyiuph stf qharfg vpmel mpfz nvs ytgbbc
ieepn ndueuw svmdr tcvumw mceyrn mrjwhyl tbdj mgrgvz
uxrs ckyi xpmqm czzrkl cjp
nlliwd wrqkrkz yjmng nlliwd zirde hcjjn wco ysf mgl
dxti lcahe ommare izlwf ramsfb nzgfvo ijvm fwymrdu bndq
isxy jpvuzu tdduyhw dixp cfa fkzbteg ytoi kepk ysf yqcpi
qmeprfj soqo ncgeor cqsuuj grzy wogxy vyblnbg slvtry vdols kka
ltykfp gtzl olrp gxend vapee deq
emywfbn dbfiut rkt wvwe dbfiut bwffhea yuzcxv gogpicp wvwe
vqvmrp ofbk dlfabd jwllzxk obx vqpwjj umvng tqwis fstxy fstxy
miha zgvyux rmraszo xwf
kjaagk btm kjaagk wkewjrg kjaagk
lbmli aizs omrdr gzktnx asiz ptanzpa xlo ljre ckyb wob
svz dlk rijagg avxmg fkzwhk uro gegm
dzplum temdw jqnm tvxcww bmg tftttpp deuw comxey xfimzjx caluczi nqn
uwvhxa ztkd nlsdyt vihl julkwwv uzch dwakhs
wkhuihh ycrc cxff vzcfhpp uegfd gaok kcnvz lhzogq lwa tyrypvu
idp zmrrzp zmrrzp nktp xsnx rjsxn
eybrnib ivgntl vaxsbpi eybrnib
nzvnq xvbfa pbhwwh ylju runvsj imlx vztesn
nfdohd nfdohd gtevnky pivjyct ihvd fzcsrq lko fmqk
kwpkks ecikxu bcxswlt qvrxm sbcqmh
kdjrmj piuh kdjrmj vnaf gyedkg vptxgm xezssxx zsg qjzpo zsg
oqo sley aqx qmpqb fgmylbj egd zivj kepxizv kuakyn lunbnd
hmcf hmcf xlhgc hmcf cdlm buofnx
onjcj yluonz kzmk phqo phqo phqo
ohaafy efl bnkkjww wwjnyoj dxeaig ywnjjwo slk hrbebw ohlyju elf
msohiqz aunk njki bfktdgi htmyrj mgx
numlzrl rmnlulz glb ltt fhbajz gqxpu
gko hco oai ryq xwy sdqosft spjkiu cxfhg ycwpglh noy rah
btzpjem brpk vqr atxu rhlh rqv jmg fvyus
phmxxgj ejx xje qtk hsb kqt npwj gqt
hujyjp nwmsd ant zipuya lrkahww uwqal vzlo qmbo twkjkse ufivi
zfbnyz fwvh xrnrw usn zin daq iwjzj
yykyg iwypfy hehqnl cjvk cevdrec
gui muuto wsta glqmx gfo rdmbv mxwz gffzt eejpw gion
lpng nduid iqbpu nduid knrqd
xwxn oefpckv gjaua ugaaj gjuaa
qxk aeql trqdmqc crzlinj crzlinj trqdmqc rijcne ewyf
rfv qmbe fvr bmeq
upqyfw lowzq wpen upqyfw gfskbil sljuzh wpen
bdcara qyhx rtaez qyq gbyr
evzls qxtxq clzd svbgqi zxlzgss vtrre fko eebo qjyl
zaapeo kpwhz tygknau nyd pch trp xqe
ypzcafg rnqmbh qtteg sncu ssojhhm zonfym thir xmgheb wqj gpjg ssojhhm
wvcwyn xrf muozyya lasdp xpjgu kpqv zkiihiv ifje cbdlavg xbied hfnaa
qqqb rettz rycukl ihpkhh
dnxzxqv znb znb fbxj azxtezb xvxa
peqkd xlzqkov esgnw ucku hrwpfxd xtd vnig vlmfp ajte qswr kqoj
dpwy oavzkk dwyp ehij upqxgii pydw
amfc hfv xmqa nqvn cal rqmcq oej amqx cla ntxj
hqhhe qkbhwli wmhlcq xaczs peywuo
vcr xfv xfv kymo qpszwzo xfv
nmrbur tswo xbo ljlrzo bmhpgc pev zovkznz lok wbbhtkk
tojj lxqgr rhjavrm ndsdup gdbjwaq cqpnl wfaxivl rfry ryfr udspnd
beffod sknlph amb feobdf
mldgn jxovw yuawcvz kzgzwht rxqhzev fsdnvu vluuo eycoh cugf qjugo
tlnd qcxj ker fdir cgkpo nrqhyq raef uqadf iahy rxx
mhvisju lhmdbs tcxied xeidtc ujry cditex gvqpqm
cgc jazrp crgnna uvuokl uvuokl uoiwl sknmc sknmc
rvbu czwpdit vmlihg spz lfaxxev zslfuto oog dvoksub")


					; part 1

(defun valid-passwordp (password)
  (let ((words (cl-ppcre:split "\\s+" password)))
    (= (length (remove-duplicates words :test 'equal)) (length words))))

(count-if #'valid-passwordp (cl-ppcre:split "\\n" *day-4-input*))

					; part 2


(defun anagram-identity (word)
  (sort (copy-seq word) 'char<))

(defun valid-passwordp-2 (password)
  (let ((words (map 'list #'anagram-identity (cl-ppcre:split "\\s+" password))))
    (= (length (remove-duplicates words :test 'equal)) (length words))))

(count-if #'valid-passwordp-2 (cl-ppcre:split "\\n" *day-4-input*))


;; day 4
(defvar *day-4-input*)
(setf *day-4-input* #(0 3 0 1 -3))

(setf *day-4-input* #(0 1 1 -2 -2 -2 -2 -4 2 0 -9 1 -5 -12 -9 -10 -7 -7 -2 -10 -2 0 -6 -14 -6 -24 -10 0 -4 1 1 -30 -31 -24 0 -20 -24 -10 -2 0 -28 -8 -3 -23 -6 2 -41 -36 -2 -11 -39 -27 -21 -48 -38 -4 -29 -49 -9 -57 -33 -60 -42 -12 -21 -19 -21 -52 -65 -62 -26 -64 -67 -12 -22 -21 -30 -31 -20 -54 -57 -20 -18 -46 -1 1 -76 -25 -80 -60 -80 -36 -30 -85 -21 -89 -62 -66 -4 -39 -64 -39 -88 -17 -5 -69 -90 -14 -2 -13 -76 1 -10 -10 -28 0 -96 -16 -33 -90 -56 -25 -9 -4 -110 -54 -72 -92 -127 -112 -38 -17 -114 -82 -35 -51 -41 -3 -14 -69 -102 -72 -6 -118 -80 -111 -96 -45 -43 -19 -37 -24 -75 -75 -115 -54 -52 -19 -123 -151 -122 -96 -20 -46 -67 -128 -123 -9 -43 -34 -160 -111 2 -164 -158 -63 -67 -2 -145 -103 -80 -53 -148 -103 -47 0 -178 -147 -57 -152 -46 -173 -119 -184 -69 -113 -112 -51 -33 -187 -172 -172 -122 -56 -59 -24 -204 -86 -65 -152 -119 -201 -75 -16 -106 -159 -152 -77 -29 -9 -39 -49 -141 -211 -23 -145 -96 -94 -84 -99 -66 -9 -135 -185 -15 -184 -123 -152 -94 -67 -43 -127 -3 -21 -11 -76 -129 -139 -65 -185 -15 -215 -163 -232 -1 -173 -81 -148 -12 1 -251 -183 -214 -162 -213 -247 -165 -232 -107 -75 -190 -205 -138 -149 -232 -99 -57 -148 -231 -188 -141 -193 -39 -208 -245 -102 -247 -1 -76 -13 -20 -263 -146 -158 -81 -270 -162 -70 -215 -286 -98 -186 -184 -147 -146 -160 -172 -42 -48 -14 -175 -299 -293 -214 -198 -128 -272 -108 -94 -102 -108 -53 2 -172 -41 -293 -14 -256 -92 -121 -140 -294 -54 -121 -221 -145 -260 -298 -82 -284 -238 -15 -159 -159 -213 -31 -44 -61 -203 -247 -39 -157 -130 -347 -272 -23 -185 -162 -337 -91 -72 -91 -315 -144 -165 -360 -173 -258 -275 -99 -81 -16 -72 -150 -238 -45 -7 -344 -364 -339 -54 -61 -24 -324 -321 -294 -104 -87 -165 -113 -4 -306 -198 -147 -136 -360 -217 -20 -391 -169 -209 -12 -95 -164 -215 -239 -87 -341 -241 -340 -343 -372 -305 -252 -398 -208 -284 -28 -11 -222 -360 -190 -9 -233 -68 -14 -220 -34 -87 -392 -84 -41 -187 -59 -247 -258 -143 -102 -208 -182 -254 -67 -182 -279 -339 -200 -445 -43 -120 -418 -273 -201 -113 -394 -4 -197 -313 -116 -62 -323 -47 -14 -24 -416 -150 -28 -288 -461 0 -388 -375 -424 -302 -4 -75 -54 -288 -212 -436 -414 -447 -362 -120 -467 -135 -93 -268 -8 -192 -342 -466 -162 -387 -348 -351 -236 -123 -51 -225 -259 -52 -14 -26 -347 -327 -206 -37 -77 -316 -278 -195 -348 -330 -235 -125 -36 -323 -10 -77 -133 -353 -109 -51 -134 -281 -227 -483 -406 -11 -356 -443 -104 -264 -90 -489 -39 -145 -318 -399 -238 -434 -145 -122 -507 -196 -156 -251 -370 -207 -534 -171 -117 -416 -226 -393 -133 -391 -347 -510 -121 -84 2 -110 -427 -456 -184 -295 -337 -444 -143 -120 -163 -351 -36 -483 -315 -240 -462 -367 -237 -277 -102 -426 -250 -240 -503 -567 -324 -555 -14 -197 -24 -371 -484 -54 -13 -432 -343 -54 -450 -374 -28 -154 -216 -314 -122 -281 -495 -351 -365 -528 -545 -429 -411 -93 -230 -170 -188 -227 -499 -562 -275 -412 -597 -64 -303 -374 -262 -359 -549 -579 -379 -143 -598 -273 -618 -449 -425 -441 -251 -135 -150 -521 -561 -460 -79 -252 -336 -27 -331 -335 -46 -555 -121 -447 -563 -617 -42 -125 -92 -472 -41 -164 -450 -372 -584 -327 -278 -307 -378 -513 -52 -55 -551 -81 -550 -472 -347 -664 -348 -150 -88 -7 -559 -475 -553 -342 -20 -411 -574 -419 -363 -176 -379 -429 -548 -649 -178 -449 -594 -536 -386 -108 -552 -179 -578 -398 -281 -3 -93 -706 -679 -623 -140 -682 -59 -710 -416 -390 -217 -679 -540 -85 -31 -403 -28 -15 -105 -388 -571 -103 -136 -404 -555 -667 -189 -460 -433 -278 -310 -300 -393 -383 -203 -632 -482 -371 -385 -265 -197 -100 -512 -668 -291 -626 -384 -40 -21 -411 -288 -187 -56 -556 -455 -114 -560 -205 -22 -442 -38 -260 -492 -276 -621 -202 -183 -5 -345 -25 -500 -633 -476 -47 -778 -726 -628 -308 -715 -705 -247 -670 -699 -136 -521 -311 -773 -333 -721 -77 -76 -197 -101 -31 -6 -701 -640 -678 -421 -778 -627 -359 -789 -463 -99 -557 -796 -12 -678 -591 -359 -711 -175 -82 -18 -347 -601 -819 0 -40 -32 -454 -680 -783 -269 -744 -726 -336 -563 -152 -782 -651 -674 -788 -311 -640 -525 -54 -317 -312 -328 -128 -162 -133 -769 -669 -611 -553 -247 -174 -217 -497 -202 -450 -486 -102 -35 -273 -436 -282 -343 -544 -602 -171 -444 -865 -206 -486 -5 -566 -444 -496 -142 -502 -9 -359 -330 -797 -735 -726 -66 -290 -716 -494 -796 -373 -519 -502 -78 -622 -602 -408 -511 -114 -330 -794 -102 -795 -882 -264 -771 -832 -729 -527 -264 -4 -12 -517 -516 -85 -899 -693 -759 -367 -844 -377 -207 -590 -551 -93 -810 -449 -464 -111 -161 -154 -823 -60 -523 -265 -219 -903 -170 -601 -785 -144 -439 -190 -275 -72 -75 -175 -137 -842 -336 -169 -245 -480 -751 -363 -610 -505 -771 -85 -337 -307 -687 -731 -118 -313 -541 -490 -485 -647 -104 -101 -324 -57 -5 -610 -251 -841 -97 -562 -664 -765 -969 -449 -172 -846 -465 -212 -600 -62 -399 -923 -62 -400 -197 -144 -699 -730 -754 -748 -405 -518 -633 -893 -675 -717 -380 -464 -193 -590 -888 -64 -111 -905 -774 -209 -492 -64 -589 -952 -59 -5 -615 -208 -131 -867 -594 -668 -253 -75 -418 -5 -394 -456 -266 -669 -335 -687 -661 -73 -446 -828 -413 -643 -410 -91 -25 -84 -335 -943 -355 -155 -778 -1013 -772 -976 -305 -575 -1060 -148 -931 -588 -204 -56 -102 -467 -937 -481 -890 -503 -379 -442 -774 -316 -732))

					; part 1

(defun jump-distance-from-exit (steps current-position)
  (if (or (< current-position 0) (>= current-position (length *day-4-input*)))
      steps
      (let ((offset (elt *day-4-input* current-position)))
	   (setf (elt *day-4-input* current-position) (1+ offset))
	   (jump-distance-from-exit (1+ steps) (+ current-position offset)))))

(jump-distance-from-exit 0 0)

					; part 2

(defun jump-distance-from-exit-2 (steps current-position)
  (if (or (< current-position 0) (>= current-position (length *day-4-input*)))
      steps
      (let ((offset (elt *day-4-input* current-position)))
	   (setf (elt *day-4-input* current-position) (+ (if (>= offset 3) -1 1) offset))
	   (jump-distance-from-exit-2 (1+ steps) (+ current-position offset)))))

(jump-distance-from-exit-2 0 0)


;; day 6

(defun rotate-left-n (l n)
  (append (nthcdr n l)
	  (butlast l (- (length l) n))))

(defun reallocate-memory (memory-banks)
  (let* ((memory-banks-copy (copy-seq memory-banks))
	 (highest-value (apply 'max memory-banks-copy))
	 (highest-position (position highest-value memory-banks-copy))
	 (rotated-list (rotate-left-n memory-banks-copy (1+ highest-position)))
	 (part-size (floor (/ highest-value (length memory-banks-copy))))
	 (num-banks-that-get-an-extra-block (rem highest-value (length memory-banks-copy)))
	 (memory-banks-copy (append (map 'list (lambda (i) (+ i part-size)) (butlast rotated-list)) (list part-size))))
    (rotate-right-n (append (map 'list #'1+ (butlast memory-banks-copy (- (length memory-banks-copy) num-banks-that-get-an-extra-block)))
			    (nthcdr num-banks-that-get-an-extra-block memory-banks-copy))
		    (1+ highest-position))))

(defvar *day-6-input*)
(setf *day-6-input* '(0 2 7 0))
(setf *day-6-input* '(2	8	8	5	4	2	3	1	5	5	1	2	15	13	5	14))

(do ((current-bank *day-6-input* (reallocate-memory current-bank))
     (seen-before (make-hash-table :test 'equal) (and (setf (gethash current-bank seen-before) 1) seen-before))
     (iterations 0 (1+ iterations)))
    ((gethash current-bank seen-before) (list iterations current-bank)))

					; part 2

					; just figure out the duplicated input and rerun part 1
(setf *day-6-input* '(0 13 12 10 9 8 7 5 3 2 1 1 1 10 6 5))



;; day 7

(defvar *day-7-input*)
(setf *day-7-input* "pbga (66)
xhth (57)
ebii (61)
havc (66)
ktlj (57)
fwft (72) -> ktlj, cntj, xhth
qoyq (66)
padx (45) -> pbga, havc, qoyq
tknk (41) -> ugml, padx, fwft
jptl (61)
ugml (68) -> gyxo, ebii, jptl
gyxo (61)
cntj (57)")

(setf *day-7-input* "keztg (7)
uwbtawx (9)
mgyhaax (46)
fuvokrr (14) -> pnjbsm, glrua
cymmj (257) -> phyzvno, pmfprs, ozgprze, bgjngh
goilxo (80)
cumfrfc (102) -> yjivxcf, swqkqgz
yquljjj (20)
ehywag (18)
mmtyhkd (21)
paglk (98)
wtqfs (82)
oaynkf (8)
cupbfut (78)
vpcruoy (70)
wmdbo (50)
tmbtipi (48)
lkopm (9)
gluzk (18)
prvrg (76)
lkdkyk (30) -> oldwss, nadxwf
iqsztjd (181) -> hovelvz, pndcqot, naglm, oxxlsk
nxdkpuh (217) -> yhcsc, ydmeqtl
nxlhjq (306) -> hcwjxe, zixbap
vtkgj (89)
rzrzage (73)
ftegwk (284)
lircjh (23)
zosskdz (232) -> isrch, bwzvefg, dxodoee
dphcbfr (67)
pnmvk (180) -> wrabgy, vlfpuo
owmjbhg (120) -> szfxhin, czzpk, zwrfiyf
oonqts (26)
zjaqq (129) -> hopjmyt, cdwkezv
hxeoxk (33)
csaqixs (1237) -> alzipi, lhxycw, tkeuvp
avenz (7)
nnhknbl (55) -> owzwbpn, iaonkp
bxifcld (86)
neyeo (165) -> gxxzwq, fxwez
qnjpz (71)
qhxmh (61)
jmhfgr (139) -> ucuqxgm, hovhxsp
tyuhzom (80)
pqtboz (207) -> ayvns, codwosk
dqyjg (65)
nujppls (24)
mxbixyi (60)
xkzgz (85)
oxklzu (2285) -> ehwlw, fptoo, sgobq, eduwet, pqmpnzo
fuleuxt (6) -> ljzuyyk, pxydes
zktmxll (451) -> txsrez, ewjrko, drtrgwp, kiggy
qpxbow (40)
rshpnha (36)
pqmpnzo (1374) -> hpltoci, oxvwr, vrxeemw
wdazzdu (54)
kivcyus (53)
cvvncju (10)
dtkuik (36)
opkvs (64)
kwjnfg (28)
suoiohi (197) -> gluzk, fdhdpw
jkaxk (98)
zsoro (12)
fqvtm (15)
nqktjw (14)
cbhkkx (116) -> wrprrev, vyoxx
rmqdolg (55)
mdkes (95)
obxansb (343) -> uzyprc, uxaqq
pjitmnv (31)
vdkrvi (73)
nystxqv (35)
odgzsnk (73)
hehbbo (83)
zrthre (30)
zwoot (9)
mfawmsq (92)
sckaqs (1141) -> qpaei, cbhkkx, qezwkkx
vxfci (60) -> tdecuga, wssvxr, pchccgz, ypogtw
vauwilt (78)
qxkas (24) -> mzgyj, xappjar, cgbgm, muarkn
eqibqs (20)
wuefg (549) -> pwwyeqx, ylidl, qwbfod, mqztoa
jmchsu (77)
dinng (30)
nlpmbrd (37) -> fnzjtvw, qzjyi
yjivxcf (19)
yhjopn (34)
hqxdyv (17568) -> apktiv, ybekxtf, etoxfc
wvivxrz (82)
wszqat (85) -> eddmyv, edkwqih, mxbixyi
xcldsl (78)
rnbzlx (35)
hibxz (94) -> zgzsu, vzgsgk
rgyco (21)
lmvvs (22)
nezny (13)
tpcvq (251)
puxgb (15)
merrako (8)
nzweur (431) -> rfouw, sukktk, rreqg, fmpcnql
pwwyeqx (100) -> fmjhlia, yquljjj
lccoo (27) -> bmlid, prvrg
dudxud (202)
cmnzh (49)
nitvw (8)
dcakuo (21)
nnbty (81)
kjlxeat (318) -> nmhww, zacsvwk
frinpfj (88) -> lwfoqny, tdgel
zgqsgm (38) -> phdcpp, qwcrc
bvikij (91) -> obxansb, bizbsjd, usggwvu, zrhny, svngfr
dnycw (219) -> sibzrx, hdgvs, wnfqg
youpfn (38)
zixbap (47)
mpwldri (55)
rfqenw (80)
tjtzx (78)
zbfut (55)
eruxzoi (63)
nandmg (344) -> qpqplm, zrthre
bizbsjd (159) -> piecd, ghkdvw, caurb
jpkwter (19) -> mcjgfx, ujsbt
uwpbnv (83)
devljb (45)
euzztul (30)
frmbrb (1660) -> norkse, iitweo, mebwy, sckaqs, xdrge
duccgc (15)
bmlid (76)
clwwv (238) -> jofvyvx, zgjoaiw
llmmm (69) -> wvivxrz, pikvdx
rstdh (21)
afckjn (51)
ojqia (22)
qtlzten (155) -> fjgsw, uujpt
eqxgwfz (40)
ljwsi (20)
vuxbzm (48)
qzuwt (130) -> qnrjqj, bjdtdn
lnnwiq (20)
pbxpdo (281) -> vabmsx, kwjnfg
bfcdy (31)
ykpsfj (28)
uegcs (210)
qezwkkx (74) -> gquil, stfzaxc
jkvduo (44)
vtylgti (66) -> twgbxu, uukshmq, rlvggmr, aynpr
eyyokyd (28)
nrbcaqo (45)
caocs (35)
cfdpxpm (207) -> jhwmc, nezny
qwjmobb (28425) -> ohusizx, gqoxv, xatjlb
akniuo (129) -> zpczji, tugrmnp
kravhjd (17)
wtbwbpz (43) -> kybegv, qxhda
hovelvz (31)
jhwmc (13)
wctze (102) -> qwdhdrk, znooxvq, vhrxl, zfhkfwn
nmhww (43)
fqufwq (58)
zyxjg (81)
eltbyz (61)
ehtsbv (783) -> upaqlj, cckqr, pgprg, ubksf
zbrbb (12)
vjgvk (28)
mqulwk (15)
kywmnbd (404)
wcuvk (20)
ymfls (75)
spxwcuv (173) -> iobcvl, xwfbb, wxpauwt
eumzi (24)
kqoigs (53) -> krfgye, oxklzu, pinipk, ojatf, memkrd
alneqju (77)
joczsir (313) -> xwkoc, atkmjxg, gurxxfd, axxkh, jmpknjs
uwzvy (35)
xqxyx (386) -> avenz, keztg
erylwj (804) -> wdsbi, ugrhs, fzmaw
corfkob (87)
sibzrx (67) -> pqccp, audeogd
crrfxfn (38)
piecd (72)
bxefs (22)
lnufi (93)
qifuph (44)
uqccsbh (26)
jaqwzi (79) -> zkgoa, juymjz
nivpffu (169) -> tnccv, lfqca, sgfco, nnbbrbf, egsgwch
dfrkf (49)
cdrhm (56)
vaylgz (80)
ayvns (23)
mdddafe (56)
fpldxlq (195) -> gjnnmvb, ljwsi
eygaz (427) -> ascyv, kmjfxcf, puxgb
ymeelep (92)
iuzvl (23)
tkjeu (41)
xdlyd (75) -> cymmj, pbxpdo, vmjbgo, cwttq
jsltf (39)
ciojx (146) -> ioobamp, ahrfot
eqmeu (211) -> anrjxof, nepxnu, mwpbyo, rbzqabo
bogvr (202) -> zghrr, bompiu
jefztzv (91)
fvikm (80) -> zepvwyv, oonqts
zdqcu (194) -> ucbuez, nqktjw
pdvolf (75)
mkmci (40) -> ggaxx, xvzlrw
sqmfis (35)
chrqi (74)
tvgytpm (49)
bjoyw (29)
nkfvkp (62)
xbtswv (7446) -> iehfo, xcrkb, qksclw
qomxhp (721) -> agufw, djgzb, jxbksoh, twnfzz, ucxgom
ibmiu (9)
atmzoso (6) -> drosj, wcrrrlf
fuqvw (56)
jfaca (49)
yulga (213)
dxodoee (7)
gethyvd (39)
hjxcpi (30)
jlcgqt (55)
lzouo (144) -> ubjgijo, rnbzlx
djyxrkb (78)
bscpyic (61) -> nktmu, sqpdsk
sxmdnhl (31)
qbdafi (25)
vwbxg (35)
rlvggmr (63)
kiggy (27) -> pgsokae, ottiad, eruxzoi, zhttn
ocpngbz (73) -> hqzay, ewzryd, ipjbc, xjnhqlg
movmxq (216) -> kfxhl, ulpkj
mzgyj (85)
orwbdwn (52)
ixyqeq (6735) -> xgwjcx, nkkgyl, sykwd
htsjndf (211)
ndhsa (82)
jmpknjs (80) -> gdrcfwr, wgivp
hieel (65)
htdwe (25)
qrfuvjh (9)
ubjgijo (35)
vobnpuq (32)
elgyjo (141) -> ineoncq, pfdmmg
zsnge (71)
zbwxa (28)
ogczchc (31)
njdpm (53)
cpsce (84)
ftdylco (19)
zrwfi (22)
hyfuy (252) -> pmscqw, ecaph
nayudfl (320) -> ssvsso, zrwfi
kykfb (72) -> euzztul, vxrtejs
ggpjxwv (9)
aostqf (29)
zujltb (13) -> hatvlca, ppmrgga, cjoya, bogvr, gtbpbl, ocwkc, qzuwt
ajbtn (18)
fzmaw (20) -> qjixqo, fkhxkeg, uqccsbh
akmbqb (108) -> kqkzsm, grgsn
slrdn (55)
nrjwctg (96)
norkse (661) -> jmhfgr, anwxvv, ptwhbm, znubct, djrrc, hgmjvpp
bwzvefg (7)
peuadz (8)
kvmqsdx (308) -> zvtoom, twvdhg
bvnjiou (32)
lnwuqu (159) -> yeqnq, fqvtm
pksfx (54)
xatua (97)
tbrznk (37)
ucuqxgm (31)
hwjhf (78)
pinipk (7229) -> pqewl, zujltb, kfcowx
yyhzd (12) -> kvqspmf, dtkuik, wsvir
imyvlyt (38)
oldwss (96)
paegovu (86)
knjlz (83)
oevbo (23)
yeucm (98)
usvkq (56)
hatvlca (69) -> gpogy, eyrfvtl, subwna
qkhtsa (208) -> pyxgmtu, pqgpuv, pudnxf, dilqo, juqbdco, flufot, fzdyvo
gzvjxk (1397) -> wctze, zoijv, fuleuxt
bkwcwf (326) -> jazkpl, kfcgv, qpjctjw
vqghhbs (157) -> lsrvhoi, livmxo
fzdyvo (186) -> gwfrqr, tcgffi
rqkfkxw (47)
bntdh (76)
rfmiqz (158) -> omlwg, uhwvnbg
zorvsm (50)
edzgraw (83)
iwsknb (345) -> qhqnfsp, vrwkr
vyoxx (63)
livmxo (27)
foabep (92)
dbuccip (28)
oojme (73) -> ugkxkqe, keucu, zjqeu
cmmqbz (29868) -> frmbrb, ixyqeq, njatvu
syjvwzt (6039) -> bigkiu, wjipa, pmbnia
uxaqq (16)
vboha (79)
irsfgz (94)
lsrvhoi (27)
zdtvktq (99) -> ezrix, lyhfj
hgbkwjv (32)
inxivh (261) -> mzzxjcu, rkaxx
nfrtom (44)
xhzylb (97)
nuxqskl (39)
qhfxqrr (65)
foaayon (78)
rhtdtxv (234) -> rplxsw, fjqomax
kppxrk (73)
qjixqo (26)
sgngx (75)
ycsbsyn (87) -> gmeueu, wdhmsi, zrqqtx
kqkzsm (61)
irjvpam (39) -> ajozeez, xxlbk, nfwlplx
ldfofo (84)
mygcpku (84)
caurb (72)
gpysit (22)
gxfij (171) -> sxnsqj, ksxixz
mxbyg (39)
jlshk (29)
havdbe (132) -> dqyjg, wvdapsm
ocwkc (86) -> dtzws, fleszz
rqrtz (83)
eddmyv (60)
ecaph (56)
ebgsk (60) -> rshpnha, puexzf
lymsa (44) -> qcyypa, vbdcxx
mozvs (54) -> dfrkf, cmnzh
kpghxz (17)
wrpqf (83)
dzjsx (57)
vrzbmt (14)
dryngd (29) -> shawokt, elkflt, tjtzx, auiiuv
ittmm (28)
zqurr (284) -> fqufwq, htsuyvw
ktejrze (69)
pudnxf (192) -> aostqf, szwpt
tqjlm (74)
codwosk (23)
pmscqw (56)
cmcto (441)
edoycls (93) -> vvnzr, imnvt, vuxbzm, rhzco
zrhny (177) -> dcwfs, dppwsec
vffew (46)
hcwjxe (47)
dinlw (342) -> zzjuf, lnwuqu, gxfij, kmqurp
lgtpaqk (75)
fluwt (65)
syuoewb (288) -> uwbtawx, avjkgl, nkycb
anrjxof (6)
vrwkr (5)
pngubp (76)
vvnhe (89)
rnlaw (45) -> wszqat, ptizsk, mofyda, ttolm, velktz, nzsnkla, hhdzz
nadxwf (96)
sxpan (31)
gdrcfwr (71)
vabmsx (28)
tsiyp (52)
fptoo (1398) -> gintpbf, cklkizf, kjgtfqc
obxrn (756) -> yyhzd, afatio, uqjge
znkchkk (50)
hvapnf (121) -> siruccf, tgpxvyr
htsuyvw (58)
yifny (122) -> dcakuo, xhtzti
uvvqcxz (79)
suiyl (773) -> hjfwn, thknml, gkijw
shawokt (78)
ezdiq (37)
khrqmbo (9)
gkjbikb (79) -> nrjwctg, iamrpx
ysskib (76)
zufoz (93) -> smrvw, kkkjsil
uusiaqf (84)
cjxyt (69) -> deczzuu, ymeelep
gcefq (33)
tgebda (247) -> ngthpc, bnvlsm, afckjn
ljmve (55)
rqdcuf (144) -> xzxvwzf, bjoyw
sjxaxv (76)
jiybx (88)
wxpauwt (20)
clsbdm (10)
fmpcnql (56) -> rjvfwcu, xjjdapk, nwtlu
hchmn (258) -> bremy, vpcruoy
labnsw (157) -> wnwsbdq, jqjkgv
hvpcvdf (95)
xatjlb (40) -> aiunbee, keoaqjb, jxkofob, plbtdq
dflvw (86)
vmcgj (233)
jxwyzy (13)
mebwy (1107) -> uegez, dpiyhv, vdzuw, icpwoof
btqebbp (156)
mmvszxj (71)
gpysm (85)
dppwsec (99)
iodveqh (89)
xcycu (116) -> dnkiyf, njdpm
ijictm (38)
ineay (19)
hvdpzbv (73)
cckqr (148) -> rqhfhc, jcyciyq, jbqvpsb, uapwikr
opowpvm (265) -> aewie, phrume
ptwhbm (69) -> omzlzx, suxpc
bpdixmv (83)
mzlmr (43)
ezrix (79)
jovly (61)
audeogd (99)
hjfwn (168) -> wtqfs, uamje
wdhbym (184) -> pksfx, bgahvfu, krlep, pkmmfc
nmmtdv (83)
uftwml (50)
ycyyvdy (17)
dqdpop (53) -> ggllv, zatwq
egqmgr (37)
txsrez (99) -> iiznokx, dobmve
lqmdutk (25)
mfecx (33)
kldww (23)
dxbxha (135) -> uvswv, ydmrd
xmvdh (41)
ctinwus (96)
arkedwb (237) -> qsvckew, merrako
iaonkp (89)
uthjdye (124) -> awoedy, wuisqnk, qsfpaj
yzbccbx (36) -> fxemb, osryil, nkfvkp
xvlymgg (33)
wbbaxr (10)
wcrrrlf (98)
bclse (38) -> slgjon, tzltv
krzjli (33)
dnkiyf (53)
cklkizf (219)
pqccp (99)
bgahvfu (54)
gotqku (33)
vregap (88)
qjcqm (138) -> vdkrvi, qoijc
phocd (23)
lerycoe (84)
aidbql (35)
qwvmczr (187) -> youpfn, kkukqoj
achhc (30)
qxnkp (68)
znkzp (94) -> otpnx, drtxytc, ntonira, zyxjg
ewzryd (92)
xxssj (168) -> nhnrpz, rzwfp
lnaoiv (70) -> awqat, dnaoe, qombesj, eygaz
qzjyi (88)
aevhbim (49)
jeuzbj (44)
msskvkg (60)
douvhy (133) -> mqulwk, nnkmf
qrfgzm (552) -> lrtds, yrpat, tvjwhhy, atmzoso, dudxud, rqdcuf, xxssj
alankuj (75)
juymjz (77)
gcqaj (69)
ozwpmzc (43) -> bknogxz, ldfofo
gintpbf (59) -> byckty, tagyci
yrhid (222) -> phocd, wtzkvm, kldww, xugyewm
keucu (15) -> kwozg, xfmxo, rwepl
zicelk (67)
xwwxswj (40) -> frnhsjr, mmvszxj, qsmvtif
lcfyznt (151) -> klmvmt, bgmmb
ehwlw (55) -> tgebda, ehypcwy, xqxyx, vxfci, nxlhjq
uegez (150) -> rnjfcg, tusgzei
pjaxkr (92)
mdprv (179) -> paanydj, vtaejs
avjkgl (9)
sshfxeu (11) -> goilxo, ltichp
alzipi (53) -> cpsce, ezspcab
wxoqoa (100) -> mfvul, pwatre
trmwq (10)
ltmwbs (33)
kmdzlmk (235) -> mzauq, ycsbsyn, srqclhj, shhfy, xcycu, zsnatp
dfjvh (61)
zknesz (39)
ezglz (289) -> kyjgf, oaynkf
pqixzdw (128) -> nvvca, cwxgqrk, duccgc, sbnsoxi
zpczji (60)
rathd (135) -> rsbune, wmdbo, uftwml
pchccgz (85)
rzwfp (17)
umgzhmp (150) -> eqibqs, rwgpi, wcuvk
ouqtjz (127) -> ejkei, sdgdv
sstvew (52)
qxhda (96)
cvstod (96)
rbyiay (94)
vwzmkq (63) -> vhjcue, nnbty
dtzws (83)
nyepy (363) -> jsltf, nuxqskl
iiwkm (84) -> fluwt, clcixal
ucezr (10)
zvtoom (55)
fleszz (83)
hdtcizc (9)
wzatr (57)
teqswj (40) -> advhon, qhfxqrr, hieel
usuaiv (45)
eygdy (86) -> pqtboz, kmodn, dqklqo, tukyh
janrdqf (81)
jjukf (723) -> vqghhbs, ozwpmzc, htsjndf, wsupp
udmez (91)
lsvqox (1205) -> tpcvq, fvqvgw, jusfet
zjymq (33)
wuisqnk (34)
pxydes (62)
qegmu (84) -> hzcanxq, xfbosce, glyxk, fvlmvtk, akmbqb, lymsa, fbrwdf
mckrfxj (38)
keylghg (60)
dqwdx (215) -> hseaxj, ubxke
fzjgh (179) -> bafdzbu, uanvh
qsvckew (8)
pvhrim (52)
mzauq (98) -> sxpan, pjitmnv, ogczchc, aruczxj
nxwjvx (53)
agufw (229) -> wbbaxr, knrgg
urpnw (47) -> khtjh, dflvw, otxphme
klmvmt (17)
skgwztg (233)
ghaxvq (9)
sgobq (60) -> lsspa, srgmt, wofung, rathd, joaed, fzjgh, pbkfd
qaveutv (44)
dnaoe (80) -> vohepl, mymke, paglk, pahwxj
edgdvfa (23)
mrfmn (94)
hovhxsp (31)
hqlyg (47) -> aidbql, tfhij, krvyy
zuybvj (32)
kquxfy (1294) -> srcyajr, neako, vtuqq
htsuvhg (253)
tagyci (80)
ldnnoag (35)
qkougo (76)
fvmhrf (38)
spvcd (84)
tcbpqu (70)
yzxqp (76)
ihmqs (66) -> oydsj, qheany
neako (48) -> panao, fcufg
dkgmsse (18)
zupym (33)
pwumvgy (80)
ognax (83)
oxvwr (203) -> wixlvcp, wqgcqb
ysltqk (65) -> mrfmn, hrjgbc
dtqzu (61) -> tgnqn, corfkob
vmhwy (177) -> yaudo, eqxgwfz
pwatre (16)
uijtrw (247) -> cbdczg, idjhk
stfzaxc (84)
refya (15)
xfbosce (188) -> rstdh, nqzwt
kbrxrks (56)
fplihc (88)
fkoqh (251) -> ohvifiy, npckah
lwfoqny (61)
paanydj (39)
xzxvwzf (29)
bpebim (180) -> vvnohc, jlyaty, kixaco
aiunbee (1214) -> fpldxlq, eqmeu, teqswj, ngtkzm, yndyrey, wtbwbpz, eoqtf
wrdgs (220)
oxxlsk (31)
atlrd (269) -> dgszhd, dkgmsse
svtwviu (69) -> iiwkm, ieuwo, carbhvi, lzouo, cklpcr, fkkzg, ouubjrl
mfvul (16)
ndpwgdy (36)
rhzco (48)
nzetqt (8)
xvzlrw (87)
utsob (14)
rsbune (50)
tcgffi (32)
cwttq (9) -> mughfl, ponlfyf, tuyyte, ndhsa
wejvpzr (29)
ozgprze (20)
eemnlc (83)
omlwg (28)
hpmuqvl (37) -> gawck, yeucm
ohusizx (9151) -> fsokbvd, oojme, bonjgrt
mqllnxu (206)
cnqrxk (38)
qpjctjw (32)
wttpvzh (23)
tgnqn (87)
eyrfvtl (61)
mzzxjcu (27)
velktz (145) -> dxzlkz, keylghg
hvdhkw (210)
zafde (41)
qmwvc (558) -> hibxz, xqbvg, bgjzy, mvswhtp
knrgg (10)
xrbuyn (38)
wsupp (35) -> ecgpjx, jiybx
krlep (54)
gvajgxp (40)
yaudo (40)
prdkf (96) -> chrqi, tqjlm
suxpc (66)
pnmfw (51)
jldaz (64)
rucse (85)
ivcedgz (46)
zytvsav (21)
tytka (53)
pfdmmg (11)
hmumqsz (47)
cncicpd (93)
rrixk (27)
uzyprc (16)
ljctbd (13)
llgoq (76) -> rgwpu, movmxq, hvifpbk, hiiqp, kszkv, bjtza, hfttss
gqoxv (1646) -> bvikij, dcqpq, kquxfy, qrfgzm, qomxhp
apktiv (8441) -> obxrn, kqaksir, itjrw, supnaxw, wuefg, twtcx
nicjj (56)
nzmaui (18) -> wmxobe, eipmjyb, jovly, tjblqzk
ezspcab (84)
bclrfac (31)
jrcng (318)
pbkfd (157) -> pcdvdp, jldaz
vmqlqrp (46)
ynvqpm (92)
rnjfcg (20)
qhqnfsp (5)
ajozeez (92)
shciqu (127) -> ehywag, ajbtn
uujpt (29)
znooxvq (7)
mwblvo (257) -> ixmfiwz, jlcgqt, rmqdolg
rrazh (233) -> clsbdm, uavnq
ipjbc (92)
jovkydi (89)
rkfsa (102) -> pestqep, vnvkvb
owzwbpn (89)
cragbdx (19)
vtpldh (19)
rplxsw (82)
cbvamfw (47) -> wdnebh, vpqqbz
vhrxl (7)
cqvyvl (122) -> eemnlc, hehbbo
nmxmtaa (188) -> bvahtih, asklr
kfcgv (32)
pztxq (339) -> pqbar, bclrfac
gwfrqr (32)
dcqpq (1411) -> dqdpop, gtrgqb, lcfyznt
ksxixz (9)
itjrw (9) -> jjjpzm, mhofo, znljf, wrdgs, dcknzvh
omwrb (859) -> pqixzdw, hoqtxuf, weenw
rwgpi (20)
xoqxg (73)
rdyda (5)
dolng (83)
osvmh (77)
pqewl (185) -> hchmn, rhtdtxv, xzgejmu, fumvuu
dcknzvh (138) -> pdviq, hnpndnp
vjmwqzj (35)
zhttn (63)
lyuys (284) -> zicelk, joernlg
jjjpzm (164) -> vjgvk, yaiinhu
siruccf (37)
tzkuvl (87)
jpbiodh (10)
jodrf (56) -> lktgac, ysskib, qkougo
egvza (251) -> gotqku, viqlepb
jdpcmb (6)
ejlgch (133) -> mmoea, vvbcb
ecgpjx (88)
pdviq (41)
igqvq (288)
pndcqot (31)
kmjfxcf (15)
rqhfhc (12)
cgbgm (85)
jjmfi (33)
vpizq (37)
zynpfpv (73)
vnyllno (37)
sfyad (69)
sooqm (69)
dxdltnx (80) -> xtexo, ohpvt, acjtzxw, pnmfw
gcmpmnt (166) -> stqwvs, xmgkswu
jwgrqmj (44)
gcksx (73)
uswphji (1472) -> nzweur, pocxcw, xdlyd, joczsir, kymbpe, rhwgdsv, omwrb
lhifvp (49)
sgfco (305)
wqgcqb (12)
rgjxp (116) -> nujppls, eumzi
omzlzx (66)
jeplz (96) -> gpysit, gzmagb
bgjzy (78) -> ibmiu, ghaxvq, lkopm, khrqmbo
kvqsn (69) -> bpdixmv, knjlz, nmmtdv, edzgraw
ugkuxzz (314)
pryjaeo (21)
jzpwsg (78) -> paegovu, bxifcld, oijrg, wyftg
flufot (182) -> ftoskhn, afzlar
cmqaaw (83)
fvqvgw (26) -> ymfls, lgtpaqk, cumus
gurxxfd (46) -> qaveutv, bwtusip, jeuzbj, qifuph
jqtlcm (380) -> njeahp, sfyoyp
bywpbd (215) -> qrfuvjh, nrczybn
pcpnk (28)
pnjbsm (69)
lhxycw (161) -> achhc, ttwnws
qczhlyf (14)
sldytqh (49)
fmjhlia (20)
soqvass (53)
qptfj (21)
izuebwg (37)
iaafo (1335) -> iggtk, gkjbikb, gcvpqk
bkzdepq (56)
nqzwt (21)
frnhsjr (71)
nfwlplx (92)
egsgwch (279) -> jxwyzy, sbnot
ntonira (81)
ahrfot (9)
zjbiqnt (54)
tpozmfd (27)
jopepd (27)
kkukqoj (38)
panao (88)
szsntp (88) -> uvvqcxz, vboha, xgzexud, aaqcpt
lfqca (65) -> kieka, rfqenw, srbuc
ttwnws (30)
iobcvl (20)
gmeueu (45)
jazkpl (32)
carbhvi (118) -> vxrzo, ujlhns
jlyaty (11)
jwbxyd (75)
mlmzsqc (78)
sqpdsk (86)
gkijw (268) -> hgbkwjv, stmzb
sebxl (52)
anwxvv (59) -> qnjpz, kbibi
cylfwm (81)
sbnot (13)
cdcnp (5)
sohzsx (80)
gfmgl (47)
cbdczg (47)
uamje (82)
phrume (20)
jhpijl (185) -> mojjl, yhjopn
czzpk (17)
mwpdil (37)
lizssx (47)
otxphme (86)
vvnohc (11)
xmusk (10)
pqgpuv (210) -> lnnwiq, qqosg
iqumjrz (19)
khtjh (86)
sqfgpm (41)
qpqplm (30)
pqlav (8)
nzhprt (93) -> sohzsx, vaylgz
iehfo (624) -> fcsfrg, cbvamfw, dtqzu
vhfwgvx (83)
tbkhho (97)
uqymxu (58) -> egqmgr, izuebwg
xzgejmu (188) -> cwiwn, vtadj, tcbpqu
gjnnmvb (20)
ppmlbky (544) -> kvqsn, kcgui, chetpy, pztxq
jqqojdl (33)
qksclw (69) -> inxivh, dxbxha, irjvpam, syuoewb
axxkh (222)
tnccv (259) -> wttpvzh, edgdvfa
cumus (75)
jxkofob (1875) -> mrizfl, zgqsgm, bclse, ciojx, rgjxp, yifny
uydtye (8)
tuyyte (82)
twgbxu (63)
ulpkj (40)
lunefek (35)
pestqep (60)
kvxnjmq (25)
siitl (61)
chutuh (13)
gtjbuae (71)
fbrwdf (17) -> jzjrtvd, gtjbuae, zsnge
tukyh (71) -> ggzym, rfvehkx
xqpffoe (68) -> elthna, mwpdil, vpizq
iepmwr (176) -> irsfgz, rbyiay
lntikva (47)
kmodn (253)
ufsmed (81)
psrqv (54)
xluwv (30)
qombesj (380) -> rdghpyb, ivcedgz
shhfy (76) -> zynpfpv, kppxrk
fvlmvtk (130) -> ilpuzq, jzndr
gdqtqg (155) -> lhifvp, jfaca
osgjrb (69)
uqjge (88) -> nzetqt, nitvw, wxemcge, uydtye
ppafb (30)
opduu (38)
fpank (10)
sngnjdu (15)
ndidblx (52)
asklr (19)
wmxobe (61)
vvnzr (48)
usggwvu (375)
sukktk (202) -> oevbo, sitjrw
mqztoa (26) -> dzjsx, wzatr
kfxhl (40)
jsjexer (258) -> ljctbd, chutuh
bremy (70)
xugyewm (23)
djrrc (39) -> egcat, wdazzdu, zjbiqnt
juqbdco (144) -> vggsia, tytka
rfouw (28) -> yxzzaaq, zbfut, mpwldri, ljmve
svngfr (83) -> odgzsnk, rzrzage, lbzddgt, hvdpzbv
viqlepb (33)
pgsokae (63)
cobwyky (76)
zgjoaiw (25)
ohvifiy (6)
simmjy (65) -> mlksi, mfawmsq
zrqqtx (45)
yrpat (120) -> zlkzyr, zafde
nevkec (53)
gvxhl (125) -> liymk, psrqv
ohpvt (51)
kmqurp (155) -> ycyyvdy, slmxb
acrsmhw (99) -> cncicpd, tiwakam
yrdsj (149) -> mmtyhkd, omshqjl, pryjaeo, zytvsav
pahwxj (98)
nhnrpz (17)
mfzvywf (14)
fhphiyb (45183) -> ixktgj, zfzum, yzyzmzw
eduwet (1794) -> tzkuvl, jcmte, rndomrv
akrgqe (74)
bzjctqm (115) -> dphcbfr, sauzcee
xqbvg (104) -> cdcnp, wixfh
ljzuyyk (62)
egusv (173) -> qpxbow, gvajgxp
grmhpg (75)
gtbpbl (252)
fkdukrv (331) -> zbrbb, zsoro
aondpve (53)
vgsnxlm (23)
hcptw (44)
zkgoa (77)
dasyq (66)
skumcr (55)
afatio (30) -> hjxcpi, qmddtb, ppafb
ioobamp (9)
wrprrev (63)
nrczybn (9)
hoxdnl (32)
ndcfy (19)
tdecuga (85)
ugqqv (70) -> akrgqe, aeawrmy, ozjbwv
ravjm (83)
tsizsce (52)
uyopxvt (56)
djgzb (79) -> gpysm, rucse
ozbfh (64)
rwepl (73)
vkfrqct (56)
mansh (22)
bnsivw (336) -> vobnpuq, hoxdnl
omshqjl (21)
lktgac (76)
hgrua (66)
vrxeemw (49) -> vvnhe, nyejrrv
ydfajlk (88) -> qgjqyh, alneqju, jmchsu
ewjrko (167) -> kbrxrks, cygdj
oeftx (49) -> bpebim, sdbmop, nlpmbrd, yulga, qtlzten, hidrx
ypogtw (85)
eoqtf (97) -> ktejrze, osgjrb
cbbcj (46)
dvlyc (16) -> arkedwb, nzhprt, zjaqq, cjxyt, gdqtqg, jhpijl, egusv
tvjwhhy (120) -> tkjeu, vaeuo
cahxjyq (52)
muarkn (85)
zzjuf (95) -> rqkfkxw, lizssx
cygdj (56)
vlagh (5)
vohepl (98)
pgprg (152) -> jqdmk, bxefs
yrwyc (5) -> ocpngbz, ybfew, nyepy, cmcto
asfqik (180) -> orcyokv, tuyzi
jcyciyq (12)
twnfzz (149) -> vbateme, olydb
jofvyvx (25)
tazfdb (43)
jcmte (87)
ttolm (173) -> vmqlqrp, vffew
fndeqk (27)
xydxd (56)
elthna (37)
uspimc (85)
zatwq (66)
mwpbyo (6)
joaed (205) -> ycxlhqo, mgzox, nmemj, ljmkz
vtadj (70)
ilpuzq (50)
ubksf (106) -> nrbcaqo, shnrfq
mmnejdx (10)
srqclhj (90) -> jqqojdl, krzjli, ltmwbs, mtrnyd
rpegyn (23)
nzsnkla (265)
zfhkfwn (7)
hrjgbc (94)
stqwvs (28)
etkdg (38)
fumvuu (86) -> xcldsl, memrqz, ryfse, vauwilt
uavnq (10)
yaiinhu (28)
mlksi (92)
oyguh (32) -> ipxkky, lqmdutk, hrtgwt, kvxnjmq
ceeem (25)
pqbar (31)
aeawrmy (74)
fcufg (88)
hjnhdkp (27)
xmxzvhr (14) -> cvstod, jxcnmmo
mketjaw (45)
srbuc (80)
qqosg (20)
ynrctr (140)
kbibi (71)
lalfu (50)
iamrpx (96)
xgfsqq (309) -> qwvmczr, ladcna, fkoqh
dobmve (90)
nvvca (15)
vtaejs (39)
xgzexud (79)
tmvkuob (15)
foxjx (60)
ladcna (97) -> hbdwzm, cmqaaw
klnew (68)
vtdwe (37)
eayhoi (58) -> tvgytpm, pvlirjn
nccoxir (52)
wibfie (90) -> uqymxu, oyguh, wxoqoa, alomcle, fvikm, kykfb, ebgsk
yhcsc (34)
ptzbutd (38)
xmgkswu (28)
ajhio (301) -> jjmfi, mfecx, zjymq
lasluxv (87) -> etkdg, mckrfxj
msyvug (76)
weenw (188)
drtrgwp (185) -> hmumqsz, lntikva
gyojd (214)
zwrfiyf (17)
gquil (84)
erghvss (45)
mqmgirb (251) -> buffpzu, qpwiok
xaali (31)
clcixal (65)
pyxgmtu (169) -> hjnhdkp, jopepd, fndeqk
grgsn (61)
finkdao (78)
njeahp (10)
hidrx (175) -> ineay, vtpldh
qcyypa (93)
gnwlorj (48)
xjjdapk (64)
vxrtejs (30)
uukshmq (63)
hzcanxq (214) -> peuadz, pqlav
ptizsk (10) -> cbirs, syurke, ihpmp
cjoya (196) -> pcpnk, dbuccip
qpaei (86) -> djyxrkb, cupbfut
tjblqzk (61)
xjnhqlg (92)
qscpjx (39)
xptjtlm (108) -> utsob, lmxnloe, vrzbmt
rgwpu (161) -> xjuoid, oszlto, mketjaw
erdxj (299) -> mdprv, zdtvktq, labnsw, vmhwy
stmzb (32)
vxjjxhz (49)
xxlbk (92)
fxwez (7)
hbdwzm (83)
bjdtdn (61)
wnfqg (99) -> ravjm, dolng
sauzcee (67)
nkfrt (98)
rfvehkx (91)
fnzjtvw (88)
wdhmsi (45)
slmxb (17)
dkoxq (128) -> hvpcvdf, mdkes
qheany (80)
lpziczm (229) -> xydxd, fuqvw
gzmagb (22)
bgjngh (20)
krfgye (51) -> gzvjxk, dvlyc, niwzp, olqbic, iaixlte, tycqst, kfqrgj
dcwfs (99)
ixktgj (955) -> eabhh, iqvhov, lemnz
mbtsi (300) -> gbixdw, lalfu
dqklqo (57) -> jkaxk, nkfrt
qwsan (44)
iggtk (235) -> gyuyb, cjjbqxl
ascyv (15)
gtrgqb (17) -> coaznz, spvcd
awoedy (34)
mtrnyd (33)
wofung (133) -> ptzbutd, jlpxjeo, cnqrxk, crrfxfn
dpiyhv (86) -> tsizsce, gfdyf
surri (214)
tusgzei (20)
kzcyjtb (78) -> sqmfis, ppfmc, ldnnoag
memrqz (78)
javiioo (87) -> gnwlorj, tmbtipi
dzqrtc (206) -> zbwxa, ykpsfj
jtpgzp (31)
yndyrey (189) -> iuzvl, poinih
hkazlkt (235) -> adxeaf, rkfsa, yzbccbx, lkdkyk, gcmpmnt, zdqcu
bjqkaya (78) -> szsntp, nandmg, kjlxeat, kywmnbd
guywt (55)
qwcrc (63)
asdlfku (80)
vhjcue (81)
hiiqp (148) -> lhvyfsb, dltungt, tbrznk, vnyllno
deczzuu (92)
mymke (98)
subwna (61)
kmcad (1022) -> bzjctqm, akniuo, simmjy
uufxdt (328) -> hyfuy, iepmwr, asfqik, qxkas, nayudfl
ejkei (89)
afzlar (34)
buffpzu (33)
kyjgf (8)
bigkiu (748) -> uegcs, umgzhmp, frinpfj, nurclfr, hvdhkw
ryfse (78)
olztzl (25)
tgkbxa (33)
nktmu (86)
zlkzyr (41)
pmgwwzy (191) -> rgyco, qptfj
jfmts (56)
zacsvwk (43)
znljf (175) -> refya, tmvkuob, sngnjdu
kwozg (73)
eklqi (90)
cdwkezv (62)
ugmlsij (121) -> kpghxz, kravhjd
awqat (372) -> fdmbkt, znkchkk
ydgzjs (110) -> ymyzead, yzxqp
tugrmnp (60)
fmarl (5688) -> xbtswv, jvqwi, slgiv, uswphji, syjvwzt
memkrd (7478) -> nivpffu, bjqkaya, qegmu
jvfmwp (27)
qhqyt (66)
lbzddgt (73)
qsmvtif (71)
ytmpzku (49) -> jwgrqmj, nfrtom, lfiqwye, jkvduo
tdgel (61)
shnrfq (45)
hhrqz (129) -> rrixk, jvfmwp
ojatf (9518) -> qmwvc, wibfie, dnycw
yxzzaaq (55)
wjipa (1180) -> mqllnxu, xmxzvhr, qbynkmw
cwxgqrk (15)
aotajs (133) -> ucezr, mmnejdx, jpbiodh
lsspa (60) -> paldf, grmhpg, exjsjxd
ineoncq (11)
eipmjyb (61)
fjgsw (29)
wnwsbdq (50)
hdgvs (37) -> msyvug, bntdh, sjxaxv
npckah (6)
swqkqgz (19)
eabhh (379) -> jzpwsg, bkwcwf, mwblvo
ggaxx (87)
rjvfwcu (64)
bwtusip (44)
slgjon (63)
vlpqaxo (66)
mmoea (11)
xnltw (93)
tgpxvyr (37)
phdcpp (63)
dltungt (37)
ljvrcj (83)
upaqlj (44) -> cobwyky, pngubp
hwriv (97)
hopjmyt (62)
sizmuwa (180) -> sooqm, sfyad
jzndr (50)
ppmrgga (222) -> cvvncju, xmusk, trmwq
uvswv (90)
xstrla (46)
pcdvdp (64)
kszkv (164) -> hgrua, qhqyt
niwzp (855) -> nnhknbl, ichznto, llmmm, hpmuqvl
kybegv (96)
rxcbsdp (30) -> ynvqpm, pjaxkr
ihpmp (85)
hseaxj (51)
fuzhuf (38)
plbtdq (633) -> gkuma, jrcng, dkoxq, vtylgti, qifoay, sizmuwa, pjxttn
esooc (151) -> vtdwe, ezdiq
jvqwi (33) -> csaqixs, rnlaw, hvkhvwl, rkpcoen, nptoou, ypvztl
luqyr (81)
ggzym (91)
fcsfrg (113) -> siitl, dkntzn
nnbbrbf (135) -> uspimc, xkzgz
cwiwn (70)
zragt (58)
glyxk (22) -> pvhrim, ndidblx, sebxl, sstvew
rhwgdsv (481) -> ugkuxzz, yyjqegu, yrhid
bmltf (23)
gfdyf (52)
cbirs (85)
xhtzti (21)
advhon (65)
xcrkb (864) -> ugmlsij, jpkwter, ejlgch
kfqrgj (347) -> igqvq, cqvyvl, jzgzd, ashxu, clwwv
ouubjrl (36) -> iodveqh, wanom
nptoou (269) -> yrdsj, bywpbd, lmcqig, skgwztg, suoiohi, gvxhl, rgdvmy
jqdmk (22)
ljmkz (20)
dxzlkz (60)
svhqwim (9)
nurclfr (30) -> usuaiv, zyjxpjz, devljb, erghvss
vdzuw (146) -> ostrl, lmvvs
fsokbvd (262) -> owmjbhg, sshfxeu, zufoz
cdvkf (6)
lrtds (202)
ybfew (377) -> zuybvj, bvnjiou
lfiqwye (44)
htbjl (57) -> czdpe, gcqaj
cjjbqxl (18)
vucfp (53)
bnvlsm (51)
ybekxtf (10703) -> erylwj, xgfsqq, eygdy, dinlw
ashxu (96) -> ctinwus, ypdpmwi
qpwiok (33)
nrdlfop (84)
sbhiqhs (27) -> qxnkp, klnew
ujlhns (48)
ostrl (22)
vmjbgo (155) -> udmez, jefztzv
rkaxx (27)
pocxcw (823) -> blhgy, xptjtlm, kerjqr, ekfewi
gkuma (134) -> kvyjyt, foabep
ugrhs (32) -> mansh, hpkvfr, ojqia
gbixdw (50)
phkthld (75)
mofyda (109) -> finkdao, mlmzsqc
lxucxpm (73) -> iytknc, qhxmh, eltbyz, dfjvh
qgjqyh (77)
ucxgom (249)
mmufeg (13) -> gcksx, oybdjt, xoqxg, masykz
yexxg (319)
bvahtih (19)
lesmi (204) -> qwsan, hcptw
pmbnia (94) -> jsjexer, qjcqm, ftegwk, dxdltnx, negkd, jodrf
knmac (36)
hnpndnp (41)
qoijc (73)
lemnz (60) -> egvza, mqmgirb, dqwdx, whglbyy, lxucxpm
ihtfh (116) -> fplihc, vregap
lmxnloe (14)
whglbyy (201) -> vnkqnhh, zragt
vbateme (50)
inxav (77)
sykwd (860) -> jeplz, sttlom, ynrctr, cumfrfc
aaqcpt (79)
mwpatsx (64) -> nrdlfop, lerycoe, mygcpku, uusiaqf
uanvh (53)
pvlirjn (49)
kerjqr (150)
wrabgy (32)
xjzpum (148) -> vjmwqzj, uwzvy, vwbxg
tycqst (1112) -> ytmpzku, vwzmkq, esooc
gxxzwq (7)
knjbw (27)
hpkvfr (22)
xappjar (85)
oybdjt (73)
qmddtb (30)
krvyy (35)
mughfl (82)
hoqtxuf (74) -> xrbuyn, imyvlyt, fvmhrf
xtexo (51)
bknogxz (84)
rreqg (144) -> nccoxir, cahxjyq
icpwoof (172) -> wxhsqe, svhqwim
kszyl (113) -> sqfgpm, xmvdh
zyjxpjz (45)
ipxkky (25)
jxcnmmo (96)
ghkdvw (72)
cwphzk (25)
mgzox (20)
gcvpqk (143) -> ozbfh, opkvs
bompiu (25)
ckisc (103) -> alankuj, phkthld
ngtkzm (67) -> vkfrqct, usvkq, jfmts
qifoay (318)
rkpcoen (1788) -> uyopxvt, bkzdepq
vtuqq (30) -> tbkhho, xhzylb
ekfewi (96) -> knjbw, tpozmfd
paldf (75)
zghrr (25)
ggllv (66)
vlfpuo (32)
sdgdv (89)
osryil (62)
puexzf (36)
smrvw (39)
ozjbwv (74)
naglm (31)
aewie (20)
etoxfc (59) -> uxdeg, hcceg, ppmlbky, uufxdt, qbcscs, llgoq, iaafo
wxemcge (8)
xwkoc (63) -> ijcfw, aondpve, nevkec
byckty (80)
kvyjyt (92)
pkmmfc (54)
sxnsqj (9)
wtzkvm (23)
kfcowx (352) -> edoycls, pqdfbrl, nxdkpuh, acrsmhw, rrkqend
ngthpc (51)
nkycb (9)
otpnx (81)
jusfet (39) -> kivcyus, nxwjvx, vucfp, soqvass
mvswhtp (28) -> tazfdb, mzlmr
exjsjxd (75)
ugkxkqe (159) -> qbdafi, htdwe, olztzl
pikvdx (82)
vnvkvb (60)
sitjrw (23)
yyjqegu (94) -> guywt, skumcr, whoqvq, slrdn
qnrjqj (61)
szfxhin (17)
iiznokx (90)
vbpyoqo (97)
qwdhdrk (7)
iqvhov (120) -> atlrd, mmufeg, ouqtjz, xgitr, ezglz
wdnebh (94)
ylidl (82) -> wejvpzr, jlshk
qsfpaj (34)
qbcscs (1680) -> yazsie, eayhoi, btqebbp
auiiuv (78)
ixmfiwz (55)
hcceg (1083) -> dkoxwi, iwsknb, fkdukrv
kqaksir (152) -> ydfajlk, tyvhfhz, yexxg
wixlvcp (12)
tfhij (35)
vnkqnhh (58)
dkntzn (61)
oggnstj (279) -> ydgzjs, havdbe, dzqrtc, nzmaui
yzyzmzw (583) -> yrwyc, suiyl, kmcad
hfttss (5) -> xatua, hwriv, vbpyoqo
kieka (80)
czdpe (69)
vxrzo (48)
mpmue (9)
ujsbt (68)
oijrg (86)
vaeuo (41)
qbynkmw (114) -> mgyhaax, cbbcj
srgmt (165) -> msskvkg, foxjx
ycxlhqo (20)
pmfprs (20)
zbsxee (35)
ypvztl (1363) -> xqpffoe, neyeo, lccoo
hwrwnh (90)
szwpt (29)
mhofo (66) -> inxav, osvmh
masykz (73)
gawck (98)
drtxytc (81)
wyftg (86)
ssvsso (22)
koeqohh (275) -> zupym, hxeoxk
kcgui (335) -> kguwwze, gcefq
olqbic (911) -> ihtfh, ugqqv, lesmi
sttlom (62) -> zknesz, gethyvd
acjtzxw (51)
supnaxw (194) -> iqsztjd, urpnw, opowpvm
jzjrtvd (71)
rbzqabo (6)
keoaqjb (59) -> jqtlcm, wdhbym, mwpatsx, mbtsi, bnsivw, zqurr, ajhio
kkkjsil (39)
jzgzd (188) -> zorvsm, ainstbs
iitweo (96) -> ckisc, ysltqk, zosskdz, xjzpum, rrazh, htsuvhg, xwwxswj
xyzbni (58) -> xnltw, lnufi
rgdvmy (53) -> hwrwnh, eklqi
hhdzz (227) -> cragbdx, iqumjrz
mcjgfx (68)
jlpxjeo (38)
zfzum (16) -> lnaoiv, qkhtsa, lsvqox
njatvu (26) -> ehtsbv, hkazlkt, tvgwgq, zktmxll, jjukf, kmdzlmk, svtwviu
wdsbi (98)
ieuwo (196) -> hdtcizc, ggpjxwv
tuyzi (92)
dkoxwi (35) -> pwumvgy, asdlfku, aedcmjb, tyuhzom
ichznto (221) -> cdvkf, jdpcmb
tyvhfhz (179) -> nystxqv, caocs, zbsxee, lunefek
uxdeg (443) -> eewnnkx, dryngd, lpziczm, koeqohh, uijtrw
egcat (54)
kvqspmf (36)
ydmrd (90)
syurke (85)
tkeuvp (159) -> jtpgzp, bfcdy
ypdpmwi (96)
nmemj (20)
eewnnkx (341)
bafdzbu (53)
wgivp (71)
tvgwgq (835) -> prdkf, pnmvk, xyzbni
slgiv (7452) -> oeftx, erdxj, oggnstj
gpogy (61)
xdrge (889) -> shciqu, aotajs, douvhy, lasluxv, sbhiqhs, elgyjo
vbdcxx (93)
oydsj (80)
pjxttn (168) -> jwbxyd, isymgjd
edkwqih (60)
wanom (89)
hqzay (92)
xwfbb (20)
phyzvno (20)
glrua (69)
ijcfw (53)
alomcle (112) -> fpank, rfwvc
lmcqig (71) -> ufsmed, cylfwm
ltichp (80)
tzltv (63)
thknml (89) -> luqyr, janrdqf, wkxkv
zsnatp (118) -> orwbdwn, tsiyp
zoijv (130)
aynpr (63)
isymgjd (75)
pqdfbrl (117) -> nicjj, cdrhm, mdddafe
bonjgrt (319) -> fuvokrr, hqlyg, mozvs
vggsia (53)
ucbuez (14)
wixfh (5)
dgszhd (18)
joernlg (67)
elkflt (78)
lhvyfsb (37)
liymk (54)
xgitr (139) -> vhfwgvx, rqrtz
wxhsqe (9)
wssvxr (85)
bgmmb (17)
ehypcwy (334) -> xvlymgg, tgkbxa
qwbfod (68) -> ndpwgdy, knmac
nepxnu (6)
ftoskhn (34)
ponlfyf (82)
atkmjxg (162) -> xluwv, dinng
rrkqend (223) -> xaali, sxmdnhl
sbnsoxi (15)
fxemb (62)
sfyoyp (10)
lyhfj (79)
nnkmf (15)
xfmxo (73)
dilqo (84) -> ljvrcj, ognax
jxbksoh (157) -> xstrla, ykudi
fkhxkeg (26)
wsvir (36)
chetpy (363) -> ndcfy, ftdylco
iytknc (61)
hvifpbk (202) -> mhkxu, gfmgl
gyuyb (18)
whoqvq (55)
twtcx (524) -> htbjl, kszyl, hvapnf
wkxkv (81)
ainstbs (50)
ppfmc (35)
hvkhvwl (1351) -> hhrqz, kzcyjtb, javiioo
yeqnq (15)
aruczxj (31)
poinih (23)
kymbpe (745) -> ihmqs, uthjdye, nmxmtaa
kixaco (11)
oszlto (45)
ydmeqtl (34)
mojjl (34)
adxeaf (212) -> rdyda, vlagh
xjuoid (45)
rndomrv (87)
mhkxu (47)
znubct (69) -> dasyq, vlpqaxo
fdmbkt (50)
coaznz (84)
caxbgqp (23)
uhwvnbg (28)
orcyokv (92)
zgzsu (10)
xgwjcx (22) -> cfdpxpm, pmgwwzy, bscpyic, vmcgj, jaqwzi, spxwcuv
vpqqbz (94)
wvdapsm (65)
uapwikr (12)
olydb (50)
kjgtfqc (69) -> pdvolf, sgngx
cklpcr (58) -> hwjhf, foaayon
ottiad (63)
bjtza (250) -> caxbgqp, rpegyn
fdhdpw (18)
jqjkgv (50)
kguwwze (33)
blhgy (100) -> cwphzk, ceeem
aedcmjb (80)
drosj (98)
jbqvpsb (12)
isrch (7)
vzgsgk (10)
fjqomax (82)
twvdhg (55)
vvbcb (11)
yazsie (78) -> qscpjx, mxbyg
nkkgyl (166) -> kvmqsdx, znkzp, lyuys
mrizfl (108) -> ittmm, eyyokyd
fkkzg (145) -> vgsnxlm, bmltf, lircjh
zepvwyv (26)
idjhk (47)
hrtgwt (25)
iaixlte (717) -> surri, mkmci, rfmiqz, gyojd, rxcbsdp
ubxke (51)
imnvt (48)
hgmjvpp (35) -> wrpqf, uwpbnv
tiwakam (93)
rfwvc (10)
zjqeu (120) -> fuzhuf, opduu, ijictm
ymyzead (76)
veboyvy (61) -> qwjmobb, fmarl, hqxdyv, fhphiyb, cmmqbz, kqoigs
nwtlu (64)
rdghpyb (46)
srcyajr (77) -> vxjjxhz, aevhbim, sldytqh
hpltoci (199) -> qczhlyf, mfzvywf
sdbmop (35) -> vtkgj, jovkydi
nyejrrv (89)
negkd (266) -> zwoot, mpmue
ykudi (46)")

					; part 1
(defun root-with-children (line)
  (multiple-value-bind (_ matches) (cl-ppcre:scan-to-strings "([a-z]+) \\((\\\d+)\\)( -> )?(.*)" line)
    (let ((root (elt matches 0))
	  (children (cl-ppcre:split ", " (elt matches 3))))
      (append (list root) children))))

(let* ((parsed-lines (map 'list #'root-with-children (cl-ppcre:split "\\n" *day-7-input*)))
       (all-nodes (map 'list #'first parsed-lines))
       (all-children (apply 'append (map 'list #'rest parsed-lines))))
  (set-difference all-nodes all-children :test 'equal))


					; part 2

(defun root-with-weight-and-children (line)
  (multiple-value-bind (_ matches) (cl-ppcre:scan-to-strings "([a-z]+) \\((\\\d+)\\)( -> )?(.*)" line)
    (let ((root (elt matches 0))
	  (weight (parse-integer (elt matches 1)))
	  (children (cl-ppcre:split ", " (elt matches 3))))
      (append (list root weight) children))))

(defun total-weight (node-name children-table weights-table)
  (+ (gethash node-name weights-table)
     (apply '+ (map 'list (lambda (child-name)
			    (total-weight child-name children-table weights-table))
		    (gethash node-name children-table)))))

(let* ((parsed-lines (map 'list #'root-with-weight-and-children (cl-ppcre:split "\\n" *day-7-input*)))
       (weights-table (make-hash-table :test 'equal))
       (children-table (make-hash-table :test 'equal)))
					; populate the weights and children tables
  (dolist (entry parsed-lines)
    (let ((node (first entry))
	  (weight (second entry))
	  (children (nthcdr 2 entry)))
      (setf (gethash node weights-table) weight)
      (setf (gethash node children-table) children)))

					; manually walking the tree: veboyvy hqxdyv apktiv obxrn
					; (1109 - 1116) + (gethash "obxrn" weights-table) -> 749
  (map 'list
       (lambda (child) (list child (total-weight child children-table weights-table)))
       (gethash "apktiv" children-table)))

;; day 8

(defvar *day-8-input*)
(setf *day-8-input* "b inc 5 if a > 1
a inc 1 if b < 5
c dec -10 if a >= 1
c inc -20 if c == 10")

(setf *day-8-input* "gug dec 188 if zpw >= 8
pnt inc 428 if xek < 10
qt inc 274 if d == -8
wv dec 661 if gug > -1
kw dec 518 if zpw > -8
kw inc -894 if vk <= 6
pnt dec 503 if vk == 8
kw dec 644 if fs == 0
vk inc 880 if pnt == 428
sg inc 242 if kmh <= 9
gug inc -853 if kmh < 3
d dec 518 if oa < 1
u dec -948 if ar == 7
qt dec 871 if pnt < 433
qt dec -875 if kw < -2065
d inc -644 if kw == -2056
j dec 521 if qnf == 9
yl inc 705 if pu < 2
pnt dec 150 if d < -1156
fs inc 731 if afx >= -7
fwk inc -417 if qt != -874
ll dec -677 if on == 0
fs inc 249 if vk != 872
on dec 125 if vk < 882
gug dec -805 if vk != 880
kw dec 104 if kw == -2056
on dec 476 if pnt == 278
g dec 939 if bl == 0
qt inc 245 if afx < 6
wv dec -529 if u > 4
on inc -751 if j != 5
qt dec 662 if fs > 977
afx dec 861 if yl == 705
qt dec -110 if rre > -7
d dec 199 if qnf != 3
xek dec 167 if j >= -4
fwk dec 641 if oa > -1
gug dec -799 if kw < -2150
on dec -445 if bl >= -1
fs inc 822 if pnt > 274
fwk dec -493 if j >= 10
pnt inc 315 if vk <= 882
zpw dec -166 if qt >= -1178
fs dec -47 if j > 7
wv dec -325 if oa > -3
kw dec -641 if on < -899
pu inc 911 if pnt <= 594
bl dec -2 if xek > -162
on inc -922 if d < -1354
u dec -400 if vk >= 875
sg dec -376 if kw < -1518
afx inc -22 if kw != -1519
zpw dec -788 if pu != 910
zpw dec -847 if oa == 0
yl inc -418 if kmh >= -1
ar dec -279 if g >= -946
kw dec -472 if vk < 885
g dec 766 if pnt <= 583
wv inc 159 if on >= -1823
d dec 140 if on == -1819
vk dec -160 if rre != 0
afx inc 748 if rre <= 8
u dec 78 if qt > -1181
d inc -970 if g == -939
j dec -91 if zpw != 1801
afx inc 141 if u >= 332
fs inc -564 if qnf > -6
bl inc -937 if on != -1834
ll dec 146 if pu < 919
ar inc -307 if d < -2325
sg inc 370 if xek < -168
ll inc 314 if bl <= -942
sg inc -537 if qt >= -1183
zpw inc 435 if vk < 877
bl dec 725 if g != -939
yl inc 493 if pu != 918
u dec 976 if kmh >= -1
pnt dec 150 if ar < -22
vk dec 734 if g >= -944
wv inc 270 if sg != 90
afx dec -764 if sg >= 82
rre dec 694 if zpw != 1796
fwk inc -885 if kw > -1049
bl inc -567 if afx < -106
on dec 195 if xek >= -168
wv dec 393 if vk <= 154
bl dec -517 if ar == -36
kw inc 669 if wv >= -453
u inc -220 if fwk > -1948
pnt dec -624 if kw < -1049
kw inc -820 if g != -937
ar inc -91 if zpw != 1811
bl dec 490 if qt == -1178
vk dec 599 if j < -2
u inc 719 if qnf >= -8
on inc -24 if ar > -126
bl inc 634 if sg >= 73
j inc -256 if kw < -1859
pu inc -656 if d == -2331
gug inc 295 if afx != -107
bl inc 228 if afx < -109
d dec -300 if ar <= -117
qt dec -749 if qnf > 7
d dec -369 if sg > 75
g dec -839 if wv != -460
xek inc -48 if fs <= 1241
pu inc 273 if yl > 773
ll dec -317 if g < -99
qnf dec 687 if on > -2049
bl inc 763 if vk == 146
fwk inc -925 if gug <= 245
zpw inc 994 if oa <= 4
pnt dec 697 if ar <= -120
fwk dec 277 if fs <= 1242
on dec -508 if pu >= 527
ar dec -261 if qt == -1178
yl dec -566 if qnf >= -691
pu inc -867 if j < -247
yl dec 653 if rre >= -693
oa dec 258 if kmh < 8
xek inc -388 if fs > 1242
g inc 11 if d == -1662
gug inc 512 if bl < -369
kw inc 456 if ar != 144
gug dec 540 if bl >= -375
oa inc -447 if fwk >= -3154
g inc -100 if pu >= -346
g dec 175 if vk != 146
yl inc 31 if fs <= 1244
kmh dec 508 if d >= -1653
rre dec -197 if xek >= -221
j inc 258 if bl > -377
g dec 574 if kw >= -1415
pu dec -514 if j != 3
j dec 168 if vk <= 141
wv inc 964 if zpw < 2802
zpw dec 541 if pu <= 180
ar inc 233 if kw < -1410
g dec 445 if fwk == -3145
vk dec 537 if pnt < 445
gug inc 578 if qt < -1175
rre inc -539 if j <= -2
afx inc -12 if qnf > -695
vk dec -881 if g >= -1209
sg inc 936 if pnt < 448
afx inc -386 if xek > -216
g inc 325 if afx == -511
pu dec -26 if j <= -4
zpw dec 308 if ll < 841
xek dec 62 if qnf != -681
ar dec -27 if pu > 180
fwk inc -863 if pnt >= 438
on dec 256 if sg == 1017
qnf dec -563 if ar != 383
wv dec -970 if afx <= -507
vk dec -15 if sg < 1023
ar inc 398 if ll < 858
ar dec 630 if u > -157
oa dec 465 if fwk > -4012
zpw dec 554 if fs >= 1229
g dec -548 if d <= -1654
oa inc 719 if kw >= -1416
pu inc -429 if qt <= -1172
qnf dec -183 if on == -1796
yl inc -46 if u <= -162
qt dec 19 if ll < 855
sg inc 476 if vk != 502
wv dec 204 if qt >= -1201
ar inc -332 if u != -161
wv dec -656 if oa < -447
on inc -866 if rre <= -491
on inc -615 if kw >= -1416
xek inc 641 if xek >= -285
qt dec -204 if yl != 1385
xek dec 328 if on <= -3275
d inc -504 if ar != -180
qt dec 801 if yl > 1376
fs dec 124 if d != -2176
u dec -602 if zpw <= 1709
fwk dec 845 if pnt == 443
j inc -110 if kw != -1409
u dec -19 if pu != -263
g dec -859 if ll == 848
zpw dec -132 if zpw != 1695
g inc -94 if on != -3268
qt inc -654 if gug != 279
pu inc -111 if zpw >= 1825
fs dec 252 if pnt > 436
gug dec 933 if gug != 280
pu dec 899 if fwk > -4857
fs dec 163 if gug <= -648
pnt dec 829 if bl == -369
bl inc -490 if ll > 844
zpw inc -46 if sg >= 1486
fs dec -121 if qnf > 54
fs inc -516 if gug == -654
qnf dec -799 if fwk > -4858
fs dec -431 if ll == 856
oa dec 159 if fs >= 304
bl inc 713 if kmh != -9
oa inc 933 if pu > -1266
on inc -394 if yl >= 1376
zpw inc -920 if fs == 304
xek inc -573 if on == -3677
xek dec -909 if sg == 1493
g dec 918 if fs < 304
pu dec -502 if ll > 844
vk dec -401 if qt > -1800
sg inc 729 if g >= 427
oa inc -468 if yl != 1387
kw inc 879 if xek >= 937
ll inc -121 if pu < -756
oa inc -868 if pu > -767
sg inc 392 if pu > -770
on dec 335 if ll <= 736
oa inc 394 if afx == -511
bl inc 893 if ar < -187
fs dec 592 if yl != 1377
j dec -53 if kmh < -5
d dec 432 if pnt > -396
fs inc 175 if j == -116
on inc 750 if kw >= -526
fwk dec 821 if qnf != 863
fs inc -185 if pnt == -386
kw dec 914 if kmh > -2
kmh inc 482 if afx != -502
zpw dec 281 if pu >= -753
pu inc 774 if wv < 1919
d dec 549 if g == 430
bl inc 259 if yl > 1374
d dec -439 if d <= -3143
rre dec 79 if g == 430
pnt inc -630 if kw <= -1439
wv dec -137 if on < -3997
zpw dec -622 if pu < -756
kmh inc 87 if j >= -116
xek inc -772 if pu <= -764
j inc -696 if ll > 735
sg dec 655 if vk == 906
ar dec 368 if bl != 1006
vk inc -130 if u >= 457
kw inc -240 if oa > -618
gug inc 880 if gug > -655
qnf dec 985 if vk == 783
j dec 817 if qnf >= 859
ar dec 4 if ar < -181
qnf dec 958 if on != -4008
vk inc -15 if qt < -1786
pnt dec -712 if on != -4011
u dec -326 if sg > 1949
kw inc -144 if oa == -619
zpw dec 690 if gug == 226
kmh dec -942 if on < -4011
fwk dec 357 if zpw > 800
wv dec 423 if qnf < -96
j inc 504 if on > -4012
rre inc -947 if fs <= 120
u inc 160 if u < 788
g dec -222 if d <= -2707
pu inc -994 if xek == 955
qnf dec 621 if xek != 953
qnf inc 991 if fs == 119
gug inc -123 if yl == 1378
gug inc 950 if yl < 1385
g inc -842 if kmh >= 565
sg inc 488 if oa <= -612
sg inc 347 if qnf > 277
ll inc -943 if u < 797
g dec 920 if rre != -1523
afx dec 471 if on <= -4007
wv dec -862 if u == 797
g inc -491 if pu != -766
wv inc -290 if fs > 117
ll inc -630 if pu != -771
bl dec 232 if g != -673
bl inc 21 if ll >= -836
kmh inc -816 if ll != -851
kmh dec -723 if zpw != 796
fwk dec 776 if d >= -2710
qnf dec -723 if sg < 2450
fs inc -276 if zpw >= 794
rre inc -748 if on != -4011
oa inc -81 if fwk >= -6457
xek inc -704 if on < -4003
sg dec 399 if qnf < 988
vk inc -557 if afx < -502
xek inc 378 if j <= 403
sg inc 509 if gug <= 1176
on dec -745 if d == -2708
g dec -323 if rre < -2271
qt inc 835 if fs <= -148
on dec 7 if qnf < 1000
kmh inc -818 if rre < -2266
gug inc 124 if rre >= -2272
kmh inc -844 if qnf <= 991
rre dec 618 if xek < 618
rre inc -159 if ar <= -187
wv dec 433 if pu > -767
afx inc -104 if afx != -510
kmh inc 322 if j >= 396
xek inc -641 if kmh != -29
on inc -721 if ar <= -190
g inc 835 if ll != -848
fs dec 431 if bl <= 767
ll inc -625 if kmh < -17
kmh dec -925 if j == 396
j dec 667 if g != 152
sg inc 289 if fwk > -6442
rre inc 522 if qnf > 992
pu dec -551 if zpw <= 805
kw dec 644 if pnt != -301
ar dec 93 if vk >= 198
qnf dec -243 if qt < -951
sg dec -148 if d >= -2713
qt dec -243 if on > -3981
kw dec -926 if qt <= -954
u inc -874 if pu <= -221
u inc 557 if vk <= 212
g inc -981 if fwk > -6455
kw dec -473 if qt > -952
rre inc -470 if on > -3981
qt inc -147 if g <= -822
fwk dec 185 if fwk <= -6442
rre inc -372 if vk > 194
afx dec -136 if oa != -700
pu inc -921 if j < -265
g inc 615 if yl <= 1377
j inc -592 if j < -267
fwk inc -25 if yl >= 1372
oa dec 47 if qt != -1108
fwk inc 524 if g >= -206
ll inc 38 if ll != -1478
fwk dec 433 if g <= -211
sg inc -541 if d >= -2701
vk dec 222 if zpw == 798
qt inc 683 if vk > -19
wv inc -359 if u <= 1349
xek dec 547 if yl == 1381
qnf inc 933 if wv != 566
on inc -103 if j <= -869
sg inc 765 if fwk > -7087
u inc -731 if pu >= -1133
qt inc -535 if j != -867
yl inc -97 if yl < 1378
g dec -128 if wv > 557
zpw inc -159 if d > -2713
oa dec 558 if kmh > 911
kmh inc -425 if kmh != 906
vk inc -199 if u != 620
fwk dec 134 if d < -2699
sg inc -660 if xek < -14
gug inc -881 if xek >= -31
kw inc 664 if on >= -3998
ll dec -723 if d < -2705
pu inc -376 if kw != -644
qnf dec -798 if zpw > 632
kw dec -603 if qt == -958
xek inc 135 if kw >= -42
kw dec 890 if d == -2713
vk inc -74 if pnt < -303
wv dec -451 if fwk > -7237
afx dec -848 if kmh == 480
rre inc -70 if on != -3989
yl dec 336 if qt != -960
ll dec 16 if zpw > 634
ll dec 644 if kw >= -43
yl dec 692 if gug <= 420
kw dec -118 if kw <= -43
j dec -583 if u < 622
ar dec 957 if j >= -277
qnf dec 313 if g <= -75
gug dec 662 if vk < -297
g dec -45 if oa >= -750
pu dec -99 if qnf != 2654
ll dec 183 if j > -287
u inc 138 if qt == -967
gug inc -667 if vk > -293
j inc -154 if j != -272
afx dec 573 if on > -3991
qnf inc 357 if vk > -298
d dec 500 if afx != -332
g dec -599 if d <= -3203
zpw inc -443 if gug > -249
bl inc 771 if pu > -1137
sg inc 215 if xek == 113
vk inc -827 if ll == -1551
bl inc 220 if xek >= 113
ll inc 981 if ar == -286
gug dec -292 if kw < -33
ll dec 483 if qt <= -968
fwk inc 804 if u >= 609
fwk inc 596 if qt != -956
pnt dec -543 if rre == -2280
pnt dec 362 if rre < -2280
sg dec 118 if d != -3206
vk inc 527 if xek >= 121
sg inc -49 if xek >= 111
j dec -270 if d <= -3210
sg dec 704 if d <= -3199
kmh dec -82 if xek == 113
d dec -217 if sg != 1787
u inc 711 if afx <= -335
j inc 694 if xek > 110
vk dec 745 if j >= 252
fwk dec -192 if afx > -342
d dec -441 if oa >= -751
fwk inc -312 if zpw > 199
d inc 925 if fwk <= -5630
ar inc -500 if vk > -1034
bl dec -411 if wv < 1014
bl dec 281 if j != 267
pu dec 992 if fwk != -5635
ar inc -630 if sg >= 1780
fs dec -580 if pu <= -1124
fs inc -755 if bl != 1900
bl inc 294 if rre < -2273
pnt dec -780 if qnf < 3016
rre dec -608 if fs >= -328
kw dec -637 if vk <= -1027
oa inc -247 if bl != 2185
vk dec 27 if gug == 44
qt dec 184 if bl > 2187
pu inc -867 if wv < 1015
oa dec 650 if qnf >= 3007
kw inc 694 if pu < -1989
fs inc 142 if pu == -1999
pu dec 344 if kmh == 562
rre inc -494 if sg < 1796
yl inc 579 if oa != -1654
pu inc -563 if ar == -916
wv inc -216 if qnf >= 3007
pu dec 692 if vk == -1063
fs inc -790 if zpw != 189
rre dec 705 if xek < 106
d dec 878 if u <= 1331
bl inc -666 if qnf != 3011
kw inc -780 if sg != 1785
vk dec -633 if bl >= 2182
xek dec 302 if wv < 801
ar dec 867 if afx != -348
on dec -997 if pnt != 1027
fs inc 915 if g != 552
on inc -139 if sg >= 1779
wv inc -719 if oa < -1640
qnf inc 140 if u != 1323
bl inc 882 if yl == 831
on dec 398 if oa <= -1646
wv dec 176 if pu == -3598
sg inc 824 if oa >= -1652
g inc -445 if pnt != 1023
oa dec -63 if fs > -75
xek inc 557 if pu <= -3597
afx inc 531 if xek >= 360
zpw dec -837 if u >= 1335
gug inc -955 if kw > 508
fs inc 600 if ar > -1784
yl inc -548 if kmh < 566
afx dec -66 if fwk < -5634
ar dec -109 if pnt < 1023
kmh dec -892 if d >= -2512
fs dec -434 if ll < -570
d dec -491 if d == -2503
bl dec -558 if oa < -1574
ar inc 719 if j < 263
on dec 44 if kw >= 506
d dec 988 if kmh <= 1460
zpw dec 253 if oa > -1589
oa inc 325 if u >= 1320
vk inc -270 if gug == -920
rre dec 52 if j != 262
fs dec 922 if fwk != -5629
g dec 711 if gug < -917
bl dec 255 if g < 125
qnf inc 119 if fs == 47
afx inc -108 if qnf != 3262
afx inc 10 if vk == -430
pu inc -928 if wv == -101
zpw inc 787 if zpw >= -66
sg inc -484 if xek == 368
afx dec -402 if kw != 506
j inc -256 if oa == -1256
rre inc -305 if sg == 2128
fwk dec -111 if u != 1320
kmh inc 359 if wv == -101
pu dec 806 if gug <= -906
gug dec 885 if ar > -961
rre inc 443 if afx != 561
qt inc 353 if pnt < 1022
vk dec -731 if ll != -577
ll inc -816 if j > -3
pu inc -807 if qt > -795
yl dec 805 if qnf == 3270
fs dec 419 if kmh == 1809
ar inc -100 if fwk > -5525
sg inc -111 if rre >= -3123
rre dec 452 if g < 125
ll dec -832 if gug > -1790
wv dec -695 if g != 124
fwk inc -373 if wv > 587
zpw inc -418 if g < 124
d inc -903 if j > 2
yl dec 115 if j != 13
ar inc -962 if fs <= 49
d inc -367 if kw <= 517
u dec 584 if qt < -783
on inc 69 if bl >= 3374
d dec -647 if vk < 308
xek dec 174 if kmh > 1803
oa inc 313 if kmh == 1813
qnf inc -309 if fs < 53
fs dec -554 if qt == -789
kmh dec 776 if fs == 601
wv dec -742 if kw == 515
fwk inc -414 if wv == 589
fs inc 504 if xek >= 187
kw dec 576 if j < 6
ll inc 639 if qnf <= 2964
oa inc 322 if fs != 1105
ll inc -533 if kw == -66
afx inc 864 if kmh >= 1029
qt inc 341 if kw <= -66
on dec 20 if bl < 3381
g inc 844 if fwk <= -5890
pu inc -226 if pu > -6145
kmh inc 633 if on == -3126
gug inc 803 if vk > 304
gug inc -625 if u >= 745
xek dec 292 if qnf >= 2955
d dec -480 if on == -3126
ll dec -259 if xek != -89
g inc 552 if zpw >= 308
vk dec 607 if yl == -637
qt inc 938 if fs < 1110
rre dec -813 if ll < -1016
oa dec -723 if rre < -2771
kw inc 376 if bl == 3374
pu dec 186 if on == -3126
fs inc 231 if fwk < -5894
rre dec 402 if sg != 2128
j dec -236 if fs < 1339
pu inc 540 if ar > -2022
rre inc -314 if fwk < -5900
u inc -563 if kw <= 317
afx dec 150 if on != -3127
ll dec -775 if qt <= 494
qt inc 707 if zpw < 315
bl dec 695 if fs == 1344
rre inc -943 if zpw > 305
j inc 275 if fwk < -5887
zpw inc 446 if fs < 1328
g dec -773 if zpw >= 309
on dec -376 if ll > -251
xek inc -406 if d == -3143
rre dec 500 if xek != -501
qt inc 875 if bl > 3366
pu inc 178 if bl >= 3373
gug inc -469 if afx != 1283
kmh inc -439 if bl < 3376
j dec -979 if oa <= -953
kw inc -824 if sg == 2128
pu inc 849 if d >= -3145
kw dec -654 if g == 2283
u dec -192 if j != 517
vk inc -540 if pu > -4993
d dec 474 if on >= -2754
rre dec 435 if sg > 2119
qnf inc 737 if on != -2750
wv inc -940 if oa < -945
oa inc 569 if fs >= 1329
ar dec -851 if kw < -509
kw dec 352 if d != -3619
on dec -301 if yl == -637
oa dec -520 if pnt > 1013
kw inc -914 if wv > 597
zpw inc -947 if sg != 2134
oa inc 703 if gug >= -2888
kmh inc -112 if u < 381
ar dec -100 if qnf > 2963
afx inc 919 if j == 515
u dec 15 if j > 514
sg inc 425 if xek == -504
afx dec -342 if j == 515
sg dec -20 if yl == -637
zpw dec -678 if pu != -4976
yl dec 212 if on < -2445
vk inc 140 if on != -2448
wv dec 850 if xek <= -495
j inc -526 if ll <= -245
zpw dec 790 if kmh <= 1125
qt dec -342 if afx == 2536
rre inc 137 if pnt != 1028
yl inc -548 if pu >= -4984
pnt inc 86 if fs > 1328
yl dec -253 if ll == -248
sg dec -276 if yl < -1145
pu dec -443 if pu >= -4986
afx dec -163 if fwk != -5902
xek inc -760 if vk > -711
wv inc 656 if kmh == 1119
yl dec 619 if u != 364
rre dec 203 if j >= -6
fs inc 319 if rre != -4502
kmh inc 394 if ar == -1166
qnf dec 263 if oa <= 155
pnt dec -670 if oa != 150
u dec -22 if pu >= -4549
yl inc -444 if kw != -863
fs inc 398 if qnf == 2698
fs dec -88 if qnf > 2689
kw inc 211 if rre > -4504
ll inc -46 if ll != -239
qnf inc 887 if ar != -1170
kmh dec 231 if sg != 2573
g dec 769 if yl >= -2212
ll dec -426 if qt > 2407
j dec 232 if kmh != 1520
g dec 796 if kmh != 1511
g inc 151 if g < 727
afx inc 910 if wv > 405
pu inc -847 if pu == -4549
ar dec 920 if u > 377
gug inc 648 if j > -248
fwk dec 603 if rre >= -4513
u inc -355 if zpw < -745
qnf dec -820 if d == -3617
on inc -85 if on >= -2452
yl inc -967 if d >= -3615
u dec -679 if d <= -3613
ar inc 199 if g >= 862
sg inc -148 if fs > 2140
kmh dec -747 if j < -241
kmh inc -598 if wv == 400
gug inc 783 if oa != 156
qt inc 234 if ll != 142
pnt dec -408 if bl >= 3374
on dec 978 if fwk <= -6498
fs dec 516 if zpw > -753
sg dec 968 if d >= -3626
wv inc 43 if g <= 873
j inc -335 if fs >= 1619
rre dec -388 if yl == -2207
fwk dec 363 if u > 699
zpw inc 9 if d != -3616
sg inc 925 if on > -3516
qt dec 603 if kmh > 1653
oa inc -402 if wv >= 440
g inc 906 if fs >= 1621
pu inc 876 if oa < -246
pnt inc -871 if kw <= -861
vk dec 538 if kw < -856
ll dec -831 if kw == -866
pnt inc 418 if d <= -3615
j inc -308 if gug == -1459
vk inc 574 if ll <= 969
zpw dec -733 if kmh > 1656
fwk dec -349 if fs == 1625
u dec -761 if bl < 3378
oa dec 115 if pu >= -3667
pnt dec 50 if vk > -674
qnf inc 663 if oa != -372
rre inc 383 if kmh <= 1670
fwk dec 126 if pnt > 1670
wv dec -199 if ar > -1893
ar inc -385 if pnt < 1674
d dec -814 if rre > -3745
wv inc 849 if j >= -888
pnt inc 934 if wv <= 1484
kmh inc -508 if sg < 2376
kmh inc 664 if yl != -2198
qt inc -409 if sg < 2385
gug inc 953 if u == 1466
yl inc -793 if oa != -366
xek dec 245 if ar < -1892
oa dec 942 if u == 1466
yl inc 935 if zpw <= 1
ar dec 91 if fwk < -6644
u dec -210 if kw > -864
sg dec -231 if pnt < 1683
pnt inc 825 if qnf == 5068
j inc -859 if vk > -671
ar dec -528 if kw <= -876
rre inc 120 if vk < -662
kw inc 150 if d == -2803
ll dec -984 if gug > -514
vk dec 415 if xek <= -1257
vk inc 813 if gug <= -501
on dec 866 if qt < 1633
bl inc -270 if qt < 1644
j inc -814 if bl != 3104
sg dec -856 if rre == -3620
gug inc 760 if oa > -1304
ll inc -982 if zpw <= -2
g inc -433 if oa < -1310
u dec 487 if g == 1343
pu dec -898 if ar < -1885
fwk dec 121 if u > 970
afx inc 790 if qnf < 5069
kmh dec -820 if d > -2805
pu dec 443 if pnt > 2502
on dec -777 if afx < 3498
fwk inc -99 if kmh != 3137
ll dec -997 if vk <= -264
qt inc -696 if gug > -512
fs dec 426 if j < -1741
yl inc -897 if ll < 1968
sg dec 350 if zpw >= -10
yl dec 124 if d != -2811
fwk dec -653 if pnt <= 2507
kw dec -737 if ll > 1956
sg inc 574 if u <= 983
u dec 942 if kmh < 3156
ll dec 604 if yl == -3086
u inc -995 if rre != -3610
sg inc 27 if kw >= 17
pu inc 586 if ll != 1349
pnt inc 400 if d != -2811
gug inc 660 if ar > -1890
bl inc -297 if afx >= 3491
ar inc 902 if wv > 1486
wv dec 158 if pnt != 2910
kw inc 755 if xek == -1266
g dec -66 if on > -2736
bl inc -905 if on != -2733
gug dec -50 if fwk >= -6211
rre inc -757 if g != 1399
ar dec 121 if wv > 1326
bl dec 155 if ll > 1356
pu dec -398 if ar <= -1113
gug inc -23 if d < -2801
sg inc -283 if oa != -1318
wv inc 959 if pnt != 2904
j dec 364 if ll >= 1357
on dec 195 if oa < -1310
xek dec 884 if vk <= -270
zpw inc 899 if d > -2810
kw inc -398 if fs > 1192
kmh dec 521 if pnt >= 2896
vk dec -937 if xek < -2142
pu inc 586 if qt < 946
qt inc -974 if sg >= 3434
fwk dec 236 if qt != -33
j inc 885 if vk <= 670
g inc 933 if vk <= 665
d inc 607 if wv == 2292
xek dec -823 if pu > -2034
sg inc 916 if ar < -1101
pu inc -983 if pu == -2038
oa dec 25 if ll == 1358
g dec 444 if zpw <= 899
rre inc 892 if g > 1890
vk inc -108 if vk != 665
zpw dec 506 if oa > -1345
yl inc 0 if zpw <= 396
pu dec 311 if xek < -2142
oa inc -148 if rre >= -3491
pnt inc -740 if sg <= 4344
qnf inc -632 if gug > 185
wv inc -947 if kmh < 2620
oa inc -741 if ar <= -1103
wv inc -234 if kmh == 2625
on inc -182 if kw == -377
u dec -98 if qnf <= 5070
g inc 553 if pnt >= 2904
u inc -717 if kw != -371
afx inc 174 if gug < 185
xek inc -922 if pnt < 2913
qnf inc -337 if oa >= -2235
xek dec 837 if fwk < -6439
gug inc -645 if gug < 190
ll dec 388 if xek != -3913
yl dec 707 if fs > 1206
bl inc -743 if kmh >= 2621
pnt dec 888 if j == -1234
ll dec 183 if pu < -3327
d inc 152 if xek > -3909
u dec -752 if rre >= -3479
j inc -436 if fs >= 1193
rre inc 450 if yl < -3086
kw inc 608 if pnt >= 2908
rre inc 761 if qnf >= 4736
gug dec -976 if qt >= -41
afx inc -24 if yl != -3091
qnf dec 827 if rre >= -3488
vk dec -778 if g > 2443
oa dec -590 if bl != 1310
bl dec 240 if vk >= 1449
qt dec 584 if j == -1669
wv dec -260 if j < -1651
fwk dec 280 if qnf != 3903
sg dec 174 if rre != -3487
vk dec 990 if wv <= 2322
oa dec 118 if ll == 787
fwk inc 754 if kmh >= 2625
kmh inc -697 if rre >= -3475
wv inc 21 if pu >= -3336
kmh inc -694 if qnf < 3907
bl dec -383 if qt <= -25
xek dec -201 if bl > 1681
rre inc -606 if kw != -383
qt inc -476 if g == 2451
kw dec 552 if wv != 2341
ar inc -991 if kw < -935
sg inc -405 if on != -3118
pu dec -239 if ll == 787
j dec 515 if gug < 510
rre inc 133 if vk < 459
qt inc 968 if qnf <= 3912
wv dec -131 if kmh > 1929
fs dec 482 if wv < 2474
rre dec -438 if j >= -1654
kmh inc 557 if u != -1577
fwk inc -234 if yl != -3076
vk inc 670 if j < -1652
oa dec 992 if kw == -929
on dec -42 if gug < 518
j inc 53 if wv < 2478
gug dec 996 if ar == -1112
zpw dec 844 if fs == 717
kw dec -785 if g <= 2443
u inc -929 if pnt <= 2907
yl inc 956 if fwk == -6203
gug dec -469 if fs != 722
qnf dec 584 if d > -2052
fwk dec 652 if wv > 2460
kw inc -115 if sg < 3782
qnf dec -866 if ll == 793
oa dec -10 if d == -2044
g dec 533 if ll == 787
bl inc -924 if rre == -3958
xek inc 53 if u == -2506
wv dec -950 if g < 1926
bl inc 83 if gug != 974
oa dec 582 if vk < 1129
u inc -664 if gug <= 990
rre inc 484 if ar < -1109
ll inc 531 if fs > 715
qt inc 642 if fwk == -6855
g dec -504 if bl < 843
j dec -422 if rre < -3961
vk inc -898 if qt > 1093
xek inc -28 if vk <= 230
zpw dec 113 if kw > -1046
on dec -182 if qt != 1095
oa inc -986 if ll == 1318
on dec 660 if afx == 3639
ar dec -912 if kw >= -1049
yl inc -375 if kmh != 1931
kmh inc 913 if xek != -3685
u dec 384 if bl < 836
on inc -151 if xek < -3675
qt inc -363 if fwk != -6857
kw inc 87 if pu == -3085
sg inc 598 if pnt < 2911
yl dec 839 if kw < -1042
oa inc -706 if kmh <= 2841
afx dec -488 if wv == 3420
vk dec -845 if qnf == 3320
fwk dec -658 if sg <= 4374
vk inc -490 if sg < 4375
qt inc -973 if ar != -189
qnf inc -395 if ar <= -194
pnt inc -224 if oa != -4300
d dec 926 if qt <= -233
on inc -776 if qnf >= 2921
fwk dec 249 if yl >= -2976
sg inc 406 if wv != 3424
vk dec 55 if g > 1914
fwk dec 919 if yl != -2977
u dec -752 if zpw > -579
xek dec -89 if afx != 4123
oa dec 633 if g <= 1924
xek dec -332 if j < -1606
gug dec 623 if u != -2428
zpw dec 618 if pnt > 2689
u dec 568 if pu < -3089
yl dec -59 if j != -1603
kmh dec 846 if sg == 4778
pnt dec 897 if g >= 1914
bl inc -753 if qnf < 2918
yl inc 237 if wv < 3417
yl dec 429 if ar > -202
oa dec 381 if pnt >= 1781
kw dec -876 if fwk == -7372
qnf inc 454 if kw == -1044
fs inc 338 if g == 1918
j inc -526 if j != -1607
vk inc 72 if qnf >= 3374
oa dec 674 if wv > 3412
pnt inc -383 if zpw != -569
yl dec 715 if d >= -2969
on inc 292 if wv > 3413
kmh dec 867 if sg != 4778
fs inc -460 if bl < 834
zpw inc -236 if kw <= -1043
sg dec 791 if zpw <= -796
qnf inc 304 if pnt < 1790
vk dec -800 if zpw == -805
rre inc -16 if u == -2986
gug inc 180 if kmh > 1990
bl dec 596 if d <= -2967
u inc 372 if fs == 1055
fs inc -618 if fwk >= -7369
rre inc 610 if on <= -4186
d dec 264 if ll <= 1326
zpw inc 933 if kw <= -1045
qnf inc 780 if kmh < 1997
d dec -354 if bl != 247
vk inc -338 if oa != -5999
ar inc -8 if rre != -3964
kmh inc -480 if j <= -1605
vk inc -568 if ll != 1319
qnf inc 155 if vk < 499
zpw inc -127 if u == -2614
pu inc -731 if vk == 491
vk dec 980 if zpw > -942
fwk dec 888 if zpw >= -938
wv dec -172 if j > -1610
yl dec 91 if on < -4177
bl inc -622 if ll >= 1328
wv dec 335 if on <= -4193
ll dec -983 if qt == -236
bl inc -749 if gug > 537
d inc -836 if on >= -4174
pnt inc -149 if pu != -3824
sg dec -952 if ll == 2301
rre dec 402 if rre > -3973
yl dec 808 if kw > -1040
oa inc -682 if u > -2618
fs inc -720 if pu <= -3827
qt dec -930 if kw != -1039
oa dec 243 if xek == -3260
kmh inc 773 if u <= -2610
ar dec -887 if fwk <= -8259
vk dec -282 if yl >= -3432
sg dec -532 if kmh > 2284
kmh dec -158 if zpw < -922
gug inc 486 if bl > -502
wv inc -944 if wv <= 3598
pnt dec 526 if afx >= 4120
d dec 732 if yl < -3426
fwk inc -107 if bl >= -502
g dec 943 if bl != -506
ar inc -29 if vk > -209
g dec -123 if vk == -207
qnf dec -981 if fs >= 429
ll dec -293 if fwk > -8365
u dec 929 if bl != -502
vk dec 185 if fwk <= -8357
j dec -26 if kw >= -1049
zpw dec 636 if qt < 697
bl inc -830 if vk == -392
fs dec -576 if ar > -229
qt inc -621 if afx > 4119
ar inc -97 if sg == 5471
zpw inc -998 if ar >= -331
j dec -892 if fwk <= -8354
bl dec 349 if bl != -1338
fwk dec 336 if sg > 5462
g inc -806 if ar >= -323
kmh inc 697 if kmh == 2449
zpw inc 205 if pu >= -3827
wv dec -630 if xek <= -3252
xek inc -226 if fwk < -8693
on dec 914 if on <= -4181
yl dec 943 if ar <= -326
gug inc -620 if kmh == 3146
qt dec 345 if j != -689
vk inc 149 if fwk < -8693
fs dec -238 if ll < 2603
oa inc 96 if pnt < 1259
oa dec 110 if g >= 1095
j inc 390 if yl <= -4364
g dec 324 if bl != -1671
pnt inc 657 if oa >= -6936
g inc -787 if ll == 2591
qnf dec -58 if rre != -3969
xek dec 550 if afx != 4129
pu inc -432 if xek < -4027
afx dec 873 if ll > 2594
rre inc 909 if j > -300
ar inc -19 if afx < 4134
pu inc -94 if vk > -245
gug inc 411 if zpw <= -2360
zpw dec -450 if gug <= 336
fs dec -224 if qnf == 4877
j inc 973 if ll == 2596
fs inc 242 if yl >= -4376
ll inc -142 if pu != -4358
kmh dec -428 if afx >= 4126
vk dec -981 if vk < -234
sg inc -472 if vk >= 734
afx dec -275 if oa >= -6941
wv dec -22 if rre != -3067
sg inc -213 if vk > 733
on inc 245 if fwk <= -8695
on dec -920 if vk >= 731
zpw dec 870 if g != 775
ll inc -401 if on == -3931
zpw dec 249 if on != -3941
g dec -541 if u == -2614")

(defun read-statement (fp)
  (list (intern (symbol-name (read fp)) :keyword)
	(read fp)
	(read fp)
	(read fp)
	(intern (symbol-name (read fp)) :keyword)
	(read fp)
	(read fp)))

(defvar *registers*)
(setf *registers* (make-hash-table))

(defun get-register (register)
  (gethash register *registers* 0))

(defun set-register (register value)
  (setf (gethash register *registers*) value))

(defun inc (&rest values)
  (apply '+ values))

(defun dec (&rest values)
  (apply '- values))

(defun == (&rest values)
  (apply '= values))

(defun != (&rest values)
  (apply '/= values))

(defun parse-statement (statement)
  (list 'when
	(list (sixth statement) (list 'get-register (fifth statement)) (seventh statement))
	(list 'set-register (first statement) (list (second statement) (list 'get-register (first statement)) (third statement)))))

(defun eval-line (line)
  (when line
    (let ((statement (with-input-from-string (fp line)
		       (read-statement fp))))
      (eval (parse-statement statement)))))

(with-input-from-string (fp *day-8-input*)
  (loop for line = (read-line fp nil)
     while line do (eval-line line))
  (close fp))

(apply 'max (alexandria:hash-table-values *registers*))

					; part 2

(setf *registers* (make-hash-table))
(defvar *max-value-seen* 0)
(setf (gethash :a *registers*) 0)

(with-input-from-string (fp *day-8-input*)
  (loop for line = (read-line fp nil)
     while line do
       (eval-line line)
       (setf *max-value-seen* (max *max-value-seen* (apply 'max (alexandria:hash-table-values *registers*)))))
  (close fp))

*max-value-seen*

;; day 9

(defvar *day-9-input*)
(setf *day-9-input*
      (alexandria:read-file-into-string "day9.txt"))

; part 1
(defun next-state (state c)
  (destructuring-bind (total-score group-depth in-garbagep escape-charp) state
    (cond
      ((not c) state)
      (escape-charp (list total-score group-depth in-garbagep NIL))
      ((and in-garbagep (char= c #\!)) (list total-score group-depth T T))
      ((char= c #\<) (list total-score group-depth T NIL))
      ((char= c #\>) (list total-score group-depth NIL NIL))
      ((and (not in-garbagep) (char= c #\})) (list (+ group-depth total-score) (1- group-depth) NIL NIL))
      ((and (not in-garbagep) (char= c #\{)) (list total-score (1+ group-depth) NIL NIL))
      (T state))))

(reduce #'next-state *day-9-input* :initial-value (list 0 0 NIL NIL))

; part 2
(defun next-state2 (state c)
  (destructuring-bind (total-score group-depth garbage-count in-garbagep escape-charp) state
    (cond
      ((not c) state)
      (escape-charp (list total-score group-depth garbage-count in-garbagep NIL))
      ((and in-garbagep (char= c #\!)) (list total-score group-depth garbage-count T T))
      ((and (not in-garbagep) (char= c #\<)) (list total-score group-depth garbage-count T NIL))
      ((char= c #\>) (list total-score group-depth garbage-count NIL NIL))
      ((and (not in-garbagep) (char= c #\})) (list (+ group-depth total-score) (1- group-depth) garbage-count NIL NIL))
      ((and (not in-garbagep) (char= c #\{)) (list total-score (1+ group-depth) garbage-count NIL NIL))
      (in-garbagep (list total-score group-depth (1+ garbage-count) T NIL))
      (T state))))

(reduce #'next-state2 *day-9-input* :initial-value (list 0 0 0 NIL NIL))


;; day 10

(defvar *day-10-input*)

					; part 1
(setf *day-10-input* '(183 0 31 146 254 240 223 150 2 206 161 1 255 232 199 88))

(defun next-knot-hash (list-state length)
  (destructuring-bind (current-list current-offset skip-distance) list-state
    (let* ((rotate-distance current-offset)
	   (left-rotated-list (alexandria:rotate (copy-seq current-list) (- rotate-distance)))
	   (segment (subseq left-rotated-list 0 length)))
      (setf (subseq left-rotated-list 0 length) (reverse segment))
      (list (alexandria:rotate left-rotated-list rotate-distance)
	    (+ current-offset length skip-distance)
	    (1+ skip-distance)))))

					; part 2

(defun knot-hash-round (list-state lengths)
  (reduce #'next-knot-hash lengths :initial-value list-state))

(defun knot-hash-multiple-rounds (lengths num-rounds)
  (do
   ((i 0 (1+ i))
    (current-state (list (alexandria:iota 256) 0 0) (knot-hash-round current-state lengths)))
   ((= i num-rounds) (first current-state))))

(defun knot-hash-string-to-lengths (input-string)
  (append
   (map 'list #'char-int input-string)
   '(17 31 73 47 23)))

(defun list-to-chunks-of-16 (l)
  (loop with list = l while list
     collect (loop repeat 16
		while list
		collect (pop list))))

(defun int-to-hex-string (i)
  (let ((s (write-to-string i :base 16)))
    (if (= (length s) 1)
	(concatenate 'string "0" s)
	s)))

(defun knot-hash (s)
  (string-downcase
   (apply #'concatenate 'string
	  (map 'list
	       #'int-to-hex-string
	       (map 'list
		    (lambda (chunk) (apply 'logxor chunk))
		    (list-to-chunks-of-16 (knot-hash-multiple-rounds (knot-hash-string-to-lengths s) 64)))))))


;; day 11

					;part 1

					; ne/sw - x,z
					; n/s   - y,z
					; se/nw - x,y

(defun move-hexagonally (position direction)
  (destructuring-bind (x y z) position
    (cond
      ((equal direction "ne") (list (1+ x) y (1- z)))
      ((equal direction "sw") (list (1- x) y (1+ z)))
      ((equal direction "nw") (list (1- x) (1+ y) z))
      ((equal direction "se") (list (1+ x) (1- y) z))
      ((equal direction "n")  (list x (1+ y) (1- z)))
      ((equal direction "s")  (list x (1- y) (1+ z))))))


(defun distance-from-origin (position)
  (/ (apply '+ (map 'list #'abs position)) 2))



(distance-from-origin (reduce #'move-hexagonally (cl-ppcre:split "," (alexandria:read-file-into-string "day11.txt")) :initial-value '(0 0 0)))

					; part 2

(destructuring-bind (_ positions-seen)
    (reduce
     (lambda (state direction)
       (destructuring-bind (current-position past-positions) state
	 (let ((new-position (move-hexagonally current-position direction)))
	   (list new-position
		 (cons new-position past-positions)))))
     (cl-ppcre:split "," (alexandria:read-file-into-string "day11.txt"))
     :initial-value '((0 0 0) ()))
  (apply 'max (map 'list #'distance-from-origin positions-seen)))


;; day 12

(defvar *day-12-input*)
(setf *day-12-input* "0 <-> 2
1 <-> 1
2 <-> 0, 3, 4
3 <-> 2, 4
4 <-> 2, 3, 6
5 <-> 6
6 <-> 4, 5")

(setf *day-12-input* "0 <-> 480, 1750
1 <-> 52, 393, 635, 800, 840
2 <-> 575, 1950
3 <-> 1188, 1527
4 <-> 177, 897, 898
5 <-> 1214
6 <-> 686
7 <-> 346, 1443
8 <-> 432
9 <-> 449
10 <-> 10, 678
11 <-> 1883
12 <-> 1760
13 <-> 80, 486, 533
14 <-> 483
15 <-> 1360
16 <-> 1812
17 <-> 150, 854, 1727
18 <-> 556, 1743, 1952
19 <-> 195
20 <-> 1146, 1249
21 <-> 31, 624, 1362, 1927
22 <-> 1355, 1895
23 <-> 471, 1624
24 <-> 493, 1424
25 <-> 309
26 <-> 1381
27 <-> 595
28 <-> 760, 1049, 1229
29 <-> 686, 1969
30 <-> 1498
31 <-> 21, 301, 1293
32 <-> 839, 1466
33 <-> 33, 130, 140
34 <-> 1169
35 <-> 579, 1785
36 <-> 1872
37 <-> 280
38 <-> 433, 1903
39 <-> 718
40 <-> 1541
41 <-> 599
42 <-> 1469
43 <-> 175, 205
44 <-> 148, 783
45 <-> 1752
46 <-> 1245, 1331
47 <-> 47, 58
48 <-> 242, 611
49 <-> 1418
50 <-> 129, 446
51 <-> 253
52 <-> 1, 104
53 <-> 830, 1178
54 <-> 673, 1376
55 <-> 202
56 <-> 56, 567, 1447
57 <-> 1486, 1579, 1610
58 <-> 47
59 <-> 1473
60 <-> 633
61 <-> 1780
62 <-> 62
63 <-> 155, 206, 405, 598, 639
64 <-> 1016, 1533
65 <-> 1781
66 <-> 78, 1082, 1446, 1948
67 <-> 1044, 1540
68 <-> 198, 927, 1288
69 <-> 69
70 <-> 1127
71 <-> 531, 1029
72 <-> 747
73 <-> 620, 1517, 1563
74 <-> 942, 1234
75 <-> 351, 506, 1449
76 <-> 1952, 1998
77 <-> 338
78 <-> 66
79 <-> 616, 1466
80 <-> 13, 652, 1955
81 <-> 1899
82 <-> 376, 909, 1112
83 <-> 220, 345, 838, 905, 1153
84 <-> 84
85 <-> 635, 694, 1653
86 <-> 208, 997
87 <-> 87
88 <-> 301
89 <-> 919, 1769, 1805
90 <-> 90, 992, 1720
91 <-> 786, 928
92 <-> 361
93 <-> 440, 939
94 <-> 131, 1703
95 <-> 1539
96 <-> 1752
97 <-> 687
98 <-> 822, 906
99 <-> 1564
100 <-> 597, 1817
101 <-> 162
102 <-> 138, 288, 542, 1816
103 <-> 512, 1946
104 <-> 52
105 <-> 802, 1511, 1527
106 <-> 766, 828
107 <-> 253
108 <-> 582, 1397, 1670, 1734
109 <-> 109
110 <-> 420, 1957
111 <-> 319
112 <-> 633
113 <-> 1056
114 <-> 1042, 1730
115 <-> 884
116 <-> 1428
117 <-> 1256
118 <-> 1177
119 <-> 1015
120 <-> 685, 769
121 <-> 254, 553, 1198
122 <-> 424, 1093
123 <-> 915
124 <-> 637, 690, 1117
125 <-> 1295, 1567
126 <-> 1168
127 <-> 1467, 1634
128 <-> 1858
129 <-> 50, 1950
130 <-> 33, 1521
131 <-> 94
132 <-> 483
133 <-> 1069, 1355
134 <-> 384, 610, 652
135 <-> 1633
136 <-> 1954
137 <-> 237, 377, 1720
138 <-> 102, 266, 406
139 <-> 411
140 <-> 33
141 <-> 523, 1279, 1732
142 <-> 1963
143 <-> 1117
144 <-> 520, 1051, 1142
145 <-> 1018
146 <-> 708, 1544
147 <-> 154, 442, 971, 1661, 1848
148 <-> 44, 1506, 1776
149 <-> 245, 1052, 1506
150 <-> 17, 150, 1565
151 <-> 151, 308
152 <-> 700, 1843
153 <-> 1996
154 <-> 147, 240
155 <-> 63, 990
156 <-> 1386, 1854
157 <-> 1117
158 <-> 687
159 <-> 1845
160 <-> 1477
161 <-> 161
162 <-> 101, 244, 464
163 <-> 1547
164 <-> 632, 1008, 1244, 1339
165 <-> 1332, 1373, 1533
166 <-> 697, 1002, 1871
167 <-> 547, 1004, 1498
168 <-> 559
169 <-> 408
170 <-> 170
171 <-> 398, 1878
172 <-> 1022, 1444
173 <-> 1437, 1515
174 <-> 383
175 <-> 43, 1374
176 <-> 634, 689, 1313, 1401
177 <-> 4
178 <-> 695, 870
179 <-> 179, 687
180 <-> 627, 720, 1071
181 <-> 1859
182 <-> 251, 387
183 <-> 241, 1441
184 <-> 1183, 1774, 1945
185 <-> 185, 436
186 <-> 186, 323, 1495
187 <-> 187
188 <-> 964
189 <-> 441, 808, 1673, 1803
190 <-> 190, 911, 1076
191 <-> 191, 1394
192 <-> 192
193 <-> 193
194 <-> 599, 1656, 1694
195 <-> 19, 295
196 <-> 1362, 1560
197 <-> 322
198 <-> 68
199 <-> 861
200 <-> 978, 1536, 1620
201 <-> 1190, 1315
202 <-> 55, 584, 1539
203 <-> 492, 820, 1444
204 <-> 1821
205 <-> 43, 1367
206 <-> 63, 352
207 <-> 681
208 <-> 86, 1089
209 <-> 1045
210 <-> 1672, 1783
211 <-> 682, 1136
212 <-> 354, 1003, 1403
213 <-> 543
214 <-> 994, 1922
215 <-> 564
216 <-> 541, 811, 1797
217 <-> 1398
218 <-> 542
219 <-> 278, 1527
220 <-> 83
221 <-> 917
222 <-> 515, 870, 1812
223 <-> 1130, 1203, 1891
224 <-> 224
225 <-> 300
226 <-> 424, 1223
227 <-> 793, 1431
228 <-> 1571
229 <-> 1946
230 <-> 256, 318, 1081
231 <-> 231
232 <-> 1294, 1303
233 <-> 671
234 <-> 234, 380, 1902
235 <-> 416, 492
236 <-> 545, 1113
237 <-> 137, 318, 581, 1126, 1561, 1612
238 <-> 906
239 <-> 272, 1135, 1782
240 <-> 154, 240
241 <-> 183
242 <-> 48, 679
243 <-> 396, 1841
244 <-> 162
245 <-> 149, 901, 1688
246 <-> 741
247 <-> 406, 1111, 1343
248 <-> 584
249 <-> 1551
250 <-> 889
251 <-> 182, 1230, 1980
252 <-> 781
253 <-> 51, 107, 325, 1156
254 <-> 121
255 <-> 989, 1200, 1228, 1832
256 <-> 230
257 <-> 758
258 <-> 665
259 <-> 470, 1090, 1645, 1874
260 <-> 964
261 <-> 1649, 1822, 1887, 1956
262 <-> 650
263 <-> 765
264 <-> 264, 391, 1216
265 <-> 329, 1347
266 <-> 138, 474, 1092
267 <-> 1462
268 <-> 268, 873, 1129
269 <-> 410, 866, 1863
270 <-> 1985
271 <-> 558, 1891
272 <-> 239
273 <-> 658, 1598
274 <-> 412, 1135
275 <-> 1478, 1941
276 <-> 276, 1418
277 <-> 347, 1714
278 <-> 219
279 <-> 677, 1198
280 <-> 37, 1399, 1555
281 <-> 281, 1194
282 <-> 637, 1158
283 <-> 815
284 <-> 555, 1554
285 <-> 1727
286 <-> 927
287 <-> 1352
288 <-> 102
289 <-> 1984
290 <-> 344, 1298
291 <-> 1187, 1442
292 <-> 305, 812
293 <-> 1673
294 <-> 331, 1243, 1423
295 <-> 195, 295, 1513, 1821
296 <-> 1078
297 <-> 1002, 1420
298 <-> 566, 1259
299 <-> 299, 1722
300 <-> 225, 1062
301 <-> 31, 88, 779, 1337
302 <-> 917
303 <-> 1367
304 <-> 1038, 1773
305 <-> 292, 1554, 1808
306 <-> 1065, 1471
307 <-> 1246, 1614, 1702
308 <-> 151, 443, 1205
309 <-> 25, 1522
310 <-> 729, 1977
311 <-> 1011
312 <-> 559, 718, 1471
313 <-> 1047, 1441
314 <-> 480, 1676
315 <-> 316, 1943
316 <-> 315, 555, 753
317 <-> 398, 428, 534, 1959
318 <-> 230, 237
319 <-> 111, 591
320 <-> 394
321 <-> 1832, 1866
322 <-> 197, 1768
323 <-> 186
324 <-> 1156, 1208, 1575
325 <-> 253
326 <-> 1584, 1845
327 <-> 1077
328 <-> 1009
329 <-> 265, 329, 1070, 1128
330 <-> 1892
331 <-> 294
332 <-> 1616, 1786, 1962
333 <-> 1667
334 <-> 1135
335 <-> 548
336 <-> 969, 1855, 1868
337 <-> 610
338 <-> 77, 1044
339 <-> 809, 1114
340 <-> 1841
341 <-> 578
342 <-> 1099
343 <-> 1410
344 <-> 290, 379, 991
345 <-> 83, 1317, 1588
346 <-> 7, 547
347 <-> 277, 1478
348 <-> 348, 802
349 <-> 1286, 1504
350 <-> 767, 1073
351 <-> 75, 731, 1163, 1526, 1558
352 <-> 206, 1733
353 <-> 731
354 <-> 212
355 <-> 404
356 <-> 1666
357 <-> 1116
358 <-> 1552, 1739
359 <-> 360
360 <-> 359, 1564, 1876
361 <-> 92, 949, 1741
362 <-> 441, 730, 1053
363 <-> 1469
364 <-> 1936
365 <-> 1250, 1529
366 <-> 366, 413, 1520
367 <-> 407, 1338, 1681, 1692
368 <-> 1312
369 <-> 1572, 1628, 1811
370 <-> 430
371 <-> 1886
372 <-> 1416
373 <-> 1943
374 <-> 569
375 <-> 556
376 <-> 82, 834
377 <-> 137, 1779
378 <-> 1177, 1478
379 <-> 344, 728, 801, 1141
380 <-> 234
381 <-> 940
382 <-> 611, 996
383 <-> 174, 515, 596, 1903
384 <-> 134
385 <-> 1408, 1471
386 <-> 1061, 1867
387 <-> 182
388 <-> 1123
389 <-> 397, 1068
390 <-> 1503
391 <-> 264, 1138, 1921
392 <-> 1996
393 <-> 1
394 <-> 320, 665, 1106
395 <-> 395
396 <-> 243, 831
397 <-> 389, 1193
398 <-> 171, 317
399 <-> 399
400 <-> 740, 1448
401 <-> 659, 963, 1929
402 <-> 1396
403 <-> 1082, 1793
404 <-> 355, 404, 613, 1901
405 <-> 63, 716, 1602
406 <-> 138, 247
407 <-> 367, 1186, 1996
408 <-> 169, 472, 1551
409 <-> 1078, 1216
410 <-> 269, 1328
411 <-> 139, 1758
412 <-> 274
413 <-> 366, 986, 1171, 1836
414 <-> 783
415 <-> 947
416 <-> 235, 1976
417 <-> 473, 774
418 <-> 1010, 1082
419 <-> 612
420 <-> 110
421 <-> 1747, 1804
422 <-> 422, 1987
423 <-> 423
424 <-> 122, 226, 1119
425 <-> 707, 1378
426 <-> 1199, 1325
427 <-> 1415
428 <-> 317, 702, 930
429 <-> 485, 700, 1601
430 <-> 370, 627
431 <-> 1297, 1622
432 <-> 8, 705, 736, 1649
433 <-> 38
434 <-> 607, 1787
435 <-> 1803
436 <-> 185
437 <-> 437, 1634
438 <-> 716, 1152
439 <-> 1098, 1798, 1991
440 <-> 93, 440
441 <-> 189, 362, 449
442 <-> 147
443 <-> 308, 1505
444 <-> 444
445 <-> 445, 1001, 1165, 1591
446 <-> 50, 457, 712, 1683
447 <-> 1822
448 <-> 1525
449 <-> 9, 441, 668
450 <-> 782
451 <-> 745, 868, 1296, 1453
452 <-> 1295
453 <-> 465, 997
454 <-> 663
455 <-> 839, 1197
456 <-> 1802, 1949
457 <-> 446, 577, 605, 1101
458 <-> 691, 1466, 1635, 1643
459 <-> 1484
460 <-> 1672, 1953
461 <-> 577, 989, 1393, 1799
462 <-> 462
463 <-> 552, 1474
464 <-> 162, 669
465 <-> 453
466 <-> 1213, 1345
467 <-> 706, 941
468 <-> 1254, 1478
469 <-> 1658
470 <-> 259, 1464, 1624, 1797
471 <-> 23
472 <-> 408, 1549
473 <-> 417, 676
474 <-> 266, 1340, 1543
475 <-> 1699, 1717
476 <-> 1048, 1741
477 <-> 1741
478 <-> 1627
479 <-> 1494
480 <-> 0, 314
481 <-> 1206
482 <-> 987
483 <-> 14, 132, 1844, 1881, 1961
484 <-> 1320
485 <-> 429, 1544
486 <-> 13, 1097
487 <-> 1130
488 <-> 1606
489 <-> 1544
490 <-> 1752
491 <-> 545, 641
492 <-> 203, 235, 1518, 1538, 1633
493 <-> 24, 1029
494 <-> 834, 962
495 <-> 495
496 <-> 1069
497 <-> 571, 638, 993, 1067
498 <-> 1220
499 <-> 521, 1919
500 <-> 1650, 1768
501 <-> 843, 1932
502 <-> 1318, 1751
503 <-> 976
504 <-> 1151
505 <-> 594, 1282, 1433
506 <-> 75
507 <-> 1932
508 <-> 932, 934, 1439, 1938
509 <-> 509
510 <-> 1284
511 <-> 1324
512 <-> 103, 813, 1011
513 <-> 1120
514 <-> 742
515 <-> 222, 383
516 <-> 1976
517 <-> 776, 790, 821, 1242
518 <-> 647, 1549
519 <-> 671, 1457
520 <-> 144
521 <-> 499, 1207
522 <-> 1634
523 <-> 141, 1310
524 <-> 1604
525 <-> 1933
526 <-> 592, 1671, 1712
527 <-> 527
528 <-> 1167, 1171, 1309
529 <-> 529, 1137
530 <-> 980
531 <-> 71
532 <-> 750
533 <-> 13
534 <-> 317, 1440
535 <-> 685, 922, 1034, 1427
536 <-> 1784
537 <-> 1677
538 <-> 1635
539 <-> 1575
540 <-> 1243
541 <-> 216, 617, 1095, 1834
542 <-> 102, 218
543 <-> 213, 1346, 1501
544 <-> 1648
545 <-> 236, 491
546 <-> 994, 1800
547 <-> 167, 346, 1724
548 <-> 335, 693
549 <-> 1707
550 <-> 1526
551 <-> 1159
552 <-> 463, 1658
553 <-> 121, 1103
554 <-> 1641, 1891
555 <-> 284, 316
556 <-> 18, 375
557 <-> 1081
558 <-> 271, 1033
559 <-> 168, 312, 1014, 1660
560 <-> 1536
561 <-> 1681
562 <-> 973, 1136
563 <-> 1114
564 <-> 215, 1429
565 <-> 565, 1960
566 <-> 298, 636, 959, 1135, 1136
567 <-> 56, 709
568 <-> 1772
569 <-> 374, 569, 763
570 <-> 604, 1862
571 <-> 497
572 <-> 1896
573 <-> 573, 997, 1640, 1711
574 <-> 1806
575 <-> 2
576 <-> 1953
577 <-> 457, 461, 1499, 1771
578 <-> 341, 981, 1344, 1701
579 <-> 35, 735, 765
580 <-> 580, 1116
581 <-> 237
582 <-> 108
583 <-> 1227, 1762
584 <-> 202, 248, 795, 1523, 1537, 1845
585 <-> 1389, 1943
586 <-> 586, 867
587 <-> 587, 1145, 1574, 1651
588 <-> 744
589 <-> 593, 799
590 <-> 1160
591 <-> 319, 863, 1260
592 <-> 526, 1622
593 <-> 589, 1267
594 <-> 505
595 <-> 27, 595
596 <-> 383, 1951
597 <-> 100
598 <-> 63
599 <-> 41, 194
600 <-> 1397
601 <-> 722, 1115
602 <-> 974
603 <-> 1348
604 <-> 570, 604, 1342
605 <-> 457, 630, 1479, 1767
606 <-> 606
607 <-> 434
608 <-> 863, 1421
609 <-> 1505, 1524
610 <-> 134, 337, 1380
611 <-> 48, 382
612 <-> 419, 763
613 <-> 404, 729
614 <-> 1211, 1496, 1654
615 <-> 1152
616 <-> 79
617 <-> 541, 960
618 <-> 1224, 1889
619 <-> 837, 1094
620 <-> 73
621 <-> 717
622 <-> 716, 1570
623 <-> 770
624 <-> 21, 1564
625 <-> 750, 807, 1042, 1715
626 <-> 1164, 1307, 1700, 1933
627 <-> 180, 430, 666
628 <-> 1194, 1463
629 <-> 875
630 <-> 605
631 <-> 846, 1731
632 <-> 164, 1454, 1579
633 <-> 60, 112, 1940
634 <-> 176
635 <-> 1, 85, 1948
636 <-> 566, 1057
637 <-> 124, 282
638 <-> 497, 727
639 <-> 63
640 <-> 843, 884, 1603
641 <-> 491, 641
642 <-> 875
643 <-> 1719
644 <-> 1214
645 <-> 1608
646 <-> 1043, 1477
647 <-> 518
648 <-> 998
649 <-> 649
650 <-> 262, 650
651 <-> 1340
652 <-> 80, 134
653 <-> 1020, 1600
654 <-> 1080, 1182
655 <-> 1499
656 <-> 656, 968
657 <-> 1608
658 <-> 273, 1245, 1698
659 <-> 401, 1049, 1774
660 <-> 660, 1877
661 <-> 1374
662 <-> 1808, 1910, 1949
663 <-> 454, 663, 721, 1823
664 <-> 664
665 <-> 258, 394, 1820
666 <-> 627, 1334, 1765
667 <-> 1158
668 <-> 449, 1073, 1336
669 <-> 464, 1362, 1716
670 <-> 670
671 <-> 233, 519, 1806
672 <-> 928, 1397, 1923
673 <-> 54, 673, 1239, 1626
674 <-> 996, 1846
675 <-> 1452, 1531
676 <-> 473, 1080, 1195
677 <-> 279
678 <-> 10
679 <-> 242
680 <-> 770, 1082
681 <-> 207, 1768
682 <-> 211, 1780
683 <-> 1366, 1969
684 <-> 1500
685 <-> 120, 535
686 <-> 6, 29, 1480
687 <-> 97, 158, 179
688 <-> 797
689 <-> 176, 902, 1656, 1735
690 <-> 124
691 <-> 458, 1685
692 <-> 977
693 <-> 548, 693, 999
694 <-> 85
695 <-> 178, 1161, 1173
696 <-> 1691
697 <-> 166
698 <-> 698
699 <-> 1632
700 <-> 152, 429, 1380, 1583
701 <-> 1393
702 <-> 428, 1072, 1123
703 <-> 703
704 <-> 1504
705 <-> 432, 1553
706 <-> 467
707 <-> 425, 950
708 <-> 146
709 <-> 567
710 <-> 710
711 <-> 1536
712 <-> 446
713 <-> 1118
714 <-> 1031, 1087
715 <-> 843
716 <-> 405, 438, 622, 809, 879
717 <-> 621, 1564, 1753
718 <-> 39, 312, 1628
719 <-> 852, 1592
720 <-> 180
721 <-> 663
722 <-> 601, 824
723 <-> 723
724 <-> 1053
725 <-> 1752, 1898
726 <-> 998, 1841
727 <-> 638
728 <-> 379
729 <-> 310, 613, 1127
730 <-> 362
731 <-> 351, 353, 1614, 1619
732 <-> 1258, 1911
733 <-> 1883
734 <-> 987
735 <-> 579, 1758
736 <-> 432
737 <-> 760
738 <-> 1125
739 <-> 1492
740 <-> 400, 1794
741 <-> 246, 1202
742 <-> 514, 1607
743 <-> 743, 948
744 <-> 588, 753
745 <-> 451
746 <-> 963, 1370, 1611, 1815, 1941
747 <-> 72, 1453
748 <-> 748, 1017
749 <-> 1160, 1661
750 <-> 532, 625
751 <-> 751, 1395
752 <-> 1788
753 <-> 316, 744
754 <-> 1329
755 <-> 1005, 1468, 1943
756 <-> 1227, 1247
757 <-> 1687
758 <-> 257, 1292, 1893
759 <-> 759, 1629
760 <-> 28, 737, 949
761 <-> 1543
762 <-> 840, 1216
763 <-> 569, 612, 1302, 1490
764 <-> 817, 1606
765 <-> 263, 579, 1731
766 <-> 106
767 <-> 350
768 <-> 1261, 1569
769 <-> 120, 1119, 1663, 1812
770 <-> 623, 680
771 <-> 1752
772 <-> 1242
773 <-> 1421, 1548
774 <-> 417, 1841
775 <-> 1801, 1880
776 <-> 517, 1221
777 <-> 1596, 1923
778 <-> 778
779 <-> 301
780 <-> 993
781 <-> 252, 781
782 <-> 450, 782, 1745
783 <-> 44, 414, 833, 1088
784 <-> 1914
785 <-> 799
786 <-> 91
787 <-> 972, 1744, 1830
788 <-> 788
789 <-> 1246
790 <-> 517, 790, 1256
791 <-> 838
792 <-> 1360, 1393
793 <-> 227, 793
794 <-> 1601
795 <-> 584, 1337
796 <-> 1537, 1549
797 <-> 688, 1221
798 <-> 1412
799 <-> 589, 785, 799, 1763
800 <-> 1
801 <-> 379
802 <-> 105, 348
803 <-> 1638, 1901
804 <-> 1356
805 <-> 1424
806 <-> 906, 983
807 <-> 625
808 <-> 189
809 <-> 339, 716
810 <-> 1775, 1938
811 <-> 216
812 <-> 292
813 <-> 512
814 <-> 1194
815 <-> 283, 1439
816 <-> 982, 1049
817 <-> 764, 860, 913
818 <-> 1027, 1394, 1396
819 <-> 1045, 1992
820 <-> 203
821 <-> 517, 865
822 <-> 98
823 <-> 920
824 <-> 722, 1409, 1530
825 <-> 1036
826 <-> 1794
827 <-> 842
828 <-> 106, 881, 1327
829 <-> 829
830 <-> 53
831 <-> 396, 885
832 <-> 1130, 1582
833 <-> 783
834 <-> 376, 494, 1236
835 <-> 1241
836 <-> 911, 1596
837 <-> 619, 1099
838 <-> 83, 791, 1334
839 <-> 32, 455, 1963
840 <-> 1, 762, 918
841 <-> 841
842 <-> 827, 1000
843 <-> 501, 640, 715, 1174
844 <-> 1448
845 <-> 1376, 1635, 1769
846 <-> 631, 1893, 1981
847 <-> 1508
848 <-> 1007
849 <-> 849, 886
850 <-> 1430
851 <-> 860
852 <-> 719, 897, 1272
853 <-> 1374
854 <-> 17, 956, 1536
855 <-> 1774
856 <-> 1378
857 <-> 1171
858 <-> 958, 1180, 1718
859 <-> 1585
860 <-> 817, 851
861 <-> 199, 1184, 1509
862 <-> 965, 1095, 1786
863 <-> 591, 608, 961, 1108
864 <-> 864
865 <-> 821
866 <-> 269, 1973
867 <-> 586
868 <-> 451, 905, 1710
869 <-> 997, 1508
870 <-> 178, 222, 1358
871 <-> 973
872 <-> 1724
873 <-> 268, 1169, 1613
874 <-> 1360
875 <-> 629, 642, 1077
876 <-> 915, 1149, 1512
877 <-> 949
878 <-> 1070
879 <-> 716, 1573
880 <-> 1371, 1552
881 <-> 828, 1483, 1883
882 <-> 1121, 1257
883 <-> 976, 1500
884 <-> 115, 640
885 <-> 831, 1012
886 <-> 849
887 <-> 1292
888 <-> 1728, 1947
889 <-> 250, 984, 1840
890 <-> 1864
891 <-> 1612
892 <-> 1492
893 <-> 928
894 <-> 1534, 1920
895 <-> 1751, 1928, 1989
896 <-> 1363, 1914
897 <-> 4, 852, 897
898 <-> 4, 1926
899 <-> 1668, 1840
900 <-> 1239
901 <-> 245
902 <-> 689, 1426, 1971
903 <-> 903, 1817
904 <-> 1721, 1755
905 <-> 83, 868
906 <-> 98, 238, 806, 1528, 1931
907 <-> 1671, 1833
908 <-> 1736
909 <-> 82, 1689
910 <-> 988, 1023
911 <-> 190, 836
912 <-> 1694
913 <-> 817
914 <-> 915
915 <-> 123, 876, 914, 1562
916 <-> 920, 997, 1278
917 <-> 221, 302, 1739
918 <-> 840
919 <-> 89
920 <-> 823, 916
921 <-> 1198, 1266, 1282, 1678
922 <-> 535
923 <-> 923
924 <-> 1464, 1636
925 <-> 1002, 1419
926 <-> 1697
927 <-> 68, 286, 1010
928 <-> 91, 672, 893, 1997
929 <-> 1311, 1475, 1954
930 <-> 428
931 <-> 1271
932 <-> 508
933 <-> 1330, 1681
934 <-> 508
935 <-> 1103, 1982
936 <-> 1088, 1773
937 <-> 1130, 1324
938 <-> 1104, 1118, 1590
939 <-> 93, 1726
940 <-> 381, 1621, 1865
941 <-> 467, 941
942 <-> 74, 1519, 1764, 1930
943 <-> 1465, 1545
944 <-> 1353
945 <-> 1665
946 <-> 1727
947 <-> 415, 1118
948 <-> 743
949 <-> 361, 760, 877, 1122
950 <-> 707, 1782
951 <-> 1438
952 <-> 952, 1180
953 <-> 1685
954 <-> 954
955 <-> 955, 1287, 1541
956 <-> 854
957 <-> 957
958 <-> 858
959 <-> 566, 1429
960 <-> 617
961 <-> 863
962 <-> 494
963 <-> 401, 746, 1132
964 <-> 188, 260, 1064, 1424
965 <-> 862
966 <-> 1742
967 <-> 1692, 1713
968 <-> 656
969 <-> 336, 1770
970 <-> 1523
971 <-> 147
972 <-> 787
973 <-> 562, 871
974 <-> 602, 1026, 1754
975 <-> 985
976 <-> 503, 883, 1636
977 <-> 692, 1298
978 <-> 200
979 <-> 1068, 1306
980 <-> 530, 1894
981 <-> 578, 1043, 1385
982 <-> 816, 1057
983 <-> 806, 1085, 1375, 1430
984 <-> 889, 1517
985 <-> 975, 1762
986 <-> 413
987 <-> 482, 734, 987
988 <-> 910, 988
989 <-> 255, 461, 1422, 1768
990 <-> 155, 1589
991 <-> 344, 1365
992 <-> 90
993 <-> 497, 780, 1688, 1888
994 <-> 214, 546
995 <-> 1246, 1268
996 <-> 382, 674
997 <-> 86, 453, 573, 869, 916
998 <-> 648, 726
999 <-> 693, 1172
1000 <-> 842, 1086, 1828
1001 <-> 445
1002 <-> 166, 297, 925, 1002
1003 <-> 212, 1458
1004 <-> 167, 1610
1005 <-> 755
1006 <-> 1613
1007 <-> 848, 1007
1008 <-> 164, 1225, 1316
1009 <-> 328, 1009
1010 <-> 418, 927, 1657
1011 <-> 311, 512, 1287, 1476
1012 <-> 885
1013 <-> 1264, 1473, 1678
1014 <-> 559
1015 <-> 119, 1502
1016 <-> 64, 1349
1017 <-> 748
1018 <-> 145, 1018
1019 <-> 1791
1020 <-> 653, 1663
1021 <-> 1854
1022 <-> 172
1023 <-> 910
1024 <-> 1151, 1913
1025 <-> 1359
1026 <-> 974, 1857
1027 <-> 818, 1351, 1760
1028 <-> 1677
1029 <-> 71, 493
1030 <-> 1030, 1175
1031 <-> 714
1032 <-> 1318
1033 <-> 558, 1861
1034 <-> 535
1035 <-> 1035, 1958
1036 <-> 825, 1310, 1742
1037 <-> 1862
1038 <-> 304, 1199
1039 <-> 1168, 1480
1040 <-> 1639, 1861, 1886
1041 <-> 1041, 1050
1042 <-> 114, 625
1043 <-> 646, 981, 1219
1044 <-> 67, 338, 1634
1045 <-> 209, 819
1046 <-> 1148, 1154, 1459
1047 <-> 313, 1921
1048 <-> 476
1049 <-> 28, 659, 816, 1356, 1427, 1795, 1934
1050 <-> 1041
1051 <-> 144
1052 <-> 149, 1052, 1383, 1423, 1621
1053 <-> 362, 724
1054 <-> 1054
1055 <-> 1491, 1784
1056 <-> 113, 1567
1057 <-> 636, 982
1058 <-> 1096, 1257, 1617, 1729
1059 <-> 1059, 1130
1060 <-> 1343, 1647, 1892
1061 <-> 386
1062 <-> 300, 1062, 1829
1063 <-> 1063, 1571
1064 <-> 964, 1846
1065 <-> 306
1066 <-> 1323, 1785
1067 <-> 497
1068 <-> 389, 979, 1330
1069 <-> 133, 496
1070 <-> 329, 878
1071 <-> 180
1072 <-> 702
1073 <-> 350, 668
1074 <-> 1188
1075 <-> 1534
1076 <-> 190, 1158
1077 <-> 327, 875, 1444
1078 <-> 296, 409
1079 <-> 1450
1080 <-> 654, 676
1081 <-> 230, 557, 1896
1082 <-> 66, 403, 418, 680
1083 <-> 1134
1084 <-> 1807, 1860
1085 <-> 983, 1555
1086 <-> 1000, 1992
1087 <-> 714, 1216, 1728, 1895
1088 <-> 783, 936
1089 <-> 208
1090 <-> 259
1091 <-> 1213
1092 <-> 266
1093 <-> 122
1094 <-> 619, 1814
1095 <-> 541, 862, 1830
1096 <-> 1058, 1145
1097 <-> 486, 1894
1098 <-> 439, 1133
1099 <-> 342, 837
1100 <-> 1559, 1931
1101 <-> 457
1102 <-> 1570
1103 <-> 553, 935
1104 <-> 938, 1897
1105 <-> 1425
1106 <-> 394
1107 <-> 1304, 1566
1108 <-> 863
1109 <-> 1423
1110 <-> 1149, 1217
1111 <-> 247, 1111
1112 <-> 82
1113 <-> 236
1114 <-> 339, 563
1115 <-> 601
1116 <-> 357, 580
1117 <-> 124, 143, 157
1118 <-> 713, 938, 947
1119 <-> 424, 769
1120 <-> 513, 1606
1121 <-> 882
1122 <-> 949
1123 <-> 388, 702
1124 <-> 1255, 1552
1125 <-> 738, 1923
1126 <-> 237
1127 <-> 70, 729
1128 <-> 329
1129 <-> 268
1130 <-> 223, 487, 832, 937, 1059
1131 <-> 1661
1132 <-> 963, 1253, 1810
1133 <-> 1098, 1354, 1852
1134 <-> 1083, 1134
1135 <-> 239, 274, 334, 566
1136 <-> 211, 562, 566, 1504
1137 <-> 529
1138 <-> 391, 1869
1139 <-> 1499
1140 <-> 1140
1141 <-> 379, 1403
1142 <-> 144, 1957
1143 <-> 1507, 1688
1144 <-> 1189
1145 <-> 587, 1096, 1372, 1942
1146 <-> 20, 1668
1147 <-> 1168
1148 <-> 1046, 1148
1149 <-> 876, 1110, 1758
1150 <-> 1491
1151 <-> 504, 1024, 1164
1152 <-> 438, 615, 1152, 1837
1153 <-> 83
1154 <-> 1046
1155 <-> 1412, 1725
1156 <-> 253, 324
1157 <-> 1381, 1631
1158 <-> 282, 667, 1076
1159 <-> 551, 1159
1160 <-> 590, 749
1161 <-> 695, 1841
1162 <-> 1535
1163 <-> 351
1164 <-> 626, 1151
1165 <-> 445, 1680
1166 <-> 1752, 1895
1167 <-> 528, 1501
1168 <-> 126, 1039, 1147
1169 <-> 34, 873
1170 <-> 1556
1171 <-> 413, 528, 857, 1365
1172 <-> 999
1173 <-> 695, 1988
1174 <-> 843, 1174
1175 <-> 1030
1176 <-> 1609
1177 <-> 118, 378, 1213
1178 <-> 53, 1839
1179 <-> 1281, 1674
1180 <-> 858, 952
1181 <-> 1542
1182 <-> 654
1183 <-> 184
1184 <-> 861, 1489, 1803
1185 <-> 1251
1186 <-> 407
1187 <-> 291
1188 <-> 3, 1074, 1627
1189 <-> 1144, 1189
1190 <-> 201, 1769
1191 <-> 1735
1192 <-> 1542, 1860
1193 <-> 397
1194 <-> 281, 628, 814
1195 <-> 676
1196 <-> 1196
1197 <-> 455
1198 <-> 121, 279, 921
1199 <-> 426, 1038
1200 <-> 255, 1381, 1882
1201 <-> 1536, 1691
1202 <-> 741, 1216
1203 <-> 223, 1922
1204 <-> 1243
1205 <-> 308
1206 <-> 481, 1206, 1434, 1605
1207 <-> 521
1208 <-> 324
1209 <-> 1209
1210 <-> 1298
1211 <-> 614, 1402
1212 <-> 1778
1213 <-> 466, 1091, 1177
1214 <-> 5, 644, 1862
1215 <-> 1215
1216 <-> 264, 409, 762, 1087, 1202
1217 <-> 1110
1218 <-> 1503
1219 <-> 1043
1220 <-> 498, 1475
1221 <-> 776, 797, 1359
1222 <-> 1316
1223 <-> 226
1224 <-> 618
1225 <-> 1008
1226 <-> 1387, 1517
1227 <-> 583, 756
1228 <-> 255
1229 <-> 28, 1659
1230 <-> 251
1231 <-> 1231
1232 <-> 1232, 1578, 1857
1233 <-> 1233
1234 <-> 74, 1326, 1576
1235 <-> 1772, 1783
1236 <-> 834, 1734
1237 <-> 1868, 1998
1238 <-> 1305, 1538
1239 <-> 673, 900, 1818
1240 <-> 1792, 1800
1241 <-> 835, 1751, 1925
1242 <-> 517, 772
1243 <-> 294, 540, 1204
1244 <-> 164
1245 <-> 46, 658
1246 <-> 307, 789, 995
1247 <-> 756, 1284
1248 <-> 1283
1249 <-> 20
1250 <-> 365
1251 <-> 1185, 1456, 1981
1252 <-> 1998
1253 <-> 1132
1254 <-> 468
1255 <-> 1124, 1255
1256 <-> 117, 790, 1970
1257 <-> 882, 1058, 1503
1258 <-> 732
1259 <-> 298
1260 <-> 591, 1497
1261 <-> 768, 1768
1262 <-> 1262
1263 <-> 1783
1264 <-> 1013, 1719
1265 <-> 1842
1266 <-> 921, 1432
1267 <-> 593
1268 <-> 995, 1390
1269 <-> 1364
1270 <-> 1667, 1781
1271 <-> 931, 1982
1272 <-> 852, 1472
1273 <-> 1961
1274 <-> 1749
1275 <-> 1364, 1445
1276 <-> 1299, 1480
1277 <-> 1277
1278 <-> 916, 1326
1279 <-> 141
1280 <-> 1280, 1775
1281 <-> 1179
1282 <-> 505, 921, 1388, 1455
1283 <-> 1248, 1363
1284 <-> 510, 1247, 1979
1285 <-> 1943
1286 <-> 349
1287 <-> 955, 1011
1288 <-> 68, 1813
1289 <-> 1883
1290 <-> 1977
1291 <-> 1751
1292 <-> 758, 887, 1529
1293 <-> 31
1294 <-> 232, 1517
1295 <-> 125, 452, 1397
1296 <-> 451
1297 <-> 431, 1775
1298 <-> 290, 977, 1210
1299 <-> 1276
1300 <-> 1300
1301 <-> 1480, 1755
1302 <-> 763, 1806
1303 <-> 232
1304 <-> 1107, 1510, 1841
1305 <-> 1238, 1680
1306 <-> 979
1307 <-> 626
1308 <-> 1854
1309 <-> 528, 1993
1310 <-> 523, 1036, 1310
1311 <-> 929, 1311
1312 <-> 368, 1957
1313 <-> 176, 1756
1314 <-> 1314
1315 <-> 201
1316 <-> 1008, 1222, 1736
1317 <-> 345
1318 <-> 502, 1032, 1914
1319 <-> 1503
1320 <-> 484, 1961
1321 <-> 1635
1322 <-> 1322, 1777
1323 <-> 1066
1324 <-> 511, 937
1325 <-> 426
1326 <-> 1234, 1278, 1912
1327 <-> 828, 1931
1328 <-> 410, 1866
1329 <-> 754, 1664, 1698
1330 <-> 933, 1068
1331 <-> 46
1332 <-> 165
1333 <-> 1884, 1886
1334 <-> 666, 838, 1983
1335 <-> 1516, 1849
1336 <-> 668, 1384
1337 <-> 301, 795
1338 <-> 367, 1535
1339 <-> 164
1340 <-> 474, 651
1341 <-> 1341
1342 <-> 604
1343 <-> 247, 1060
1344 <-> 578, 1637, 1665, 1917, 1980
1345 <-> 466
1346 <-> 543
1347 <-> 265
1348 <-> 603, 1348
1349 <-> 1016
1350 <-> 1677
1351 <-> 1027
1352 <-> 287, 1527, 1878
1353 <-> 944, 1353
1354 <-> 1133, 1485
1355 <-> 22, 133
1356 <-> 804, 1049, 1813
1357 <-> 1507
1358 <-> 870, 1889
1359 <-> 1025, 1221
1360 <-> 15, 792, 874
1361 <-> 1361
1362 <-> 21, 196, 669, 1544
1363 <-> 896, 1283
1364 <-> 1269, 1275
1365 <-> 991, 1171
1366 <-> 683
1367 <-> 205, 303
1368 <-> 1400, 1473
1369 <-> 1537
1370 <-> 746, 1451
1371 <-> 880
1372 <-> 1145
1373 <-> 165, 1482
1374 <-> 175, 661, 853, 1374
1375 <-> 983
1376 <-> 54, 845, 1433
1377 <-> 1653
1378 <-> 425, 856
1379 <-> 1604
1380 <-> 610, 700, 1967
1381 <-> 26, 1157, 1200
1382 <-> 1382, 1581, 1969
1383 <-> 1052
1384 <-> 1336
1385 <-> 981
1386 <-> 156, 1632
1387 <-> 1226
1388 <-> 1282
1389 <-> 585
1390 <-> 1268
1391 <-> 1951
1392 <-> 1392
1393 <-> 461, 701, 792
1394 <-> 191, 818
1395 <-> 751
1396 <-> 402, 818
1397 <-> 108, 600, 672, 1295, 1937
1398 <-> 217, 1675, 1943
1399 <-> 280
1400 <-> 1368
1401 <-> 176
1402 <-> 1211, 1699
1403 <-> 212, 1141
1404 <-> 1489
1405 <-> 1479
1406 <-> 1406, 1900
1407 <-> 1407
1408 <-> 385
1409 <-> 824, 1778, 1964
1410 <-> 343, 1568
1411 <-> 1796
1412 <-> 798, 1155
1413 <-> 1551
1414 <-> 1460, 1686
1415 <-> 427, 1630, 1937
1416 <-> 372, 1821
1417 <-> 1554, 1690
1418 <-> 49, 276
1419 <-> 925
1420 <-> 297
1421 <-> 608, 773, 1543
1422 <-> 989, 1520
1423 <-> 294, 1052, 1109, 1618
1424 <-> 24, 805, 964
1425 <-> 1105, 1822
1426 <-> 902
1427 <-> 535, 1049, 1986
1428 <-> 116, 1805
1429 <-> 564, 959, 1978
1430 <-> 850, 983
1431 <-> 227
1432 <-> 1266
1433 <-> 505, 1376
1434 <-> 1206
1435 <-> 1435, 1940
1436 <-> 1980
1437 <-> 173, 1853
1438 <-> 951, 1517
1439 <-> 508, 815, 1606
1440 <-> 534
1441 <-> 183, 313, 1788
1442 <-> 291, 1532, 1657
1443 <-> 7
1444 <-> 172, 203, 1077
1445 <-> 1275, 1445
1446 <-> 66
1447 <-> 56
1448 <-> 400, 844, 1568
1449 <-> 75, 1449, 1870
1450 <-> 1079, 1859
1451 <-> 1370
1452 <-> 675
1453 <-> 451, 747, 1462, 1944
1454 <-> 632, 1563
1455 <-> 1282
1456 <-> 1251, 1654
1457 <-> 519
1458 <-> 1003, 1575
1459 <-> 1046
1460 <-> 1414
1461 <-> 1830, 1986
1462 <-> 267, 1453
1463 <-> 628, 1911
1464 <-> 470, 924
1465 <-> 943, 1738
1466 <-> 32, 79, 458
1467 <-> 127
1468 <-> 755
1469 <-> 42, 363, 1469
1470 <-> 1600
1471 <-> 306, 312, 385
1472 <-> 1272, 1607
1473 <-> 59, 1013, 1368, 1695, 1992
1474 <-> 463, 1480
1475 <-> 929, 1220, 1724
1476 <-> 1011
1477 <-> 160, 646
1478 <-> 275, 347, 378, 468
1479 <-> 605, 1405, 1867
1480 <-> 686, 1039, 1276, 1301, 1474
1481 <-> 1926
1482 <-> 1373, 1529
1483 <-> 881
1484 <-> 459, 1540
1485 <-> 1354
1486 <-> 57
1487 <-> 1715, 1915
1488 <-> 1488
1489 <-> 1184, 1404
1490 <-> 763
1491 <-> 1055, 1150, 1491
1492 <-> 739, 892, 1776
1493 <-> 1493
1494 <-> 479, 1907
1495 <-> 186
1496 <-> 614
1497 <-> 1260
1498 <-> 30, 167
1499 <-> 577, 655, 1139
1500 <-> 684, 883
1501 <-> 543, 1167, 1965
1502 <-> 1015, 1502
1503 <-> 390, 1218, 1257, 1319, 1906
1504 <-> 349, 704, 1136, 1750
1505 <-> 443, 609
1506 <-> 148, 149
1507 <-> 1143, 1357
1508 <-> 847, 869
1509 <-> 861, 1837
1510 <-> 1304
1511 <-> 105
1512 <-> 876
1513 <-> 295
1514 <-> 1514
1515 <-> 173, 1593, 1809
1516 <-> 1335, 1599
1517 <-> 73, 984, 1226, 1294, 1438
1518 <-> 492
1519 <-> 942
1520 <-> 366, 1422
1521 <-> 130
1522 <-> 309, 1522
1523 <-> 584, 970, 1826
1524 <-> 609
1525 <-> 448, 1525
1526 <-> 351, 550
1527 <-> 3, 105, 219, 1352
1528 <-> 906, 1718
1529 <-> 365, 1292, 1482
1530 <-> 824
1531 <-> 675, 1531
1532 <-> 1442
1533 <-> 64, 165, 1804
1534 <-> 894, 1075, 1959
1535 <-> 1162, 1338, 1854
1536 <-> 200, 560, 711, 854, 1201, 1873
1537 <-> 584, 796, 1369
1538 <-> 492, 1238
1539 <-> 95, 202
1540 <-> 67, 1484
1541 <-> 40, 955
1542 <-> 1181, 1192
1543 <-> 474, 761, 1421
1544 <-> 146, 485, 489, 1362, 1753
1545 <-> 943
1546 <-> 1546
1547 <-> 163, 1993
1548 <-> 773
1549 <-> 472, 518, 796
1550 <-> 1550
1551 <-> 249, 408, 1413
1552 <-> 358, 880, 1124
1553 <-> 705
1554 <-> 284, 305, 1417, 1554, 1904
1555 <-> 280, 1085
1556 <-> 1170, 1989
1557 <-> 1847
1558 <-> 351, 1864
1559 <-> 1100
1560 <-> 196
1561 <-> 237, 1738
1562 <-> 915
1563 <-> 73, 1454
1564 <-> 99, 360, 624, 717
1565 <-> 150
1566 <-> 1107
1567 <-> 125, 1056
1568 <-> 1410, 1448
1569 <-> 768
1570 <-> 622, 1102
1571 <-> 228, 1063
1572 <-> 369, 1770
1573 <-> 879
1574 <-> 587, 1761
1575 <-> 324, 539, 1458
1576 <-> 1234
1577 <-> 1577
1578 <-> 1232
1579 <-> 57, 632, 1766
1580 <-> 1608
1581 <-> 1382, 1730
1582 <-> 832
1583 <-> 700, 1839, 1846
1584 <-> 326
1585 <-> 859, 1585
1586 <-> 1939
1587 <-> 1587, 1625
1588 <-> 345
1589 <-> 990
1590 <-> 938, 1590
1591 <-> 445
1592 <-> 719
1593 <-> 1515, 1646
1594 <-> 1894
1595 <-> 1595
1596 <-> 777, 836, 1827
1597 <-> 1820, 1870
1598 <-> 273
1599 <-> 1516, 1599, 1604
1600 <-> 653, 1470
1601 <-> 429, 794
1602 <-> 405
1603 <-> 640
1604 <-> 524, 1379, 1599
1605 <-> 1206
1606 <-> 488, 764, 1120, 1439, 1908
1607 <-> 742, 1472
1608 <-> 645, 657, 1580, 1829
1609 <-> 1176, 1609, 1694
1610 <-> 57, 1004
1611 <-> 746, 1679
1612 <-> 237, 891
1613 <-> 873, 1006
1614 <-> 307, 731
1615 <-> 1615
1616 <-> 332
1617 <-> 1058
1618 <-> 1423
1619 <-> 731
1620 <-> 200, 1858
1621 <-> 940, 1052
1622 <-> 431, 592, 1968
1623 <-> 1623
1624 <-> 23, 470
1625 <-> 1587
1626 <-> 673, 1751
1627 <-> 478, 1188
1628 <-> 369, 718
1629 <-> 759
1630 <-> 1415
1631 <-> 1157
1632 <-> 699, 1386
1633 <-> 135, 492, 1746
1634 <-> 127, 437, 522, 1044
1635 <-> 458, 538, 845, 1321
1636 <-> 924, 976
1637 <-> 1344
1638 <-> 803
1639 <-> 1040
1640 <-> 573
1641 <-> 554
1642 <-> 1642
1643 <-> 458
1644 <-> 1755, 1794
1645 <-> 259, 1708
1646 <-> 1593
1647 <-> 1060
1648 <-> 544, 1648
1649 <-> 261, 432
1650 <-> 500
1651 <-> 587
1652 <-> 1853
1653 <-> 85, 1377
1654 <-> 614, 1456
1655 <-> 1700, 1847
1656 <-> 194, 689
1657 <-> 1010, 1442, 1667
1658 <-> 469, 552
1659 <-> 1229
1660 <-> 559
1661 <-> 147, 749, 1131, 1835
1662 <-> 1970
1663 <-> 769, 1020
1664 <-> 1329, 1985
1665 <-> 945, 1344
1666 <-> 356, 1883
1667 <-> 333, 1270, 1657
1668 <-> 899, 1146
1669 <-> 1669
1670 <-> 108
1671 <-> 526, 907, 1842
1672 <-> 210, 460, 1687
1673 <-> 189, 293
1674 <-> 1179, 1918, 1936
1675 <-> 1398, 1743, 1749
1676 <-> 314
1677 <-> 537, 1028, 1350, 1740, 1857
1678 <-> 921, 1013
1679 <-> 1611
1680 <-> 1165, 1305
1681 <-> 367, 561, 933
1682 <-> 1701
1683 <-> 446
1684 <-> 1684
1685 <-> 691, 953
1686 <-> 1414, 1968
1687 <-> 757, 1672
1688 <-> 245, 993, 1143
1689 <-> 909
1690 <-> 1417
1691 <-> 696, 1201
1692 <-> 367, 967
1693 <-> 1693
1694 <-> 194, 912, 1609
1695 <-> 1473
1696 <-> 1696
1697 <-> 926, 1897
1698 <-> 658, 1329, 1698
1699 <-> 475, 1402
1700 <-> 626, 1655
1701 <-> 578, 1682, 1701
1702 <-> 307
1703 <-> 94, 1910
1704 <-> 1883
1705 <-> 1931
1706 <-> 1706
1707 <-> 549, 1707
1708 <-> 1645, 1872
1709 <-> 1709
1710 <-> 868, 1710
1711 <-> 573
1712 <-> 526
1713 <-> 967, 1713
1714 <-> 277, 1757, 1850
1715 <-> 625, 1487
1716 <-> 669
1717 <-> 475
1718 <-> 858, 1528
1719 <-> 643, 1264
1720 <-> 90, 137
1721 <-> 904
1722 <-> 299
1723 <-> 1975
1724 <-> 547, 872, 1475
1725 <-> 1155, 1943
1726 <-> 939
1727 <-> 17, 285, 946
1728 <-> 888, 1087
1729 <-> 1058
1730 <-> 114, 1581
1731 <-> 631, 765
1732 <-> 141
1733 <-> 352
1734 <-> 108, 1236
1735 <-> 689, 1191
1736 <-> 908, 1316
1737 <-> 1753
1738 <-> 1465, 1561
1739 <-> 358, 917
1740 <-> 1677, 1838
1741 <-> 361, 476, 477
1742 <-> 966, 1036
1743 <-> 18, 1675
1744 <-> 787
1745 <-> 782
1746 <-> 1633
1747 <-> 421
1748 <-> 1888
1749 <-> 1274, 1675
1750 <-> 0, 1504
1751 <-> 502, 895, 1241, 1291, 1626, 1999
1752 <-> 45, 96, 490, 725, 771, 1166
1753 <-> 717, 1544, 1737
1754 <-> 974
1755 <-> 904, 1301, 1644
1756 <-> 1313
1757 <-> 1714
1758 <-> 411, 735, 1149
1759 <-> 1759
1760 <-> 12, 1027
1761 <-> 1574
1762 <-> 583, 985
1763 <-> 799
1764 <-> 942
1765 <-> 666
1766 <-> 1579
1767 <-> 605
1768 <-> 322, 500, 681, 989, 1261
1769 <-> 89, 845, 1190
1770 <-> 969, 1572
1771 <-> 577
1772 <-> 568, 1235
1773 <-> 304, 936
1774 <-> 184, 659, 855
1775 <-> 810, 1280, 1297
1776 <-> 148, 1492
1777 <-> 1322, 1939
1778 <-> 1212, 1409, 1858
1779 <-> 377
1780 <-> 61, 682
1781 <-> 65, 1270
1782 <-> 239, 950
1783 <-> 210, 1235, 1263, 1783
1784 <-> 536, 1055
1785 <-> 35, 1066, 1785, 1885
1786 <-> 332, 862
1787 <-> 434, 1790
1788 <-> 752, 1441
1789 <-> 1789
1790 <-> 1787, 1790
1791 <-> 1019, 1791
1792 <-> 1240
1793 <-> 403
1794 <-> 740, 826, 1644
1795 <-> 1049
1796 <-> 1411, 1796
1797 <-> 216, 470
1798 <-> 439
1799 <-> 461
1800 <-> 546, 1240
1801 <-> 775
1802 <-> 456
1803 <-> 189, 435, 1184
1804 <-> 421, 1533
1805 <-> 89, 1428
1806 <-> 574, 671, 1302
1807 <-> 1084
1808 <-> 305, 662, 1824
1809 <-> 1515, 1913
1810 <-> 1132
1811 <-> 369
1812 <-> 16, 222, 769
1813 <-> 1288, 1356
1814 <-> 1094, 1814
1815 <-> 746
1816 <-> 102
1817 <-> 100, 903
1818 <-> 1239
1819 <-> 1819
1820 <-> 665, 1597
1821 <-> 204, 295, 1416
1822 <-> 261, 447, 1425
1823 <-> 663
1824 <-> 1808
1825 <-> 1825
1826 <-> 1523
1827 <-> 1596
1828 <-> 1000
1829 <-> 1062, 1608
1830 <-> 787, 1095, 1461, 1957
1831 <-> 1831
1832 <-> 255, 321
1833 <-> 907
1834 <-> 541
1835 <-> 1661
1836 <-> 413
1837 <-> 1152, 1509
1838 <-> 1740
1839 <-> 1178, 1583
1840 <-> 889, 899
1841 <-> 243, 340, 726, 774, 1161, 1304
1842 <-> 1265, 1671
1843 <-> 152
1844 <-> 483
1845 <-> 159, 326, 584
1846 <-> 674, 1064, 1583
1847 <-> 1557, 1655
1848 <-> 147
1849 <-> 1335
1850 <-> 1714
1851 <-> 1851, 1994
1852 <-> 1133
1853 <-> 1437, 1652
1854 <-> 156, 1021, 1308, 1535
1855 <-> 336
1856 <-> 1936, 1979
1857 <-> 1026, 1232, 1677
1858 <-> 128, 1620, 1778
1859 <-> 181, 1450, 1925
1860 <-> 1084, 1192, 1860
1861 <-> 1033, 1040
1862 <-> 570, 1037, 1214
1863 <-> 269
1864 <-> 890, 1558
1865 <-> 940
1866 <-> 321, 1328
1867 <-> 386, 1479
1868 <-> 336, 1237
1869 <-> 1138
1870 <-> 1449, 1597
1871 <-> 166
1872 <-> 36, 1708
1873 <-> 1536
1874 <-> 259
1875 <-> 1875
1876 <-> 360
1877 <-> 660
1878 <-> 171, 1352
1879 <-> 1879
1880 <-> 775, 1929
1881 <-> 483
1882 <-> 1200
1883 <-> 11, 733, 881, 1289, 1666, 1704
1884 <-> 1333
1885 <-> 1785
1886 <-> 371, 1040, 1333
1887 <-> 261, 1964
1888 <-> 993, 1748
1889 <-> 618, 1358
1890 <-> 1890
1891 <-> 223, 271, 554
1892 <-> 330, 1060
1893 <-> 758, 846
1894 <-> 980, 1097, 1594
1895 <-> 22, 1087, 1166
1896 <-> 572, 1081
1897 <-> 1104, 1697
1898 <-> 725
1899 <-> 81, 1946
1900 <-> 1406
1901 <-> 404, 803
1902 <-> 234
1903 <-> 38, 383
1904 <-> 1554
1905 <-> 1905
1906 <-> 1503
1907 <-> 1494, 1907
1908 <-> 1606
1909 <-> 1909
1910 <-> 662, 1703
1911 <-> 732, 1463
1912 <-> 1326
1913 <-> 1024, 1809, 1913
1914 <-> 784, 896, 1318
1915 <-> 1487
1916 <-> 1975
1917 <-> 1344
1918 <-> 1674, 1980
1919 <-> 499, 1919
1920 <-> 894
1921 <-> 391, 1047
1922 <-> 214, 1203
1923 <-> 672, 777, 1125
1924 <-> 1924
1925 <-> 1241, 1859
1926 <-> 898, 1481
1927 <-> 21
1928 <-> 895
1929 <-> 401, 1880
1930 <-> 942
1931 <-> 906, 1100, 1327, 1705
1932 <-> 501, 507
1933 <-> 525, 626
1934 <-> 1049
1935 <-> 1938
1936 <-> 364, 1674, 1856
1937 <-> 1397, 1415
1938 <-> 508, 810, 1935
1939 <-> 1586, 1777
1940 <-> 633, 1435
1941 <-> 275, 746
1942 <-> 1145
1943 <-> 315, 373, 585, 755, 1285, 1398, 1725
1944 <-> 1453
1945 <-> 184
1946 <-> 103, 229, 1899
1947 <-> 888
1948 <-> 66, 635
1949 <-> 456, 662
1950 <-> 2, 129
1951 <-> 596, 1391
1952 <-> 18, 76
1953 <-> 460, 576
1954 <-> 136, 929
1955 <-> 80
1956 <-> 261
1957 <-> 110, 1142, 1312, 1830
1958 <-> 1035
1959 <-> 317, 1534
1960 <-> 565
1961 <-> 483, 1273, 1320, 1961
1962 <-> 332
1963 <-> 142, 839
1964 <-> 1409, 1887
1965 <-> 1501
1966 <-> 1966
1967 <-> 1380
1968 <-> 1622, 1686
1969 <-> 29, 683, 1382
1970 <-> 1256, 1662
1971 <-> 902
1972 <-> 1972
1973 <-> 866
1974 <-> 1974
1975 <-> 1723, 1916
1976 <-> 416, 516
1977 <-> 310, 1290
1978 <-> 1429
1979 <-> 1284, 1856
1980 <-> 251, 1344, 1436, 1918
1981 <-> 846, 1251
1982 <-> 935, 1271
1983 <-> 1334
1984 <-> 289, 1984
1985 <-> 270, 1664
1986 <-> 1427, 1461
1987 <-> 422
1988 <-> 1173
1989 <-> 895, 1556
1990 <-> 1990
1991 <-> 439, 1991
1992 <-> 819, 1086, 1473
1993 <-> 1309, 1547
1994 <-> 1851
1995 <-> 1995
1996 <-> 153, 392, 407
1997 <-> 928
1998 <-> 76, 1237, 1252
1999 <-> 1751")

(defvar *day-12-tree*)

(setf *day-12-tree* (make-hash-table :test 'equal))
(dolist (line (cl-ppcre:split "\\n" *day-12-input*))
  (destructuring-bind (node children-as-string) (cl-ppcre:split " <-> " line)
    (setf (gethash node *day-12-tree*) (cl-ppcre:split ", " children-as-string))))


					; part 1

(length
 (let ((program-id "0"))
   (do ((programs-to-visit (list program-id) (append
					      (cdr programs-to-visit)
					      (remove-if
					       (lambda (node)
						 (member node visited-nodes :test 'equal))
					       (gethash (car programs-to-visit) *day-12-tree*))))
	(visited-nodes '() (adjoin (car programs-to-visit) visited-nodes :test 'equal)))
       ((eql NIL programs-to-visit) visited-nodes))))

					; part 2

(defun group-for-program-id (program-id)
  (do ((programs-to-visit (list program-id) (append
					     (cdr programs-to-visit)
					     (remove-if
					      (lambda (node)
						(member node visited-nodes :test 'equal))
					      (gethash (car programs-to-visit) *day-12-tree*))))
       (visited-nodes '() (adjoin (car programs-to-visit) visited-nodes :test 'equal)))
      ((eql NIL programs-to-visit) (sort visited-nodes #'string<))))

(length
 (reduce (lambda
	     (groups node)
	   (let ((group (group-for-program-id node)))
	     (adjoin group groups :test 'equal)))
	 (alexandria:hash-table-keys *day-12-tree*)
	 :initial-value '()))

;; day 13

(defvar *day-13-input*)
(setf *day-13-input* "0: 3
1: 2
4: 4
6: 4")

(setf *day-13-input* "0: 5
1: 2
2: 3
4: 4
6: 6
8: 4
10: 6
12: 10
14: 6
16: 8
18: 6
20: 9
22: 8
24: 8
26: 8
28: 12
30: 12
32: 8
34: 8
36: 12
38: 14
40: 12
42: 10
44: 14
46: 12
48: 12
50: 24
52: 14
54: 12
56: 12
58: 14
60: 12
62: 14
64: 12
66: 14
68: 14
72: 14
74: 14
80: 14
82: 14
86: 14
90: 18
92: 17")

(defun update-layers (layers)
  (dolist (layer (alexandria:hash-table-keys layers))
    (destructuring-bind (depth scanner-position direction) (gethash layer layers)
      (let* ((next-position (+ direction scanner-position))
	     (changing-directions (or (>= next-position depth) (< next-position 0)))
	     (new-direction (if changing-directions (- direction) direction)))
	(setf (gethash layer layers)
	      (list depth
		    (+ new-direction scanner-position)
		    new-direction)))))
  layers)

(defun reset-layers (input-string)
  (let ((layers (make-hash-table)))
    (dolist (line (cl-ppcre:split "\\n" input-string))
      (destructuring-bind (layer depth) (cl-ppcre:split ": " line)
	(setf (gethash (parse-integer layer) layers) (list (parse-integer depth) 0 1))))
    layers))

(defun got-hit? (layer)
  (when layer
    (destructuring-bind (_ scanner-position _) layer
      (= 0 scanner-position))))

(defun update-firewall-state (state my-position)
  (destructuring-bind (layers layers-i-got-hit-on) state
    (let* ((layer (gethash my-position layers))
	   (was-hit (got-hit? layer)))
      (list
       (update-layers layers)
       (if was-hit
	   (cons my-position layers-i-got-hit-on)
	   layers-i-got-hit-on)))))

(let ((layers (reset-layers *day-13-input*)))
  (apply '+
	 (map 'list
	      (lambda (layer)
		(* layer (first (gethash layer layers))))
	      (second
	       (reduce #'update-firewall-state
		       (alexandria:iota (1+ (apply 'max (alexandria:hash-table-keys layers))))
		       :initial-value (list layers '()))))))

; part 2

(defun will-hit? (layer depth)
  (= 0 (mod layer (* 2 (1- depth)))))

(defun gets-hit-with-delay? (delay layers)
  (some (lambda (key)
	  (will-hit? (+ delay key)
		     (first (gethash key layers))))
	(alexandria:hash-table-keys layers)))

(let ((layers (reset-layers *day-13-input*)))
  (do ((i 0 (1+ i)))
      ((not (gets-hit-with-delay? i layers)) i)))

(defun print-hash-table (table)
  (maphash #'(lambda (k v) (format t "~a => ~a~%" k v)) table))

;; day 14

					; part 1

(let ((input-string "xlqgujun"))
  (apply '+
	 (map 'list
	      (lambda (n)
		(count #\1 (write-to-string (parse-integer (knot-hash (format nil "~a-~a" input-string n)) :radix 16) :base 2)))
	      (alexandria:iota 128))))

					; part 2

(defun d14p2 ()
  (let ((used-coordinates (let ((input-string "xlqgujun"))
			    (apply 'append
				   (map 'list
					(lambda (y-coordinate)
					  (let ((h (right-pad
						    (write-to-string (parse-integer
								      (knot-hash (format nil "~a-~a" input-string y-coordinate)) :radix 16)
								     :base 2)
						    128
						    #\0)))
					    (map 'list
						 (lambda (x) (list x y-coordinate))
						 (remove-if-not (lambda (x) (char= (elt h x) #\1)) (alexandria:iota 128)))))
					(alexandria:iota 128))))))
    (do
     ((grid used-coordinates (remove-next-region grid))
      (count 0 (1+ count)))
     ((eql grid NIL) count))))

(defun remove-next-region (grid)
  (do* ((remaining-coordinates grid (set-difference remaining-coordinates
						    (list (first coordinates-to-search))
						    :test 'equal))
	(coordinates-to-search (list (first grid)) (append (rest coordinates-to-search)
						       (remove-if-not (lambda (c)
									(member c remaining-coordinates :test 'equal))
								      (udlr-neighbors (first coordinates-to-search))))))
       ((equal NIL coordinates-to-search) remaining-coordinates)))

(defun udlr-neighbors (position)
  (destructuring-bind (x y) position
    (list
     (list (1- x) y)
     (list (1+ x) y)
     (list x      (1- y))
     (list x      (1+ y)))))

(defun right-pad (s desired-length char-to-pad)
  (let ((fill-length (- desired-length (length s))))
    (concatenate 'string
		 (make-string fill-length :initial-element char-to-pad)
		 s)))

;; day 15

					; part 1

(defun generate-next-value (factor previous-value)
  (rem (* factor previous-value) 2147483647))

(defun lower-16-bits (n)
  (logand #b1111111111111111 n))

(defun d15p1 ()
  (do
   ((a-value 873 (generate-next-value 16807 a-value))
    (b-value 583 (generate-next-value 48271 b-value))
    (num-matches 0 (if (= (lower-16-bits a-value) (lower-16-bits b-value)) (1+ num-matches) num-matches))
    (times-run 0 (1+ times-run)))
   ((= 40000000 times-run) num-matches)))

					; part 2

(defun generate-next-value-generator (factor)
  (lambda (previous-value) (rem (* factor previous-value) 2147483647)))

(defun generate-next-value-divisible-by (generator divisor previous-value)
  (do
   ((current-value (funcall generator previous-value) (funcall generator current-value)))
   ((= 0 (mod current-value divisor)) current-value)))


(defun d15p2 ()
  (let ((a-generator (generate-next-value-generator 16807))
	(b-generator (generate-next-value-generator 48271)))
    (do
     ((a-value 873 (generate-next-value-divisible-by a-generator 4 a-value))
      (b-value 583 (generate-next-value-divisible-by b-generator 8 b-value))
      (num-matches 0 (if (= (lower-16-bits a-value) (lower-16-bits b-value)) (1+ num-matches) num-matches))
      (times-run 0 (1+ times-run)))
     ((= 5000000 times-run) num-matches))))


;; day 16

(defun handle-dance-move (state statement)
  (let ((instruction (subseq statement 0 1))
	(operand (subseq statement 1)))
    (cond
      ((equal instruction "s")
       (alexandria:rotate state (parse-integer operand)))

      ((equal instruction "x")
       (destructuring-bind (i j) (cl-ppcre:split "/" operand)
	 (let ((r (copy-seq state))
	       (i (parse-integer i))
	       (j (parse-integer j)))
	   (setf (subseq r i (1+ i)) (subseq state j (1+ j)))
	   (setf (subseq r j (1+ j)) (subseq state i (1+ i)))
	   r)))

      ((equal instruction "p")
       (destructuring-bind (i j) (cl-ppcre:split "/" operand)
	 (let* ((r (copy-seq state))
		(i (elt i 0))
		(j (elt j 0))
		(i-position (position i state :test 'equal))
		(j-position (position j state :test 'equal)))
	   (setf (subseq r i-position (1+ i-position)) (subseq state j-position (1+ j-position)))
	   (setf (subseq r j-position (1+ j-position)) (subseq state i-position (1+ i-position)))
	   r))))))


(defun d16p1 ()
  (let ((dancers "abcdefghijklmnop")
	(program (alexandria:read-file-into-string "day16.txt")))
    (reduce #'handle-dance-move (cl-ppcre:split "," program) :initial-value dancers)))

					; part 2

(defun execute-dance (state program)
  (reduce #'handle-dance-move (cl-ppcre:split "," program) :initial-value state))


(defun d16p2 ()
  (print-hash-table
   (let ((program (alexandria:read-file-into-string "day16.txt"))
	 (seen-states (make-hash-table :test 'equal)))
     (do*
      ((state "abcdefghijklmnop" (execute-dance state program))
       (count 0 (1+ count))
       (seen-before NIL (gethash state seen-states))
       (seen-states seen-states (progn (setf (gethash state seen-states) count) seen-states)))
      (seen-before seen-states)))))

;; day 17

(defun d17p1 ()
  (let ((l
	 (let ((puzzle-input 329))
	   (do
	    ((spinlock '(0) (cons i (alexandria:rotate spinlock (- (1+ puzzle-input)))))
	     (i 1 (1+ i)))
	    ((= i 50000001)  spinlock)))))
    (alexandria:rotate l (- (position 0 l)))))

					; part 2

(defun d17p2 ()
  (let ((steps 329))
    (do
     ((position 0 (1+ (rem (+ steps position) size)))
      (size 1 (1+ size))
      (next-to-0 0 (if (= 1 (1+ (rem (+ steps position) size))) size next-to-0)))
     ((= 50000001 size) next-to-0))))

(d17p2)

;; day 17

(defun get-register-value (registers register)
  (or (gethash register registers) 0))

(defun set-register-value (registers register value)
  (setf (gethash register registers) value))

(defun value-of (registers arg)
  (let ((integer-version (parse-integer arg :junk-allowed T)))
    (if integer-version
	integer-version
	(get-register-value registers arg))))

(defun evaluate-statement (state statement)
  (destructuring-bind (registers instruction-counter) state
    (destructuring-bind (operator args) statement
       (cond
	 ((equal operator "set")
	  (progn
	    (set-register-value registers (first args) (value-of registers (second args)))
	    (list registers (1+ instruction-counter))))

	 ((equal operator "snd")
	  (progn
	    (set-register-value registers "last-sound" (value-of registers (first args)))
	    (list registers (1+ instruction-counter))))

	 ((equal operator "add")
	  (progn
	    (set-register-value registers (first args) (+ (value-of registers (first args))
							  (value-of registers (second args))))
	    (list registers (1+ instruction-counter))))

	 ((equal operator "mul")
	  (progn
	    (set-register-value registers (first args) (* (value-of registers (first args))
							  (value-of registers (second args))))
	    (list registers (1+ instruction-counter))))

	 ((equal operator "mod")
	  (progn
	    (set-register-value registers (first args) (mod (value-of registers (first args))
							    (value-of registers (second args))))
	    (list registers (1+ instruction-counter))))

	 ((equal operator "rcv")
	  (progn
	    (when (/= 0 (value-of registers (first args)))
		(set-register-value registers "last-recovered-sound" (get-register-value registers "last-sound")))
	    (list registers (1+ instruction-counter))))

	 ((equal operator "jgz")
	  (progn
	    (list registers (if (> (value-of registers (first args)) 0)
				(+ (value-of registers (second args)) instruction-counter)
				(1+ instruction-counter)))))))))


(let ((statement '("set" ("a" "1")))
      (registers (make-hash-table :test 'equal))
      (instruction-counter 0)
      (program '(("set" ("a" "1"))
		 ("add" ("a" "2"))
		 ("mul" ("a" "a"))
		 ("mod" ("a" "5"))
		 ("snd" ("a"))
		 ("set" ("a" "0"))
		 ("rcv" ("a"))
		 ("jgz" ("a" "-1"))
		 ("set" ("a" "1"))
		 ("jgz" ("a" "-2")))))
  (let ((new-state (reduce #'evaluate-statement program :initial-value (list registers instruction-counter))))
    (format t "Instruction Counter: ~a~%" (second new-state))
    (print-hash-table (first new-state))))

(defun parse-program-text (program-text)
  (map 'vector
       (lambda (statement)
	 (destructuring-bind (operator &rest arguments) (cl-ppcre:split " " statement)
	   (list operator arguments)))
       (cl-ppcre:split "\\n" program-text)))

(defun d18p1 ()

  (get-register-value
   (first
    (let ((program (parse-program-text "set i 31
set a 1
mul p 17
jgz p p
mul a 2
add i -1
jgz i -2
add a -1
set i 127
set p 680
mul p 8505
mod p a
mul p 129749
add p 12345
mod p a
set b p
mod b 10000
snd b
add i -1
jgz i -9
jgz a 3
rcv b
jgz b -1
set f 0
set i 126
rcv a
rcv b
set p a
mul p -1
add p b
jgz p 4
snd a
set a b
jgz 1 3
snd b
set f 1
add i -1
jgz i -11
snd a
jgz f -16
jgz a -19")))
      (do
       ((state (list (make-hash-table :test 'equal) 0) (evaluate-statement state (elt program (second state)))))
       ((or (< (second state) 0)
	    (>= (second state) (length program))
	    (/= 0 (get-register-value (first state) "last-recovered-sound"))) state))))
   "last-recovered-sound"))

					; part 2

(defun evaluate-statement2 (registers snd rcv statement)
  (destructuring-bind (operator args) statement
    (cond
      ((equal operator "set")
       (progn
	 (set-register-value registers (first args) (value-of registers (second args)))
	 (set-register-value registers ".pc" (1+ (get-register-value registers ".pc")))))

      ((equal operator "add")
       (progn
	 (set-register-value registers (first args) (+ (value-of registers (first args))
						       (value-of registers (second args))))
	 (set-register-value registers ".pc" (1+ (get-register-value registers ".pc")))))

      ((equal operator "mul")
       (progn
	 (set-register-value registers (first args) (* (value-of registers (first args))
						       (value-of registers (second args))))
	 (set-register-value registers ".pc" (1+ (get-register-value registers ".pc")))))

      ((equal operator "mod")
       (progn
	 (set-register-value registers (first args) (mod (value-of registers (first args))
							 (value-of registers (second args))))
	 (set-register-value registers ".pc" (1+ (get-register-value registers ".pc")))))

      ((equal operator "jgz")
       (set-register-value registers
			   ".pc"
			   (if (> (value-of registers (first args)) 0)
			       (+ (value-of registers (second args)) (get-register-value registers ".pc"))
			       (1+ (get-register-value registers ".pc")))))

      ((equal "snd" operator)
       (progn
	 (set-register-value registers ".snd-count" (1+ (get-register-value registers ".snd-count")))
	 (set-register-value registers ".pc" (1+ (get-register-value registers ".pc")))
	 (queues:qpush snd (value-of registers (first args)))))

      ((equal "rcv" operator)
       (if (= 0 (queues:qsize rcv))
	   (progn
	     (set-register-value registers ".waiting" 1))
	   (progn
	     (set-register-value registers (first args) (queues:qpop rcv))
	     (set-register-value registers ".pc" (1+ (get-register-value registers ".pc")))))))))

(defun next-machine-state (registers snd-queue rcv-queue program)
  (let ((pc (get-register-value registers ".pc")))
    (when (and (>= pc 0) (< pc (length program)))
      (evaluate-statement2 registers snd-queue rcv-queue (elt program pc)))))

(defun print-state ()
  (format t "~%Registers (A)~%------~%")
  (print-hash-table *registers-a*)

  (format t "~%Registers (B)~%------~%")
  (print-hash-table *registers-b*)

  (format t "Queue (A)~%------~%")
  (queues:print-queue *a-queue*)

  (format t "Queue (B)~%------~%")
  (queues:print-queue *b-queue*))

(defun is-waiting? (registers)
  (/= 0 (get-register-value registers ".waiting")))

(defun run-until-waiting (registers snd-queue rcv-queue program)
  (do
   ((current-state T ))
   ((is-waiting? registers) T)))

(defvar *a-queue*)
(defvar *b-queue*)
(defvar *registers-a*)
(defvar *registers-b*)

(defun reset-parallel-state ()
  (setf *a-queue* (queues:make-queue :simple-queue))
  (setf *b-queue* (queues:make-queue :simple-queue))
  (setf *registers-a* (make-hash-table :test 'equal))
  (setf *registers-b* (make-hash-table :test 'equal))
  (setf (gethash ".pc" *registers-a*) 0)
  (setf (gethash ".pc" *registers-b*) 0)
  (setf (gethash ".waiting" *registers-a*) NIL)
  (setf (gethash ".waiting" *registers-b*) NIL))



(defun d18p2 ()
  ;; WIP...not nearly done
  (reset-parallel-state)
  (let ((program (parse-program-text (alexandria:read-file-into-string "day18.asm"))))
    (loop
       (next-machine-state *registers-a* *a-queue* *b-queue* program)
       (next-machine-state *registers-b* *b-queue* *a-queue* program)
       (print-state)
       (let ((input (prompt-read "stop?")))
	 (when (equal input "y")
	   (return-from d18p2))))))


;; day 19

(defun get-grid-position (grid position)
  (handler-case
      (aref grid (second position) (first position))
    (sb-int:invalid-array-index-error () #\ )
    (type-error () #\ )))

(defun move (position direction)
  (destructuring-bind (x y) position
    (case direction
      (:down  (list x (1+ y)))
      (:up    (list x (1- y)))
      (:left  (list (1- x) y))
      (:right (list (1+ x) y)))))

(defun collect-direction (grid position direction)
  (do*
   ((position position (move position direction))
    (whats-here (get-grid-position grid position) (get-grid-position grid position))
    (collected-chars (list whats-here) (append collected-chars (list whats-here))))
   ((not (or (char= whats-here #\-) (char= whats-here #\|))) (list position collected-chars))))

(defun can-move? (position direction grid)
  (let* ((candidate-position (move position direction))
	 (c (get-grid-position grid candidate-position))
	 (vertical? (or (eql direction :down) (eql direction :up)))
	 (horizontal? (or (eql direction :left) (eql direction :right))))
    (or
     (not
      (or
       (char= c #\ )
       (and vertical? (char= c #\-))
       (and horizontal? (char= c #\|))))
     (and vertical? (char= c #\-) (can-move? (move candidate-position direction) direction grid))
     (and horizontal? (char= c #\|) (can-move? (move candidate-position direction) direction grid)))))



(defun next-direction (grid position previous-direction)
  (case previous-direction
    (:up (find-if (lambda (direction) (can-move? position direction grid))    '(:up :left :right)))
    (:down (find-if (lambda (direction) (can-move? position direction grid))  '(:down :left :right)))
    (:left (find-if (lambda (direction) (can-move? position direction grid))  '(:left :up :down)))
    (:right (find-if (lambda (direction) (can-move? position direction grid)) '(:right :up :down)))
    (nil nil)))

(defun collect-grid-state (state grid)
  (destructuring-bind (position direction collected) state
    (destructuring-bind (new-position new-chars) (collect-direction grid position direction)
      (let ((new-direction (next-direction grid new-position direction)))
	(list (move new-position new-direction) new-direction (append collected new-chars))))))

(defun traverse-grid (grid position direction)
  (do
   ((state (list position direction '()) (collect-grid-state state grid)))
   ((eql NIL (second state)) (format nil "~{~A~}" (third state)))))

(defun d19p1 ()
  (let* ((lines (cl-ppcre:split "\\n" (alexandria:read-file-into-string "day19.txt")))
	 (width (length (first lines)))
	 (height (length lines))
	 (grid (make-array (list height width) :initial-contents lines))
	 (entry-position '(147 0))
	 (direction :down))
    (cl-ppcre:regex-replace-all "[^A-Z]" (traverse-grid grid entry-position direction) "")))

; part 2

(defun d19p2 ()
  (let* ((lines (cl-ppcre:split "\\n" (alexandria:read-file-into-string "day19.txt")))
	 (width (length (first lines)))
	 (height (length lines))
	 (grid (make-array (list height width) :initial-contents lines))
	 (entry-position '(147 0))
	 (direction :down))
    (length (traverse-grid grid entry-position direction))))


;; day 20

(defun update-particle (particle)
  (let ((new-v (map 'list #'+ (second particle) (third particle))))
    (list (map 'list #'+ (first particle) new-v) new-v (third particle))))

(defun parse-particle (line)
  (let ((ints (map 'list #'parse-integer (cl-ppcre:split "," (cl-ppcre:regex-replace-all "[^\-0-9,]" line "")))))
    (list (subseq ints 0 3) (subseq ints 3 6) (subseq ints 6 9))))

(defun d20p1 ()

  (let* ((input (alexandria:read-file-into-string "day20.txt"))
	 (lines (cl-ppcre:split "\\n" input))
	 (particles (map 'list #'parse-particle lines)))
    (do
     ((particles particles (map 'list #'update-particle particles))
      (iterations 1000 (1- iterations)))
     ((= 0 iterations) (map 'list (alexandria:compose #'manhattan-distance #'first) particles))))

  (let ((distances '(6609645 4449797 16352805 6990383 9668787 9415315 5851930 4949030 7065650
		     7654163 2505215 12872992 6237042 7443293 6088407 6480702 11767044 11856861
		     9848256 8823822 6598665 7096092 12948017 8021796 4589932 15960425 8071568
		     12462126 4493086 6595250 11943240 6118472 8076352 10509032 8975246 8634256
		     15715994 4034299 12110577 7584856 15462666 9514476 6685148 12921385 1162845
		     1740328 7483904 7146693 11102437 4300575 8403689 11835270 13912449 11648500
		     7515724 9631856 12719368 9955529 6079537 6513102 10354674 14880325 6899507
		     5057725 8449319 10534835 10604582 17623246 13606662 10803146 9583243 11997475
		     12600054 9475814 3875005 6321554 11521216 5091270 9099764 7665720 4395878
		     6524650 6418560 5498018 9397716 7230728 5616983 7503194 3140702 3610412
		     9404122 1496258 11475829 7180439 5003950 5751977 11870208 5045500 6569365
		     13364797 6404541 6561295 5113566 8552263 7127728 6991306 8095874 12091180
		     10515889 14944999 1414631 14607331 10422819 4406657 5800619 8551865 9854171
		     11520709 2966851 16510623 15119123 7579025 2586081 11042045 12104375 10559383
		     14111560 11584863 8338145 9484849 8622837 8468015 5336852 11082437 4399058
		     19076070 10183968 10866848 11830170 8447157 3043772 6496513 10894256 10523370
		     697490 13406364 16336257 25086220 13958445 17105757 13358584 12637748 5985513
		     13000483 10499441 12919463 4973109 3931053 4518787 6614478 7499583 5033964
		     7095392 10680579 5621047 8847089 11401775 9806837 6825050 3654276 2932750
		     10009396 8619024 8029175 3691304 11589871 13984471 11630725 18396325 6813196
		     7745493 3915372 11085296 9613665 5550093 8336828 7539685 8586580 12537665
		     4868605 14598605 3956295 13778791 13512739 7332889 6988371 8922498 9583368
		     5155038 4532055 12155094 6893931 7139793 9987724 6195560 3431739 7681031
		     8346885 11537284 8435535 7527385 9554499 11065495 2670320 4561330 4021598
		     4480650 4024562 6457502 4063910 9955118 11912615 10018414 12018995 9879559
		     7097901 14764202 7003329 10142003 9094807 5075871 9070157 6628108 7064331
		     4030501 7961712 6120649 7863969 14441175 4687441 3138554 12652133 6502661
		     6539305 12507253 11338477 15900400 1450481 8170396 8046191 14058979 10315243
		     14018228 1409894 6063348 11102232 5040084 10870804 9443471 4052694 14958336
		     5923824 10956178 6077461 3163867 9524086 2988814 12025307 5324823 6491499
		     14557221 12915840 6187545 9552158 4518514 5481982 7379002 2258428 8544254
		     7990550 7018452 10686748 7612988 4089071 5948007 10071369 4699298 9452264
		     17041661 5960707 7128315 10283527 9564665 10568158 5865494 6755191 11434189
		     6814179 1014531 5227669 10470893 12054544 1947912 10488319 13613547 9496531
		     14526444 7022835 15465913 16787994 7498506 5834598 7521270 10005246 2103858
		     2423713 5309713 6003804 10483441 6123158 15129410 2625733 13578592 4824842
		     6507530 6098040 7020360 10036258 10717442 4898558 5048856 8018766 5522569
		     2620581 9111086 3270860 1948731 12494050 9600576 9994845 13375850 8376400
		     1603894 11588140 11447154 5981247 2616732 2124435 7016883 10487188 7587830
		     17541626 10799994 13465360 15007890 3973678 13923730 5463463 8186409 10107285
		     7536610 9541077 6065031 6115294 5364339 8145149 14227277 8121283 7903993
		     2093595 7671403 7918813 5547049 7156091 7166233 5971858 7075558 6330450
		     11952885 2960647 10407708 8388748 13392638 12928623 6546578 5680146 8910871
		     6025365 6474708 8018376 17363300 4235406 8906971 10629512 16389539 7472008
		     11038656 7013607 11515238 14640436 6458476 3947781 3545088 7369916 2123869
		     6964378 5441171 8092570 11084215 9557644 10113255 6747644 7592752 2504980
		     13143439 4088901 8443633 9294233 9055968 8080161 13088193 6530676 3480964
		     7389686 7503062 5616640 6590932 5943499 14086873 11911032 9116636 6760548
		     10993017 10544808 6601218 10605097 5026474 3445429 19521804 7751464 8921626
		     12003218 9735268 8165834 6834994 10358700 10911478 9332416 13536983 20282214
		     3374606 8835314 7517964 8049957 5543055 15131760 7980867 8969919 11845002
		     6522111 12289044 10070913 6891304 10335002 10525987 8126484 5426107 5012340
		     7880688 5044809 5558888 9425057 7019580 9034624 6501363 8649895 14979040
		     10988802 7402965 12615671 3536943 2875280 4502249 7438092 8434649 12827472
		     8097388 6422361 2906977 6875718 8582770 7177718 11668754 3926194 5520054
		     3962046 3917474 5130334 18090460 12385319 3151524 13678764 9567294 6446760
		     8067149 11108550 13987723 9541416 19108132 16131261 9460251 12399047 4183151
		     13527044 7097938 7914788 15486507 11933780 9939599 12968656 10207815 9245686
		     5486070 22862946 22880962 20896199 26776342 22809540 21852831 19538683
		     14493377 21956582 24288830 29650350 18903258 22970643 29730910 16404816
		     21506384 19443015 16960609 18935420 20878011 24785335 28729957 22381517
		     19468166 18943556 24359412 20341237 24309993 19513508 25362396 24239481
		     24762211 17492322 18983734 24320084 23888058 20301975 19835737 24834652
		     18487298 21421820 18473163 17992829 20332176 20943403 29873865 18940512
		     31124734 21360399 20377561 20886111 22318032 31161975 23876937 27693753
		     21818561 23887097 19927349 18953576 21379508 20889160 12075429 26799372
		     24266699 18548670 15864978 21824645 17400721 18059299 25759075 18043181
		     23228463 14942532 27237477 24398613 16519505 17517524 26194202 16060344
		     22281828 22833729 22872957 22381540 19133749 28175989 17478212 17915265
		     21387546 27749122 21923350 21848850 15500363 19880056 25767255 28226446
		     22353333 21340298 20778343 21305038 16367535 23307978 25353365 25849774
		     24266716 14874055 14974604 16859956 20878018 21355348 23300968 25850833
		     23355338 21410703 20474181 29677580 17869963 20514476 18423896 24758204
		     23402671 19358392 20290962 29677528 19011942 20425877 31142783 18883064
		     18326147 13600150 26785344 22863849 19101656 23894076 22328138 21360308
		     15945453 20809603 24943443 20856900 20544679 28356255 19424896 22433844
		     23869938 18896116 24322128 20438911 20890064 25331209 20420810 17919376
		     26724884 23788334 26191175 25294900 19344352 29254630 26709851 18552702
		     19273696 17640376 19048233 23826569 17531659 25352320 18334175 21342267
		     26740976 16001917 26256629 20862942 17038074 27267670 23415758 18068409
		     21376490 14575934 16496409 27731942 17468107 22495329 29158984 18943510
		     20823659 24277809 21454104 20458097 22362392 24865798 20356473 19961620
		     15978745 23325150 19439974 14985784 20836813 17385641 26235493 15511460
		     21374452 15947540 25897124 20934392 18387627 24341214 26336176 21474195
		     26678583 20419759 24257650 23416783 24855863 19394674 21932438 20859872
		     30234484 17428889 18067382 20923353 20864891 21377498 20415828 19396716
		     18901155 16892163 21408648 22286839 25456024 24295921 15966718 21472147
		     20826651 18976792 22343268 16410772 20001849 17943417 22819638 13114716
		     21302034 21871991 19566809 17426861 21818635 20394671 21313064 21834669
		     29721838 25301936 27340146 15910301 24688667 20248660 21837714 27338164
		     23405682 20405730 19439975 16468190 26369483 25808529 25667507 22839704
		     21319087 15042236 26238483 18533537 15452050 18854875 22435831 16442046
		     20408761 22228495 19480190 23779258 21945484 13404757 23235522 15727031
		     25806396 26945421 17089542 24863962 18919274 20850876 16660478 22334258
		     18031090 23835742 16060310 26824592 22909240 29620197 14144824 15062374
		     24774300 24304961 19985778 27196215 23343235 17007046 20052260 22416814
		     14080466 16959613 23265720 28751104 20838786 19051234 14489349 17066379
		     28214268 20879011 27343259 19875979 20825713 17368475 18853881 28307955
		     18911299 19366433 20988775 18468139 15593030 21806524 24854799 19452093
		     19416794 20790371 20499406 20898079 25885985 24411720 16373521 18860948
		     17377480 27791313 13960624 21335273 20965603 27239476 21839764 21907192
		     17900109 26303978 25201275 24792419 18403697 21378503 25838683 23438913
		     15119692 19306060 21969683 21802481 21446997 19988806 23275776 17030187
		     26773310 28774272 22395649 29810440 18990786 22419819 24349322 13785427
		     19856957 23835715 19862015 22414809 13047275 22329234 21372491 23787351
		     22788423 22477148 21855922 21406704 15608191 17908221 24721885 23853856
		     21351251 23278826 15027098 22796396 19358434 26644319 19072340 15542675
		     18042218 19971700 18919294 21429822 17388571 21852895 16511453 19401627
		     17798514 22286873 23746079 17991812 24920351 20415828 14622275 20334225
		     16872024 21456042 26300006 19375572 24258663 22858787 21444039 23330189
		     24445962 17361398 20314069 26241569 16556755 20860935 21307056 22783349
		     22296944 24343330 19910298 19383591 19888059 22905159 21847815 17875003
		     24739056 25718856 21860840 21831686 17430972 18453043 28264696 17902186
		     20471098 29283838 23748091 22346288 23345338 19950481 19036108 31652344
		     25817552 16487323 24317024 20310003 27802494 19893126 13493369 15063342
		     12662624 23724913 16910289 23279838 21013001 24368461 21329139 15681627
		     27792402 17979739 20780295 23358371 24404710 27327106 18888090 25309953
		     23720878 23328199 19405719 21929368 22918304 24405717)))
    (position 697490 distances)))


					; part 2

(defun collided-particle-positions (particles)
  (let ((particle-positions-bag (make-hash-table :test 'equal))
	(collided-positions-set (make-hash-table :test 'equal)))
    (dolist (particle particles)
      (let ((position (first particle)))
	(setf (gethash position particle-positions-bag) (1+ (gethash position particle-positions-bag 0)))))
    (dolist (position (alexandria:hash-table-keys particle-positions-bag))
      (when (> (gethash position particle-positions-bag) 1)
	(setf (gethash position collided-positions-set) 1)))
    collided-positions-set))

(defun remove-collided-particles (particles collided-positions)
  (remove-if (lambda (particle) (gethash (first particle) collided-positions)) particles))

(defun d20p2 ()
  (length
   (let* ((input (alexandria:read-file-into-string "day20.txt"))
	  (lines (cl-ppcre:split "\\n" input))
	  (particles (map 'list #'parse-particle lines)))
     (do
      ((particles particles (let ((updated-particles (map 'list #'update-particle particles)))
			      (remove-collided-particles updated-particles (collided-particle-positions updated-particles))))
       (iterations 1000 (1- iterations)))
      ((= 0 iterations) particles)))))

;; day 21

(defvar start-grid (make-array '(3 3) :initial-contents '((1 0 0) (0 1 0) (0 0 1))))

(defvar four-grid (make-array '(4 4) :initial-contents '((1 1 2 2) (1 1 2 2) (3 3 4 4) (3 3 4 4))))

					; take a grid and explode it into 2x2 grids
;;(cl-slice:slice four-grid (cons 0 2) t)




(defun explode-grid-2 (remaining-rows)
					; slice off two rows, build our grids by chunking into 2s then zipping them together
					; then recur for the next row of grids
  0)




;(cl-slice:slice four-grid (cons 2 4) t)

;(array-length four-grid)


;; day 22

(defun d22-get-grid (grid posn)
  (gethash posn grid))

(defun d22-set-grid (grid posn value)
  (setf (gethash posn grid) value))

(defun d22-generate-grid (input)
  (let* ((height (length input))
	 (width (length (car input)))
	 (input-grid (make-array
		      (list height width)
		      :initial-contents input))
	 (grid (make-hash-table :test `equal)))
    (d22-set-grid grid 'initial-position (list (floor width 2) (floor height 2)))
    (dotimes (i height)
      (dotimes (j width)
	(let ((infected (char= #\# (aref input-grid j i))))
	  (d22-set-grid grid (list i j) infected))))
    grid))

(defun d22-rotate (left-or-right direction)
  (destructuring-bind (x y) direction
    (let ((invert-sign (or
			(and (eq 'left left-or-right)  (eq 0 y))
			(and (eq 'right left-or-right) (eq 0 x)))))
      (if invert-sign
	  (list (- y) (- x))
	  (list y x)))))

(defun d22-move (posn direction)
  (destructuring-bind ((x y) (dx dy)) (list posn direction)
    (list (+ x dx) (+ y dy))))

(defun d22-initial-state (input)
  (let* ((grid (d22-generate-grid input))
	 (posn (d22-get-grid grid 'initial-position)))
    (list grid posn '(0 -1) 0)))

(defun d22-step (state)
  (destructuring-bind (grid posn direction bursts-that-cause-infection) state
    (if (d22-get-grid grid posn)
	(progn
	  (d22-set-grid grid posn nil)
	  (let ((new-direction (d22-rotate 'right direction)))
	    (list grid (d22-move posn new-direction) new-direction bursts-that-cause-infection)))
	(progn
	  (d22-set-grid grid posn t)
	  (let ((new-direction (d22-rotate 'left direction)))
	    (list grid (d22-move posn new-direction) new-direction (1+ bursts-that-cause-infection)))))))

(defun d22p1 ()
  (do
   ((iteration 0 (1+ iteration))
    (current-state (d22-initial-state (cl-ppcre:split "\\n" (alexandria:read-file-into-string "day22.txt"))) (d22-step current-state)))
   ((= iteration 10000) current-state)))

(defun d22p2-step (state)
  (destructuring-bind (grid posn direction bursts-that-cause-infection) state
    (destructuring-bind (new-direction new-state)
	(let ((value (d22-get-grid grid posn)))
	  (cond
	    ((null value) (list (d22-rotate 'left direction) 'weakened))
	    ((eq t value) (list (d22-rotate 'right direction) 'flagged))
	    ((eq 'weakened value) (list direction t))
	    ((eq 'flagged value) (list (d22-rotate 'right (d22-rotate 'right direction)) nil))))
      (progn
	(d22-set-grid grid posn new-state)
	(list grid
	      (d22-move posn new-direction)
	      new-direction
	      (if (eq new-state t) (1+ bursts-that-cause-infection) bursts-that-cause-infection))))))

(defun d22p2 ()
  (puz:iterate #'d22p2-step
	       (d22-initial-state (cl-ppcre:split "\\n" (alexandria:read-file-into-string "day22.txt")))
	       10000000))

(defun d23-read-op (is)
  (let* ((operator (read is nil nil))
	 (op1 (read is nil nil))
	 (op2 (read is nil nil)))
    (and operator (list operator op1 op2))))

(defun d23-read-program (is &optional statements)
  (let ((op ))))

(with-open-file (is "day23.txt")
  (d23-read-op is)
  (d23-read-op is)
  (d23-read-op is))
