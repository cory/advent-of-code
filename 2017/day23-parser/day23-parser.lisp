;;;; day23-parser.lisp
(ql:quickload :puz)
(ql:quickload :alexandria)
(in-package #:day23-parser)

(defun read-op (is)
  (let* ((operator (read is nil nil))
	 (op1 (read is nil nil))
	 (op2 (read is nil nil)))
    (and operator (list operator op1 op2))))

(defun read-program (is &optional statements)
  (do*
   ((current-op (read-op is) (read-op is))
    (program (list current-op) (cons current-op program)))
   ((null current-op) (reverse (cdr program)))))

(defun init-registers ()
  (let ((h (make-hash-table :test 'equal)))
    (setf (gethash 'A h) 1)
    (setf (gethash 'B h) 0)
    (setf (gethash 'C h) 0)
    (setf (gethash 'D h) 0)
    (setf (gethash 'E h) 0)
    (setf (gethash 'F h) 0)
    (setf (gethash 'G h) 0)
    (setf (gethash 'H h) 0)
    h))

(defmacro value-of (integer-or-value)
  `(if (integerp ,integer-or-value)
      ,integer-or-value
      (gethash ,integer-or-value (registers machine-state))))

(defun emulator-step (machine-state)
  (handler-case
      (progn
	(when (= 10 (program-counter machine-state))
	  (machine-trace machine-state))
	(when (= 23 (program-counter machine-state))
	  (machine-trace machine-state))
	(let ((instruction (aref (program machine-state) (program-counter machine-state))))
	  (setf (next-program-counter machine-state) (1+ (program-counter machine-state)))
	  (apply (gethash (car instruction) *instruction-set*) (cons machine-state (cdr instruction)))
	  (setf (program-counter machine-state) (next-program-counter machine-state))
	  machine-state))
    (sb-int:invalid-array-index-error ()
      (progn
	(setf (haltedp machine-state) t)
	machine-state))))

(defparameter *instruction-set* (make-hash-table))

(defmacro definstruction (instruction-name args &body body)
  `(setf (gethash (quote ,instruction-name) *instruction-set*)
	 (lambda (machine-state ,@args)
	   ,@body
	   machine-state)))

(defun machine-trace (machine-state)
  (let ((pc (program-counter machine-state)))
    (format t "~S @ ~S A:~S B:~S C:~S D:~S E:~S F:~S G:~S H:~S S:~HALT~%"
	    (if (< (length (program machine-state)) pc)
		(aref (program machine-state) pc)
		'no-instruction)
	    pc
	    (gethash 'A (registers machine-state))
	    (gethash 'B (registers machine-state))
	    (gethash 'C (registers machine-state))
	    (gethash 'D (registers machine-state))
	    (gethash 'E (registers machine-state))
	    (gethash 'F (registers machine-state))
	    (gethash 'G (registers machine-state))
	    (gethash 'H (registers machine-state))
	    (haltedp machine-state))))

(definstruction jnz (x y)
  (when (not (zerop (value-of x)))
    (setf (next-program-counter machine-state) (+ (program-counter machine-state) (value-of y)))))

(definstruction set (x y)
  (setf (gethash x (registers machine-state)) (value-of y)))

(definstruction sub (x y)
  (setf (gethash x (registers machine-state)) (- (value-of x) (value-of y))))

(definstruction mul (x y)
  (setf (gethash x (registers machine-state)) (* (value-of x) (value-of y)))
  (incf (gethash 'mul-count (debug-registers machine-state))))

(definstruction nop ()
  (progn))

(definstruction op1 ()
  (alexandria:iota
   (- (gethash 'B (registers machine-state)) 2)
   :start 2))

(defclass d23-emulator ()
  ((program
    :initarg :program
    :reader program)
   (program-counter
    :initform 0
    :accessor program-counter)
   (next-program-counter
    :initform 0
    :accessor next-program-counter)
   (registers
    :initform (init-registers)
    :accessor registers)
   (debug-registers
    :initform (let ((h (make-hash-table :test 'equal)))
		(setf (gethash 'mul-count h) 0)
		h)
    :accessor debug-registers)
   (haltedp
    :initform nil
    :accessor haltedp)))

(with-open-file (is #p"~/Documents/Code/advent-of-code/2017/day23.txt")
  (let* ((emulator (make-instance 'd23-emulator :program (apply #'vector (read-program is))))
	 (final-state (puz:iterate #'emulator-step emulator 4000000)))
    (machine-trace final-state)))
