#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

int part1(){
  FILE * fp;
  fp = fopen("day01.txt","r");
  int sum, current;
  sum = 0;
  while(!feof(fp)){
    if(fscanf(fp, "%d", &current) == 1)
      sum += current;
  }
  fclose(fp);
  return sum;
}

int part2(){
  char * bitfield;
  FILE * fp;
  int sum, current;
  unsigned byte, bit;

  fp = fopen("day01.txt","r");
  bitfield = calloc(UINT_MAX / 8, 1);
  sum = 0;

  while(1){
    if(feof(fp))
      fseek(fp, 0, SEEK_SET);

    if(fscanf(fp, "%d", &current) == 1){
      sum += current;
      byte = (unsigned)sum / 8;
      bit = 1 << ((unsigned)sum % 8);
      if(bitfield[byte] & bit){
	return sum;
      }
      else{
	bitfield[byte] = bitfield[byte] | bit;
      }
    }
  }
  free(bitfield);
  return -1;
}

int main(){
  printf("Part 1: %d\n", part1());
  printf("Part 2: %d\n", part2());
  return 0;
}
