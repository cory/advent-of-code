#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

int INFINITY = 10000;

void fill_neighbors(int **grid, int x, int y, size_t s){
  int item = grid[y][x];
  int nbors[4][2] = {{ x, y - 1 },
		     { x, y + 1 },
		     { x + 1, y },
		     { x - 1, y }};
  int i,j,x1,y1;
  for(i = 0; i < 4; i++){
    x1 = nbors[i][0];
    y1 = nbors[i][1];
    if((x1 < 0) || (y1 < 0 ) || (x1 >= s) || (y1 >= s)){
      continue;
    }

    if((grid[y1][x1] < 0) && (grid[y1][x1] != -item)){
      grid[y1][x1] = INFINITY;
    }
    else if(grid[y1][x1] == 0){
      grid[y1][x1] = -item;
    }
  }
}

void finalize_neighbors(int **grid, size_t s){
  int i,j;
  for(i = 0; i < s; i++){
    for(j = 0; j < s; j++){
      if(grid[j][i] < 0){
	grid[j][i] = -grid[j][i];
      }
    }
  }
}

void step_fill(int **grid, size_t s){
  int x,y;
  for(y=0; y < s; y++){
    for(x=0; x < s; x++){
      if((grid[y][x] > 0) && (grid[y][x] != INFINITY)){
	fill_neighbors(grid, x, y, s);
      }
    }
  }
  finalize_neighbors(grid, s);
}

void initialize_grid(int **grid){
  FILE * fp;
  fp = fopen("day06.txt","r");
  int x,y,c;
  c = 1;
  while(!feof(fp)){
    if(fscanf(fp, "%d, %d", &x, &y) == 2){
      grid[y][x] = c;
      c++;
    }
  }
}

int topscore(int **grid, size_t s){
  int i, j, c, id_count = 60;
  int pops[id_count];
  for(i = 0; i < id_count; i++){
    pops[i] = 0;
  }
  for(i = 0; i < s; i++){
    for(j = 0; j < s; j++){
      c = grid[j][i];
      if((c >= id_count) || (c < 0))
	continue;
      if(c == INFINITY)
	continue;
      if(pops[c] == -1)
	continue;
      if((j == 0) || (j == (s - 1)) || (i == 0) || (i == (s - 1))){
	pops[c] = -1;
      }
      else if (c == 0){
	return -1;
      }
      else {
	pops[c]++;
      }
    }
  }

  j = 0;
  for(i = 0; i < id_count; i++){
    if(pops[i] > j)
      j = pops[i];
  }
  return j;
}

int part1(){
  const size_t dims = 500;
  int **grid;
  int i,j;
  grid = malloc(dims * sizeof *grid);
  for(i = 0; i < dims; i++){
    grid[i] = malloc(dims * sizeof *grid[i]);
    for(j = 0; j < dims; j++){
      grid[i][j] = 0;
    }
  }
  initialize_grid(grid);
  for(i = 0; i < dims; i++){
    step_fill(grid, dims);
  }
  return topscore(grid, dims);
}

int manhattan_distance(int x1, int y1, int x2, int y2){
  return abs(x2 - x1) + abs(y2 - y1);
}

int part2(){
  FILE * fp;
  fp = fopen("day06.txt", "r");
  int points[60][2], x, y, i, j, k, point_sum, region;
  int max_distance = 10000;
  for(i = 0; i < 60; i++){
    points[i][0] = -1;
    points[i][1] = -1;
  }
  k = 0;
  while(!feof(fp)){
    if(fscanf(fp, "%d, %d", &x, &y) == 2){
      points[k][0] = x;
      points[k][1] = y;
      k++;
    }
  }

  region = 0;
  for(i = 0; i < 500; i++){
    for(j = 0; j < 500; j++){
      point_sum = 0;
      for(k = 0; k < 60; k++){
	if(points[k][0] == -1)
	  continue;

	point_sum += manhattan_distance(i, j, points[k][0], points[k][1]);
      }
      if(point_sum < max_distance){
	region++;
      }
    }
  }
  return region;
}

int main(){
  printf("Part 1: %d\n", part1());
  printf("Part 2: %d\n", part2());
  return 0;
}
