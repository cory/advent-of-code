#include <stdio.h>
#include <stdlib.h>

struct marble{
  int number;
  struct marble * next;
  struct marble * prev;
};

struct marble * insert_marble(struct marble * current, int number){
  struct marble *new_current;
  struct marble *i;
  new_current = (struct marble *) malloc(sizeof(struct marble));
  new_current->number = number;
  if(current == NULL){
    new_current->next = new_current;
    new_current->prev = new_current;
    current = new_current;
  }
  else{
    i = current->next;
    new_current->prev = i;
    new_current->next = i->next;
    i->next->prev = new_current;
    i->next = new_current;
  }
  return new_current;
}

struct marble * remove_marble(struct marble * to_remove){
  struct marble *prev, *next;
  prev = to_remove->prev;
  next = to_remove->next;
  free(to_remove);
  prev->next = next;
  next->prev = prev;
  return next;
}

long long part1(int num_players, int last_marble){
  long long scores[num_players];
  long long high_score = 0;
  unsigned int last_marble_score = 0, i, marble_number = 0, current_player = 0;
  for(i = 0; i < num_players; i++) scores[i] = 0;
  struct marble *current = NULL, *to_remove;
  for(marble_number = 0; marble_number < last_marble; marble_number++){
     if((0 < marble_number) && (0 == (marble_number % 23))){
      to_remove = current->prev->prev->prev->prev->prev->prev->prev;
      last_marble_score = to_remove->number + marble_number;
      scores[current_player] += last_marble_score;
      if(high_score < scores[current_player])
	high_score = scores[current_player];
      current = to_remove->next;
      remove_marble(to_remove);
    }
    else{
      current = insert_marble(current, marble_number);
    }
    current_player = (current_player + 1) % num_players;
  }
  return high_score;
}

int main(){
  printf("Part 1: %lld\n", part1(459, 71790));
  printf("Part 2: %lld\n", part1(459, 7179000));
  return 0;
}
