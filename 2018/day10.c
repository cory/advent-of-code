#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#define MAX_STARS 370

struct star {
  int pos_x;
  int pos_y;
  int vel_x;
  int vel_y;
};

/* void print_stars(struct star * stars, int star_count){ */
/*   for(i = 0; i < star_count; i++){ */
/*     printf("%d %d %d %d\n", stars[i]->pos_x, stars[i]->pos_y, stars[i]->vel_x, stars[i]->vel_y); */
/*   } */
/* } */

int area(int x1, int y1, int x2, int y2){
  return abs((x2 - x1) * (y2 - y1));
}

void tick(struct star * stars[MAX_STARS], int star_count){
  int i;
  for(i = 0; i < star_count; i++){
    stars[i]->pos_x += stars[i]->vel_x;
    stars[i]->pos_y += stars[i]->vel_y;
  }
}

int find_bounding_box(struct star * stars[MAX_STARS],
		      int star_count,
		      int *mx1,
		      int *my1,
		      int *mx2,
		      int *my2){
  int x1 = INT_MAX, x2 = INT_MIN, y1 = INT_MAX, y2 = INT_MIN, i;
  for(i = 0; i < star_count; i++){
    if(stars[i]->pos_x < x1)
      x1 = stars[i]->pos_x;
    if(x2 < stars[i]->pos_x)
      x2 = stars[i]->pos_x;
    if(stars[i]->pos_y < y1)
      y1 = stars[i]->pos_y;
    if(y2 < stars[i]->pos_y)
      y2 = stars[i]->pos_y;
  }
  *mx1 = x1;
  *my1 = y1;
  *mx2 = x2;
  *my2 = y2;
  return area(x1, y1, x2, y2);
}

int read_stars(struct star * stars[MAX_STARS]){
  int i,px,py,dx,dy;
  FILE * fp;
  for(i = 0; i < MAX_STARS; i++){
    if(stars[i] != NULL){
      free(stars[i]);
      stars[i] = NULL;
    }
  }

  i = 0;
  fp = fopen("day10.txt","r");
  while(!feof(fp)){
    if(fscanf(fp, "position=< %d,  %d> velocity=< %d, %d> ", &px, &py, &dx, &dy)){
      stars[i] = (struct star *)malloc(sizeof(struct star));
      stars[i]->pos_x = px;
      stars[i]->pos_y = py;
      stars[i]->vel_x = dx;
      stars[i]->vel_y = dy;
      i++;
    }
  }
  fclose(fp);
  printf("Read %d stars\n", i);
  return i;
}

void print_stars(struct star * stars[MAX_STARS], int star_count, int x1, int y1, int x2, int y2){
  printf("Printing\n");
  int width = x2 - x1;
  int height = y2 - y1;

  int xo = x1; // subtract to get posn
  int yo = y1;

  int i,x,y;

  char grid[height][width];

  printf("Making grid: X1:%d Y1:%d X2:%d Y2:%d W:%d H:%d XO:%d YO:%d\n", x1, y1, x2, y2, width, height, xo, yo);

  for(y = 0; y < height; y++){
    for(x = 0; x < width; x++){
      grid[y][x] = ' ';
    }
  }

  printf("Cleared\n");

  for(i = 0; i < star_count; i++){
    printf("Printing star %d ", i);
    fflush(stdout);
    x = stars[i]->pos_x - xo;
    y = stars[i]->pos_y - yo;
    printf("at (%d,%d) ", x, y);
    fflush(stdout);
    grid[y][x] = '*';
    printf("OK\n");
    fflush(stdout);
  }

  printf("Drawn\n");

  for(y = 0; y < height; y++){
    for(x = 0; x < width; x++){
      printf("%c", grid[y][x]);
    }
    printf("\n");
  }
  printf("\n");
}

void part1(){
  struct star * stars[MAX_STARS] = { NULL };
  int star_count = 0;
  int ticks,i,px,py,dx,dy;
  int x1,x2,y1,y2,cur_area,min_area = INT_MAX;

  star_count = read_stars(stars);

  cur_area = find_bounding_box(stars, star_count, &x1, &y1, &x2, &y2);
  for(i = 0; i < 1000000; i++){
    if(cur_area < min_area){
      min_area = cur_area;
      ticks = i;
    }
    tick(stars, star_count);
    cur_area = find_bounding_box(stars, star_count, &x1, &y1, &x2, &y2);
  }

  printf("Found min area: %d in %d ticks\n", min_area, ticks);

  read_stars(stars);
  for(i = 0; i < ticks; i++)
    tick(stars, star_count);

  find_bounding_box(stars, star_count, &x1, &y1, &x2, &y2);
  print_stars(stars, star_count, x1, y1, x2, y2);
}

int main(){
  part1();
  return 0;
}
