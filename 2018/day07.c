#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

void add_dependency(int *dependencies, char from, char to){
  int index = from - 'A';
  int bit = (1 << (to - 'A'));
  dependencies[index] = dependencies[index] | bit;
}

void remove_dependency(int *dependencies, char to){
  int index = (to - 'A');
  int i, bit = ~(1 << (to - 'A'));
  for(i = 0; i < 26; i++)
    dependencies[i] = dependencies[i] & bit;
  dependencies[index] = ~bit;
}

int first_dependency(int *dependencies){
  int i;
  // printf("Finding dependency...\n");
  for(i = 0; i < 26; i++){
    if(dependencies[i] == 0)
      return i + 'A';
  }
  return -1;
}

void part1(){
  FILE * fp;
  char from, to;
  int *dependencies, current, done;

  dependencies = calloc(26, sizeof(int));
  fp = fopen("day07.txt","r");
  while(!feof(fp)){
    if(fscanf(fp, "Step %c must be finished before step %c can begin. ", &to, &from) == 2){
      // printf("%c,%c\n", from, to);
      add_dependency(dependencies, from, to);
    }
  }
  fclose(fp);
  printf("Part 1: ");
  done = 0;
  while(!done){
    current = first_dependency(dependencies);
    // printf("Found dependency: %d\n", current);
    if(current != -1){
      printf("%c",current);
      remove_dependency(dependencies, current);
    }
    else{
      done = 1;
    }
  }
  printf("\n");
  free(dependencies);
}

int all_done(int * dependencies){
  int i, bit;
  for(i = 0; i < 26; i++){
    bit = (1 << i);
    if(dependencies[i] != bit)
      return 0;
  }
  return 1;
}

void claim_job(int *dependencies, char job){
  int index = job - 'A';
  const int claim_bit = 1 << 28;
  dependencies[index] = dependencies[index] | claim_bit;
}

void unclaim_job(int *dependencies, char job){
  int index = job - 'A';
  const int claim_bit = 1 << 28;
  dependencies[index] = dependencies[index] & ~claim_bit;
}

int part2(){
  FILE * fp;
  char from, to;
  int *dependencies, current, done, time, worker, next_time, done_time, next_job;
  const int workers = 5;
  int workers_done_in[workers] = { INT_MAX };
  int workers_working_on[workers] = { -1 };

  dependencies = calloc(26, sizeof(int));
  fp = fopen("day07.txt","r");
  while(!feof(fp)){
    if(fscanf(fp, "Step %c must be finished before step %c can begin. ", &to, &from) == 2){
      add_dependency(dependencies, from, to);
    }
  }
  fclose(fp);

  done = 0;
  time = 0;

  while(!done){
    // if all jobs are done, we are done.
    if(all_done(dependencies)){
      return time;
    }

    // until no workers/jobs are available:
    // select a worker not doing anything
    // if a job is available, give it to him
    // if no job is available, we are done

    for(worker = 0; worker < workers; worker++){
      if(workers_working_on[worker] == -1){
	next_job = first_dependency(dependencies);
	if(next_job != -1){
	  // printf("WORKER %d CLAIMED %c\n", worker, next_job);
	  claim_job(dependencies, next_job);
	  done_time = time + 60 + (next_job - 'A') + 1;
	  workers_done_in[worker] = done_time;
	  workers_working_on[worker] = next_job;
	}
	else{
	  break;
	}
      }
    }

    for(worker = 0, next_time = INT_MAX; worker < workers; worker++){
      if(workers_done_in[worker] < next_time){
	next_time = workers_done_in[worker];
      }
    }

    time = next_time;

    for(worker = 0; worker < workers; worker++){
      if((workers_done_in[worker] != INT_MAX) && (workers_done_in[worker] == time)){
	// printf("WORKER %d JOB %c DONE AT %d\n", worker, workers_working_on[worker], time);
	unclaim_job(dependencies, workers_working_on[worker]);
	workers_done_in[worker] = INT_MAX;
	remove_dependency(dependencies, workers_working_on[worker]);
	workers_working_on[worker] = -1;
      }
    }
  }
  return -1;
}

int main(){
  part1();
  printf("Part 2: %d\n", part2());
  return 0;
}
