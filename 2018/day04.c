#include <stdio.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>

int guard_id_with_most_minutes(){
  const int MAX_GUARD_ID = 10000;
  FILE * fp;
  char *line, date_string[17], action;
  int guard_id, stop, i, j;
  size_t len;
  ssize_t nread;
  int guard_minutes[MAX_GUARD_ID] = { 0 };
  int minutes_per_guard[MAX_GUARD_ID][60] = { 0 };
  date_string[16] = '\0';
  struct tm * fell_asleep = malloc(sizeof(struct tm));
  struct tm * woke_up = malloc(sizeof(struct tm));
  time_t asleep, awake;
  int max_guard_id = 0;

  fp = fopen("day04.txt","r");
  while(!feof(fp)){
    while ((nread = getline(&line, &len, fp)) != -1) {
      if(len > 24) {
	strncpy(date_string, line + 1, 16);
	action = line[19];
	if(action == 'G'){
	  sscanf(line + 25, "#%d", &guard_id);
	  printf("%s %c:%d\n", date_string, action, guard_id);
	}
	else if(action == 'f'){
	  printf("%s %c\n", date_string, action);
	  strptime(date_string, "%Y-%m-%d %H:%M:%S", fell_asleep);
	  fell_asleep->tm_year += 500;
	  printf("%d", fell_asleep->tm_year);
	}
	else if(action == 'w'){
	  strptime(date_string, "%Y-%m-%d %H:%M:%S", woke_up);
	  woke_up->tm_year += 500;
	  asleep = mktime(fell_asleep);
	  awake = mktime(woke_up);
	  printf("a/a %ld/%ld\n", asleep, awake);
	  guard_minutes[guard_id] += difftime(awake, asleep);

	  stop = 0;
	  while(awake != asleep){
	    woke_up = localtime(&asleep);
	    minutes_per_guard[guard_id][woke_up->tm_min]++;
	    asleep += 60;
	  }

	  if(guard_minutes[guard_id] > guard_minutes[max_guard_id]){
	    max_guard_id = guard_id;
	  }
	}
      }
    }
  }
  free(fell_asleep);
  free(woke_up);
  fclose(fp);

  int max_minute = 0;
  for(i = 0; i < 60; i++){
    if(minutes_per_guard[max_guard_id][i] > minutes_per_guard[max_guard_id][max_minute]){
      max_minute = i;
    }
  }

  int best_guard = 0;
  int best_minute = 0;
  for(i = 0; i < MAX_GUARD_ID; i++){
    for(j = 0; j < 60; j++){
      if(minutes_per_guard[i][j] > minutes_per_guard[best_guard][best_minute]){
	best_guard = i;
	best_minute = j;
      }
    }
  }
  printf("BEST GUARD/MIN: %d %d %d\n", best_guard, best_minute, best_guard * best_minute);
  return max_guard_id * max_minute;
}


int main(){
  printf("%d\n", guard_id_with_most_minutes());
  return 0;
}
