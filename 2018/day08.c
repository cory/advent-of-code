#include <stdio.h>

int metadata_sum(FILE * fp){
  int children, metadata_entries, current_entry, i, sum;
  if(fscanf(fp, "%d %d ", &children, &metadata_entries) < 2){
    printf("Expected header but found nothing!\n");
    return -1;
  }
  sum = 0;
  for(i = 0; i < children; i++){
    sum += metadata_sum(fp);
  }
  for(i = 0; i < metadata_entries; i++){
    if(fscanf(fp, "%d ", &current_entry) != 1){
      printf("Expected metadata entry but found nothing!\n");
      return -1;
    }
    sum += current_entry;
  }
  return sum;
}

int node_sum(FILE * fp){
  int children_sums[11], num_children, metadata_entries, current_entry, i, sum;
  if(fscanf(fp, "%d %d ", &num_children, &metadata_entries) < 2){
    printf("Expected header but found nothing!\n");
    return -1;
  }
  // printf("NODE: %d %d\n", num_children, metadata_entries);
  for(i = 0; i < 11; i++){
    children_sums[i] = 0;
  }
  for(i = 0; i < num_children; i++){
    sum = node_sum(fp);
    // printf("CHILD SUM %d: %d\n", i, sum);
    children_sums[i] = sum;
  }
  sum = 0;
  for(i = 0; i < metadata_entries; i++){
    if(fscanf(fp, "%d ", &current_entry) != 1){
      printf("Expected metadata entry but found nothing!\n");
      return -1;
    }
    if(num_children != 0){
      sum += children_sums[current_entry-1];
      // printf("ADD %d: %d = %d\n", current_entry, children_sums[current_entry-1], sum);
    }
    else{
      sum += current_entry;
    }

  }
  // printf("NODE SUM: %d\n", sum);
  return sum;
}

int main(){
  FILE * fp;
  fp = fopen("day08.txt","r");
  printf("Part 1: %d\n", metadata_sum(fp));
  fclose(fp);
  fp = fopen("day08.txt","r");
  printf("Part 2: %d\n", node_sum(fp));
  fclose(fp);
  return 0;
}
