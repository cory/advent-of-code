#include <stdio.h>

const int MAX_DIMENSION = 1000;
int rect[MAX_DIMENSION][MAX_DIMENSION];

int part1(){
  FILE * fp;
  int id, x, y, width, height, i, j, count;
  fp = fopen("day03.txt","r");
  count = 0;
  while(!feof(fp)){
    // #3 @ 5,5: 2x2
    if(fscanf(fp, "#%d @ %d,%d: %dx%d\n", &id, &x, &y, &width, &height)){
      for(j = y; j < y + height; j++){
	for(i = x; i < x + width; i++){
	  if(rect[j][i] == 0){
	    rect[j][i] = id;
	  }
	  else if(rect[j][i] != 10000){
	    rect[j][i] = 10000;
	    count++;
	  }
	}
      }
    }
  }
  fclose(fp);
  return count;
}

int part2(){
  FILE * fp;
  int id, x, y, width, height, i, j, match;
  fp = fopen("day03.txt","r");

  while(!feof(fp)){
    // #3 @ 5,5: 2x2
    if(fscanf(fp, "#%d @ %d,%d: %dx%d\n", &id, &x, &y, &width, &height)){
      match = 1;
      for(j = y; j < y + height; j++){
	for(i = x; i < x + width; i++){
	  if(rect[j][i] != id){
	    match = 0;
	  }
	}
      }
      if(match == 1){
	fclose(fp);
	return id;
      }
    }
  }
  fclose(fp);
  return -1;
}

int main(){
  printf("Part 1: %d\n", part1());
  printf("Part 2: %d\n", part2());
  return 0;
}
