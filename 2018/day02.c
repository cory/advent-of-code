#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

int n_of_any_letter(char * s, unsigned n){
  unsigned * seen = calloc(255, 1);
  int i, slot;
  for(i = 0; i < strlen(s); i++){
    slot = s[i] - 'a';
    seen[slot]++;
  }
  for(i = 0; i < 26; i++){
    if(seen[i] == n){
      return 1;
    }
  }
  free(seen);
  return 0;
}

int two_of_any_letter(char * s){
  return n_of_any_letter(s, 2);
}

int three_of_any_letter(char * s){
  return n_of_any_letter(s, 3);
}

int part1(){
  FILE * fp;
  char s[100];
  int twos,threes;
  twos = threes = 0;
  fp = fopen("day02.txt","r");
  while(!feof(fp)){
    if(fscanf(fp, "%s", s) == 1){
      if(two_of_any_letter(s))
	twos++;
      if(three_of_any_letter(s)){
	threes++;
      }
    }
  }
  fclose(fp);
  return twos * threes;
}

int string_distance(char * s1, char * s2){
  int i, distance;
  distance = 0;
  for(i = 0; i < strlen(s1); i++){
    if(s1[i] != s2[i]) distance++;
  }
  return distance;
}

void part2(){
  FILE * fp;
  char check_word[100];
  char s[100];
  int current_word, i;

  current_word = 0;
  fp = fopen("day02.txt","r");
  while(1){
    fseek(fp, 0, SEEK_SET);
    for(i = 0; i < current_word; i++){
      if(feof(fp) || fscanf(fp, "%s", check_word) != 1){
	printf("Got to end without finding a match\n");
	return;
      }
    }

    while(!feof(fp)){
      if(fscanf(fp, "%s", s) == 1){
	if(string_distance(s, check_word) == 1){
	  printf("Part 2: %s %s\n", s, check_word);
	  return;
	}
      }
    }
    current_word++;
  }
}

int main(){
  printf("Part 1: %d\n", part1());
  part2();
  return 0;
}
