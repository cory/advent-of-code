#include <stdio.h>
#include <assert.h>
#define INPUT 7403

int power_level(int x, int y){
  int power_level;
  int rack_id = 10 + x;
  power_level = rack_id * y;
  power_level += INPUT;
  power_level *= rack_id;
  power_level %= 1000;
  power_level /= 100;
  power_level -= 5;

  return power_level;
}

int total_power_in_square(int x, int y, int size){
  int sum = 0, i, j;
  for(i = x; i < x + size; i++){
    for(j = y; j < y + size; j++){
      sum += power_level(i, j);
    }
  }
  return sum;
}

void part1(){
  int ms = -10000,mx,my,i,j,s;
  for(i = 1; i < 299; i++){
    for(j = 1; j < 299; j++){
      s = total_power_in_square(i, j, 3);
      if(s > ms){
	mx = i;
	my = j;
	ms = s;
      }
    }
  }
  printf("Part 1: Max: %d at (%d,%d)", ms, mx, my);
}

int total_power_in_square_computed(int grid[301][301], int x, int y, int size){
  int sum = 0, i, j;
  for(j = y; j < y + size; j++){
    for(i = x; i < x + size; i++){
      sum += grid[j][i];
    }
  }
  return sum;
}

void part2(){
  int grid[301][301];
  int ms = -10000,mx,my,msize,i,j,power,size,max_dimension;
  for(j = 1; j < 301; j++){
    for(i = 1; i < 301; i++){
      grid[j][i] = power_level(j, i);
    }
  }

  for(size = 1; size < 301; size++){
    max_dimension = 301 - size;
    for(i = 1; i < max_dimension; i++){
      for(j = 1; j < max_dimension; j++){
	power = total_power_in_square_computed(grid, i, j, size);
	if(power > ms){
	  mx = j;
	  my = i;
	  ms = power;
	  msize = size;
	}
      }
    }
  }
  printf("Part 2: Max: %d at (%d,%d,%d)", ms, mx, my, msize);
}

int main(){
  part1();
  part2();
  return 0;
}
